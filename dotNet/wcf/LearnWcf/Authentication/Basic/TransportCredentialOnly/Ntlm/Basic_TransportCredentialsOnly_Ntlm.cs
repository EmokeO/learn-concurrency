﻿using ro.galanton.learn.wcf.authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Basic.TransportCredentialOnly.Basic
{
    class Basic_TransportCredentialsOnly_Ntlm : AbstractServiceHost
    {
        public Basic_TransportCredentialsOnly_Ntlm(Type contractType, Type serviceType, string baseAddress) : base(contractType, serviceType, baseAddress) { }

        protected override Uri GetAddress(string baseAddress)
        {
            return new Uri(baseAddress + "/Basic/TransportCredentialsOnly/Ntlm");
        }

        protected override System.ServiceModel.Channels.Binding CreateBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            return binding;
        }

        protected override System.ServiceModel.Description.IServiceBehavior[] GetBehaviors()
        {
            return new IServiceBehavior[0];
        }
    }
}

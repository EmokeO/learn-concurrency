﻿using Authentication;
using Authentication.Basic.TransportCredentialOnly.Basic;
using ro.galanton.learn.wcf.services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.MsmqIntegration;
using System.ServiceModel.Security;

namespace ro.galanton.learn.wcf.authentication
{
    class Program
    {

        private void doNotCall()
        {
            BasicHttpBinding b1 = null;
            b1.Security.Mode = BasicHttpSecurityMode.None;
            b1.Security.Mode = BasicHttpSecurityMode.Message;
            b1.Security.Mode = BasicHttpSecurityMode.Transport;
            b1.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            b1.Security.Mode = BasicHttpSecurityMode.TransportWithMessageCredential;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.Digest;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.InheritedFromHost;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            b1.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            b1.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.Certificate;
            b1.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;

            NetTcpBinding b2 = null;
            b2.Security.Mode = SecurityMode.None;
            b2.Security.Mode = SecurityMode.Message;
            b2.Security.Mode = SecurityMode.Transport;
            b2.Security.Mode = SecurityMode.TransportWithMessageCredential;
            b2.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;
            b2.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
            b2.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
            b2.Security.Message.ClientCredentialType = MessageCredentialType.Certificate;
            b2.Security.Message.ClientCredentialType = MessageCredentialType.IssuedToken;
            b2.Security.Message.ClientCredentialType = MessageCredentialType.None;
            b2.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
            b2.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            WSHttpBinding b3 = null;
            b3.Security.Mode = SecurityMode.None; // etc
            b3.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic; // etc
            b3.Security.Message.ClientCredentialType = MessageCredentialType.None; // etc

            WSDualHttpBinding b4 = null;
            b4.Security.Mode = WSDualHttpSecurityMode.None;
            b4.Security.Mode = WSDualHttpSecurityMode.Message;
            b4.Security.Message.ClientCredentialType = MessageCredentialType.None; // etc

            // WebHttpBinding in System.ServiceModel.Web

            NetMsmqBinding b5 = null;
            b5.Security.Mode = NetMsmqSecurityMode.Both;
            b5.Security.Mode = NetMsmqSecurityMode.Message;
            b5.Security.Mode = NetMsmqSecurityMode.None;
            b5.Security.Mode = NetMsmqSecurityMode.Transport;
            b5.Security.Transport.MsmqAuthenticationMode = MsmqAuthenticationMode.Certificate;
            b5.Security.Transport.MsmqAuthenticationMode = MsmqAuthenticationMode.None;
            b5.Security.Transport.MsmqAuthenticationMode = MsmqAuthenticationMode.WindowsDomain;
            b5.Security.Message.ClientCredentialType = MessageCredentialType.None; // etc

            WSFederationHttpBinding b6 = null;
            b6.Security.Mode = WSFederationHttpSecurityMode.Message;
            b6.Security.Mode = WSFederationHttpSecurityMode.None;
            b6.Security.Mode = WSFederationHttpSecurityMode.TransportWithMessageCredential;
            b6.Security.Message = null; // complicated

            NetPeerTcpBinding b7 = null;
            MsmqIntegrationBinding b8 = null;

        }

        static void Main(string[] args)
        {
            Type contractType = typeof(ICalculatorService);
            Type serviceType = typeof(CalculatorService);
            string baseAddress = "http://localhost:8095/wcf/authentication";

            //ServiceHost host = null;

            IList<AbstractServiceHost> hosts = new List<AbstractServiceHost>();

            try
            {

                //BasicHttpBinding binding = new BasicHttpBinding();
                //binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                //host = new ServiceHost(serviceType, baseAddress);
                //host.AddServiceEndpoint(contractType, binding, "basic");

                //ServiceCredentials credentials = new ServiceCredentials();
                //credentials.UserNameAuthentication.UserNamePasswordValidationMode = UserNamePasswordValidationMode.Custom;
                //credentials.UserNameAuthentication.CustomUserNamePasswordValidator = new UserValidator();

                //host.Description.Behaviors.Add(new ServiceMetadataBehavior() { HttpGetEnabled = true });
                //host.Description.Behaviors.Add(credentials);

                

                //host.Open();

                hosts.Add(new Basic_TransportCredentialsOnly_Basic(contractType, serviceType, baseAddress));
                hosts.Add(new Basic_TransportCredentialsOnly_Digest(contractType, serviceType, baseAddress));
                hosts.Add(new Basic_TransportCredentialsOnly_Ntlm(contractType, serviceType, baseAddress));

                foreach (AbstractServiceHost host in hosts)
                {
                    try 
                    {
                        host.Open();
                        Console.WriteLine("Service {0} started", host.BaseAddress);
                    } catch (Exception e)
                    {
                        Console.WriteLine("Failed to start service {0}: {1}", host.BaseAddress, e.Message);
                        Console.WriteLine(e.StackTrace);
                    }
                }

                Console.WriteLine("Press any key to exit ...");
                Console.ReadKey(true);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                foreach (AbstractServiceHost host in hosts)
                {
                    try
                    {
                        host.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to close service {0}: {1}", host.BaseAddress, e.Message);
                        Console.WriteLine(e.StackTrace);
                    }
                }
            }
        }

        
    }
}

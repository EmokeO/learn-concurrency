﻿using System.IdentityModel.Selectors;
using System.ServiceModel;

namespace ro.galanton.learn.wcf.authentication
{
    class UserValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (userName != "petru" || password != "123")
                throw new FaultException("Invalid username or password");
        }
    }
}

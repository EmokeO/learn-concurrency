﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ro.galanton.learn.wcf.authentication
{
    public class BindingType
    {

        public static readonly BindingType BASIC = new BindingType("basic");

        private string name;

        private BindingType(string name)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj is BindingType)
                return name == (obj as BindingType).name;
            return false;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public Binding ToBinding()
        {
            switch (name)
            {
                case "basic": return new BasicHttpBinding();
                default: throw new Exception("Unknown binding type: " + name);
            }
        }

    }
}

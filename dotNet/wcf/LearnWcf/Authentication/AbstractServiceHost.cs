﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Authentication
{
    public abstract class AbstractServiceHost
    {

        private Type contractType;
        private ServiceHost host;

        public AbstractServiceHost(Type contractType, Type serviceType, string baseAddress)
        {
            this.contractType = contractType;
            this.host = new ServiceHost(serviceType, GetAddress(baseAddress));
        }

        protected abstract Uri GetAddress(string baseAddress);
        protected abstract Binding CreateBinding();
        protected abstract IServiceBehavior[] GetBehaviors();

        public void Open()
        {
            host.AddServiceEndpoint(contractType, CreateBinding(), "");

            host.Description.Behaviors.Add(new ServiceMetadataBehavior() { HttpGetEnabled = true });
            foreach (IServiceBehavior behavior in GetBehaviors())
            {
                host.Description.Behaviors.Add(behavior);
            }

            host.Open();
        }

        public void Close()
        {
            host.Close();
        }

        public Uri BaseAddress { get { return host.BaseAddresses[0]; } }

    }
}

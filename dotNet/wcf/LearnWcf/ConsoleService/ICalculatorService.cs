﻿using System.ServiceModel;

namespace ConsoleService
{

    [ServiceContract]
    interface ICalculatorService
    {
        [OperationContract]
        int add(int x, int y);

    }
}

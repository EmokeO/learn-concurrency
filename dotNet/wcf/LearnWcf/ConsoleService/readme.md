﻿In order to be able to bind the service to localhost:8095, access must be granted for the current user to bind to this port. 
Open a command prompt as Administrator and run the following command:

netsh http add urlacl url=http://+:8095/wcf/ user=DOMAIN\user

If you are not in a domain, use the name of your local machine as DOMAIN. Eg. EMERALD\Peti.
﻿using System;
using System.ServiceModel;

namespace ro.galanton.learn.wcf.services
{

    [ServiceContract]
    public interface ICalculatorService
    {

        [OperationContract]
        int add(int x, int y);

    }

    public class CalculatorService : ICalculatorService
    {

        public CalculatorService()
        {
            Console.WriteLine("New CalculatorService instance created");
        }

        public int add(int x, int y)
        {
            Console.WriteLine("[CalculatorService] [add] x={0}, y={0}", x, y);
            return x + y;
        }
    }
}

package ro.galanton.learn.spring.boot.config;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import ro.galanton.learn.spring.boot.Application;
import ro.galanton.learn.spring.boot.annotation.RootContext;

@Configuration
@ComponentScan(basePackageClasses = Application.class, includeFilters = @Filter(RootContext.class), useDefaultFilters = false)
@EnableAutoConfiguration(exclude = { DispatcherServletAutoConfiguration.class })
@RootContext
public class RootConfig implements ApplicationContextAware, ServletContextAware {

	private ApplicationContext applicationContext;
	private ServletContext servletContext;
	
	@Bean
	public DispatcherServlet defaultServlet() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setParent(applicationContext);
		context.setServletContext(servletContext);
		context.register(DefaultConfig.class);

		DispatcherServlet servlet = new DispatcherServlet(context);
		servlet.setDispatchOptionsRequest(false);
		servlet.setDispatchTraceRequest(false);
		
		return servlet;
	}
	
	@Bean
	public ServletRegistrationBean defaultServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(defaultServlet(), "/*");
		registration.setName("default");
		registration.setLoadOnStartup(1);
		
		return registration;
	}
	
	@Bean
	public DispatcherServlet serverServlet() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setParent(applicationContext);
		context.setServletContext(servletContext);
		context.register(ServerConfig.class);

		return new DispatcherServlet(context);
	}
	
	@Bean
	public ServletRegistrationBean serverServletRegistration() {
		ServletRegistrationBean servlet = new ServletRegistrationBean(serverServlet(), "/server/*");
		servlet.setName("server");
		servlet.setLoadOnStartup(1);
		
		return servlet;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### root config created in context: " + applicationContext);
		this.applicationContext = applicationContext;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}

package ro.galanton.learn.spring.boot.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.stereotype.Service;

@Retention(RetentionPolicy.RUNTIME)
@Service
@RootContext
public @interface RootService {

}

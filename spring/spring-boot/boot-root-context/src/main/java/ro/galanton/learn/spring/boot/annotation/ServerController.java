package ro.galanton.learn.spring.boot.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.stereotype.Controller;

@Retention(RetentionPolicy.RUNTIME)
@Controller
@ServerContext
public @interface ServerController {

}

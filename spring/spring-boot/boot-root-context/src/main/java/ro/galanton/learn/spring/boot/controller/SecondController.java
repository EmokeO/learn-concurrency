package ro.galanton.learn.spring.boot.controller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import ro.galanton.learn.spring.boot.annotation.ServerController;
import ro.galanton.learn.spring.boot.service.SecondService;

@ServerController
public class SecondController implements ApplicationContextAware {

	private SecondService secondService;
	
	@Autowired
	public SecondController(SecondService secondService) {
		System.out.println("### second controller created");
		this.secondService = secondService;
	}
	
	@RequestMapping("/second")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void test() {
		System.out.println("### second controller");
		secondService.test();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### second controller context: " + applicationContext);
	}
}

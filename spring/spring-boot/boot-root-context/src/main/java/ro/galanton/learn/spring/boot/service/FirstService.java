package ro.galanton.learn.spring.boot.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import ro.galanton.learn.spring.boot.annotation.RootContext;
import ro.galanton.learn.spring.boot.repository.FirstRepository;

@Service
@RootContext
public class FirstService implements ApplicationContextAware {

	private FirstRepository firstRepository;
	
	@Autowired
	public FirstService(FirstRepository firstRepository) {
		System.out.println("### first service created");
		this.firstRepository = firstRepository;
	}
	
	public void test() {
		System.out.println("### first service");
		firstRepository.test();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### first service context: " + applicationContext);
	}

}

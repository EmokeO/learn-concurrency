package ro.galanton.learn.spring.boot.annotation;

import org.springframework.stereotype.Repository;

@RootContext
@Repository
public @interface RootRepository {

}

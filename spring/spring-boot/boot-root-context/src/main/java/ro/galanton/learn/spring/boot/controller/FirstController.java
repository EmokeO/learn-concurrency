package ro.galanton.learn.spring.boot.controller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import ro.galanton.learn.spring.boot.annotation.ServerContext;
import ro.galanton.learn.spring.boot.service.FirstService;

@Controller
@ServerContext
public class FirstController implements ApplicationContextAware {

	@SuppressWarnings("unused")
	private ApplicationContext applicationContext;
	
	private FirstService firstService;
	
	@Autowired
	public FirstController(FirstService firstService) {
		this.firstService = firstService;
		System.out.println("### first controller created");
	}
	
	@RequestMapping("/first")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void test() {
		System.out.println("### first controller");
		firstService.test();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		System.out.println("### first controller context: " + applicationContext);
	}
	
}

package matchers;

import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;

public class IsInstanceOf extends DiagnosingMatcher<Object> {

	private String className;
	
	public IsInstanceOf(String className) {
		this.className = className;
	}
	
	@Override
	public void describeTo(Description description) {
		description.appendText("an instance of ").appendText(className);
	}

	@Override
	protected boolean matches(Object item, Description mismatchDescription) {
		if (item == null) {
			mismatchDescription.appendText("null");
			return false;
		}
		
		String itemClass = determineItemClass(item);
		if (itemClass == null) {
			mismatchDescription.appendText("could not determine class of ").appendValue(item);
			return false;
		}
		
		if (!className.equals(itemClass)) {
			mismatchDescription.appendText("is a ").appendText(item.getClass().getName());
			return false;
		}
		
		return true;
	}
	
	private static String determineItemClass(Object item) {
		String itemClass = item.getClass().getName();
		
		// if the class is enhanced with CGLIB, strip the CGLIB stuff
		if (itemClass.contains("$$")) {
			itemClass = itemClass.substring(0, itemClass.indexOf("$$"));
		}
		
		return itemClass;
	}

}

package matchers;

import org.hamcrest.Matcher;

public class Matchers {

	public static Matcher<Object> instanceOf(String className) {
		return new IsInstanceOf(className);
	}
}

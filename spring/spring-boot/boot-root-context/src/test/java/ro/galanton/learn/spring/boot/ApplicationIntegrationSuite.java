package ro.galanton.learn.spring.boot;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static matchers.Matchers.*;

import java.util.Map;

import javax.servlet.Servlet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.autoconfigure.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpEncodingAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import ro.galanton.learn.spring.boot.annotation.ServerContext;
import ro.galanton.learn.spring.boot.config.RootConfig;

import com.jayway.restassured.RestAssured;


@RunWith(Suite.class)
@SuiteClasses({
	ApplicationIntegrationSuite.ApplicationSpec.class,
	ApplicationIntegrationSuite.RootContextSpecSuite.class,
	ApplicationIntegrationSuite.DefaultServletContextSpecSuite.class,
	ApplicationIntegrationSuite.ServerServletContextSpec.class
})
public class ApplicationIntegrationSuite {

	static Application application;
	
	@BeforeClass
	public static void init() {
		Application.main();
		assertThat(Application.getRunningApplications(), hasSize(1));
		application = Application.getRunningApplications().get(0);
	}
	
	public static class ApplicationSpec {
		
		@Test
		public void has_a_web_application_context() {
			assertThat(application.getApplicationContext(), instanceOf(WebApplicationContext.class));
		}
		
	}
	
	@RunWith(Suite.class)
	@SuiteClasses({
		RootContextSpecSuite.RootContextSpec.class,
		RootContextSpecSuite.RootContextAutoConfigurationSpec.class
	})
	public static class RootContextSpecSuite {
	
		static WebApplicationContext context;
		static Map<String, Object> configurations;
		
		@BeforeClass
		public static void init() {
			context = (WebApplicationContext) application.getApplicationContext();
			configurations = context.getBeansWithAnnotation(Configuration.class);
		}
		
		public static class RootContextSpec {
		
			@Test
			public void is_a_root_context() {
				assertThat(context.getParent(), is(nullValue()));
			}
			
			@Test
			public void contains_a_default_dispatcher_servlet() {
				context.getBean("defaultServlet", DispatcherServlet.class);
			}
			
			@Test
			public void contains_a_default_servlet_registration() {
				ServletRegistrationBean registration = context.getBean("defaultServletRegistration", ServletRegistrationBean.class);
				
				assertThat(registration.getServletName(), is("default"));
				assertThat(registration.getUrlMappings(), contains("/*"));
			}
			
			@Test
			public void contains_a_server_dispatcher_servlet() {
				context.getBean("serverServlet", DispatcherServlet.class);
			}
			
			@Test
			public void contains_a_server_servlet_registration() {
				ServletRegistrationBean registration = context.getBean("serverServletRegistration", ServletRegistrationBean.class);
				
				assertThat(registration.getServletName(), is("server"));
				assertThat(registration.getUrlMappings(), contains("/server/*"));
			}
			
			@Test
			public void contains_no_other_servlet_beans() {
				Map<String, Servlet> servlets = context.getBeansOfType(Servlet.class);
				
				// if this fails:
				// - either it was added on purpose => add specs for it
				// - or it may have been added by accident => investigate why the new servlet appeared and
				//		- remove it
				//		- or add specs for it
				assertThat(servlets.keySet(), containsInAnyOrder("defaultServlet", "serverServlet"));
			}
			
			
			@Test
			public void contains_no_other_servlet_registrations() {
				Map<String, ServletRegistrationBean> registrations = context.getBeansOfType(ServletRegistrationBean.class);
	
				// if this fails:
				// - either it was added on purpose => add specs for it
				// - or it may have been added by accident => investigate why the new servlet registration appeared and
				//		- remove it, or
				//		- add specs for it
				assertThat(registrations.keySet(), containsInAnyOrder("defaultServletRegistration", "serverServletRegistration"));
			}
			
			@Test
			public void uses_RootConfig() {
				assertThat(configurations, hasValue(instanceOf(RootConfig.class)));
			}
			
		}
		
		public static class RootContextAutoConfigurationSpec {
			
			@Test
			public void uses_PropertyPlaceholderAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(PropertyPlaceholderAutoConfiguration.class)));
			}
			
			@Test
			public void uses_PropertySourcesPlaceholderConfigurer() {
				assertThat(configurations, hasValue(instanceOf(PropertySourcesPlaceholderConfigurer.class)));
			}
			
			@Test
			public void uses_WebSocketAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(WebSocketAutoConfiguration.class)));
			}
	
			@Test
			public void uses_WebSocketAutoConfiguration$TomcatWebSocketConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration$TomcatWebSocketConfiguration")));
			}
			
			@Test
			public void uses_EmbeddedServletContainerAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(EmbeddedServletContainerAutoConfiguration.class)));
			}
	
			@Test
			public void uses_EmbeddedServletContainerAutoConfiguration$EmbeddedTomcat() {
				assertThat(configurations, hasValue(instanceOf(EmbeddedServletContainerAutoConfiguration.EmbeddedTomcat.class)));
			}
			
			@Test
			public void uses_ErrorMvcAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(ErrorMvcAutoConfiguration.class)));
			}
	
			@Test
			public void uses_ErrorMvcAutoConfiguration$WhitelabelErrorViewConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration$WhitelabelErrorViewConfiguration")));
			}
			
			@Test
			public void uses_ErrorMvcAutoConfiguration$PreserveErrorControllerTargetClassPostProcessor() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration$PreserveErrorControllerTargetClassPostProcessor")));
			}
			
			@Test
			public void uses_WebMvcAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(WebMvcAutoConfiguration.class)));
			}
			
			@Test
			public void uses_WebMvcAutoConfiguration$EnableWebMvcConfiguration() {
				assertThat(configurations, hasValue(instanceOf(WebMvcAutoConfiguration.EnableWebMvcConfiguration.class)));
			}
			
			@Test
			public void uses_WebMvcAutoConfiguration$WebMvcAutoConfigurationAdapter() {
				assertThat(configurations, hasValue(instanceOf(WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter.class)));
			}
			
			@Test
			public void uses_WebMvcAutoConfiguration$WebMvcAutoConfigurationAdapter$FaviconConfiguration() {
				assertThat(configurations, hasValue(instanceOf(WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter.FaviconConfiguration.class)));
			}
			
			@Test
			public void uses_JmxAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(JmxAutoConfiguration.class)));
			}
			
			@Test
			public void uses_ConfigurationPropertiesAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(ConfigurationPropertiesAutoConfiguration.class)));
			}
			
			@Test
			public void uses_JacksonAutoConfiguration$JacksonObjectMapperBuilderConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration$JacksonObjectMapperBuilderConfiguration")));
			}
			
			@Test
			public void uses_JacksonAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(JacksonAutoConfiguration.class)));
			}
			
			@Test
			public void uses_JacksonAutoConfiguration$JacksonObjectMapperConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration$JacksonObjectMapperConfiguration")));
			}
			
			@Test
			public void uses_HttpMessageConvertersAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(HttpMessageConvertersAutoConfiguration.class)));
			}
			
			@Test
			public void uses_HttpMessageConvertersAutoConfiguration$StringHttpMessageConverterConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration$StringHttpMessageConverterConfiguration")));
			}
			
			
			@Test
			public void uses_JacksonHttpMessageConvertersConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.web.JacksonHttpMessageConvertersConfiguration")));
			}
			
			@Test
			public void uses_JacksonHttpMessageConvertersConfiguration$MappingJackson2HttpMessageConverterConfiguration() {
				assertThat(configurations, hasValue(instanceOf("org.springframework.boot.autoconfigure.web.JacksonHttpMessageConvertersConfiguration$MappingJackson2HttpMessageConverterConfiguration")));
			}
			
			@Test
			public void uses_HttpEncodingAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(HttpEncodingAutoConfiguration.class)));
			}
			
			@Test
			public void uses_MultipartAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(MultipartAutoConfiguration.class)));
			}
			
			@Test
			public void uses_ServerPropertiesAutoConfiguration() {
				assertThat(configurations, hasValue(instanceOf(ServerPropertiesAutoConfiguration.class)));
			}
			
			@Test
			public void uses_no_other_configurations() {
				// if this fails, add specs for new configs or check if some new configs were pulled in by accident
				// TODO would be nice if we could identify the unexpected configs in the error message
				assertThat(configurations.size(), is(26));
			}
			
		}

	}
	@RunWith(Suite.class)
	@SuiteClasses({
		DefaultServletContextSpecSuite.DefaultServletContextSpec.class,
		DefaultServletContextSpecSuite.WebResourcesSpec.class
	})
	public static class DefaultServletContextSpecSuite {
		
		static WebApplicationContext servletContext;
		
		@BeforeClass
		public static void init() throws Exception {
			WebApplicationContext context = (WebApplicationContext) application.getApplicationContext();
			
			DispatcherServlet servlet = context.getBean("defaultServlet", DispatcherServlet.class);
			servletContext = servlet.getWebApplicationContext();
			
			RestAssured.port = 8080;
		}
		
		public static class DefaultServletContextSpec {
		
			@Test
			public void has_root_context_as_parent() {
				assertThat(servletContext.getParent(), is(sameInstance(application.getApplicationContext())));
			}
			
			@Test
			public void has_resource_mapping_to_webresources() {
				SimpleUrlHandlerMapping mapping = servletContext.getBean("resourceHandlerMapping", SimpleUrlHandlerMapping.class);
				
				assertThat(mapping.getUrlMap(), hasKey("/webresources/*"));
			}
			
			@Test
			public void has_resource_mapping_to_webjars() {
				SimpleUrlHandlerMapping mapping = servletContext.getBean("resourceHandlerMapping", SimpleUrlHandlerMapping.class);
				
				assertThat(mapping.getUrlMap(), hasKey("/webjars/**"));
			}
			
			@Test
			public void has_resource_mapping_to_everything_else() {
				SimpleUrlHandlerMapping mapping = servletContext.getBean("resourceHandlerMapping", SimpleUrlHandlerMapping.class);
				
				// TODO is this a security risk ?
				assertThat(mapping.getUrlMap(), hasKey("/**"));
			}
			
			@Test
			public void has_no_other_resource_mappings() {
				SimpleUrlHandlerMapping mapping = servletContext.getBean("resourceHandlerMapping", SimpleUrlHandlerMapping.class);
				
				assertThat(mapping.getUrlMap().keySet(), hasSize(3));
			}
			
		}
		
		public static class WebResourcesSpec {

			static SimpleUrlHandlerMapping mapping;
			
			@BeforeClass
			public static void init() {
				mapping = servletContext.getBean("resourceHandlerMapping", SimpleUrlHandlerMapping.class);
			}
			
			@Test
			public void are_handled_by_resource_http_request_handler() {
				Object handlerObject = mapping.getUrlMap().get("/webresources/*");
				
				assertThat(handlerObject, is(instanceOf(ResourceHttpRequestHandler.class)));
			}
			
			@Test
			public void are_served_from_webresources_folder_in_classpath() {
				ResourceHttpRequestHandler handler = (ResourceHttpRequestHandler) mapping.getUrlMap().get("/webresources/*");
				Resource expectedLocation = new ClassPathResource("webresources/", servletContext.getClassLoader());
				
				assertThat(handler.getLocations(), contains(expectedLocation));
			}
		}
		
	}
	
	public static class ServerServletContextSpec {
		
		static WebApplicationContext serverContext;
		
		@BeforeClass
		public static void init() {
			WebApplicationContext applicationContext = (WebApplicationContext) application.getApplicationContext();
			DispatcherServlet servlet = applicationContext.getBean("serverServlet", DispatcherServlet.class);
			serverContext = servlet.getWebApplicationContext();
		}
		
		@Test
		public void contains_no_services() {
			Map<String, Object> services = serverContext.getBeansWithAnnotation(Service.class);
			
			assertThat(services.keySet(), is(empty()));
		}
		
		@Test
		public void contains_no_repositories() {
			Map<String, Object> repositories = serverContext.getBeansWithAnnotation(Repository.class);
			
			assertThat(repositories.keySet(), is(empty()));
		}
		
		@Test
		public void contains_controllers() {
			Map<String, Object> controllers = serverContext.getBeansWithAnnotation(Controller.class);
			
			assertThat(controllers.keySet(), is(not(empty())));
		}
		
	}

}

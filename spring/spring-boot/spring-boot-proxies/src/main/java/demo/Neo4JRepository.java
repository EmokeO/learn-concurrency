package demo;

import org.springframework.stereotype.Repository;

@Repository
@UsesSQL
public class Neo4JRepository implements IRepository, Stuff {

	public Neo4JRepository() {
		System.out.println("Neo4j");
	}
	@Override
	public void query() {
		System.out.println("Neo4JRepository.query");
	}

}

package demo;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	@Bean
	public static DemoBeanPostProcessor demoBeanPostProcessor() {
		return new DemoBeanPostProcessor();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Autowired
	public void test(List<IRepository> repos) {
		for (IRepository repo : repos) {
			repo.query();
		}
	}
	
}

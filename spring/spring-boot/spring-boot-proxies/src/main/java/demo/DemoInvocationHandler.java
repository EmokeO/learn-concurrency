package demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DemoInvocationHandler implements InvocationHandler {

	
	private Object target;
	
	public DemoInvocationHandler(Object target) {
		this.target = target;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("### invocation handler - invoke");
		// before logic
		
		// delegate method call
		try {
			return method.invoke(target, args);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			throw new RuntimeException("An SQL error occurred!");
		}
	}

}

package ro.galanton.learn.spring.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BasicAuthController {

	@RequestMapping(path = "/basic", method = RequestMethod.GET)
	public String index() {
		return "basic-auth";
	}

}

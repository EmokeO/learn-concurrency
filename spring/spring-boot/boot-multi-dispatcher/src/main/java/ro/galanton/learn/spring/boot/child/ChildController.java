package ro.galanton.learn.spring.boot.child;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.galanton.learn.spring.boot.main.MainService;

@RestController
public class ChildController {

	private MainService mainService;
	private ChildService childService;
	
	@Autowired
	public ChildController(MainService mainService, ChildService childService) {
		System.out.println("### child controller created");
		this.mainService = mainService;
		this.childService = childService;
	}
	
	@RequestMapping("/test")
	public void test() {
		System.out.println("### child");
		mainService.test();
		childService.test();
	}
	
}

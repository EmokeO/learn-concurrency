package ro.galanton.learn.spring.boot.main.config;

import javax.servlet.ServletContext;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import ro.galanton.learn.spring.boot.child.config.ChildConfig;

@EnableAutoConfiguration
@ComponentScan("ro.galanton.learn.spring.boot.main")
@EnableWebMvc
public class MainConfig {

	
	@Bean
	public ServletRegistrationBean childServlet(WebApplicationContext rootContext, ServletContext servletContext) {
		AnnotationConfigWebApplicationContext childContext = new AnnotationConfigWebApplicationContext();
		childContext.setParent(rootContext);
		childContext.setServletContext(servletContext);
		childContext.register(ChildConfig.class);
		childContext.refresh();
		
		ServletRegistrationBean childRegistration = new ServletRegistrationBean(new DispatcherServlet(childContext), "/child/*");
		childRegistration.setName("childServlet");
		childRegistration.setLoadOnStartup(1);
		
		return childRegistration;
	}
}

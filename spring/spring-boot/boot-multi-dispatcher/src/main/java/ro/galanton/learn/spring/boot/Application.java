package ro.galanton.learn.spring.boot;

import org.springframework.boot.SpringApplication;

import ro.galanton.learn.spring.boot.main.config.MainConfig;

public class Application {

	public static void main(String[] args) {
		new SpringApplication(MainConfig.class).run(args);
	}
	
}

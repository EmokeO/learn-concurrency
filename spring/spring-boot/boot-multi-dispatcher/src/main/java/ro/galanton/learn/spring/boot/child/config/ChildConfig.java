package ro.galanton.learn.spring.boot.child.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("ro.galanton.learn.spring.boot.child")
public class ChildConfig {

//	@Bean
//	public ServletRegistrationBean childServlet() {
//		ServletRegistrationBean childServlet = new ServletRegistrationBean(new DispatcherServlet(), "/child/*");
//		
//		return childServlet; 
//	}

}

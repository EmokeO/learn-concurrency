package ro.galanton.learn.spring.boot.main;

import org.springframework.stereotype.Service;

@Service
public class MainService {
	
	public MainService() {
		System.out.println("### main service created");
	}
	
	public void test() {
		System.out.println("### main service");
	}
}

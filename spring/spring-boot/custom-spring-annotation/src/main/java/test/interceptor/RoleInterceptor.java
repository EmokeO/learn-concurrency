package test.interceptor;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import test.annotation.InvokeWith;
import test.annotation.RequireRole;

public class RoleInterceptor extends HandlerInterceptorAdapter {

	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HandlerMethod method = (HandlerMethod) handler;
		
		System.out.println("### intercepted method: " + method);
		
		RequireRole annotation = method.getMethodAnnotation(RequireRole.class);
		if (annotation == null) {
			return true;
		}
		
		
		String requiredRole = annotation.value();
		System.out.println("### required role: " + requiredRole);
		
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication auth = securityContext.getAuthentication();
		System.out.println("### current authentication = " + auth);
		
		Collection<? extends GrantedAuthority> actualRoles = auth.getAuthorities();
		System.out.println("### actual roles: " + actualRoles);
		
		Optional<String> found = actualRoles.stream()
			.map(GrantedAuthority::getAuthority)
			.filter(roleName -> roleName.equals(requiredRole))
			.findAny();
		
		if (found.isPresent()) {
			return true;
		}

		response.setStatus(403);
		return false;
	}
	
	@InvokeWith(expression = "RoleInterceptor.getValue()")
	public void test(int x) {
		System.out.println("### invoked with " + x);
	}
	
	public static int getValue() {
		return 42;
	}
	
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		RoleInterceptor interceptor = new RoleInterceptor();
		
		Method test = RoleInterceptor.class.getMethod("test", int.class);
		
		InvokeWith annotation = test.getAnnotation(InvokeWith.class);
		if (annotation != null) {
			String expression = annotation.expression();
			ExpressionParser parser = new SpelExpressionParser();
			int value = (int) parser.parseExpression(expression).getValue();
			test.invoke(interceptor, value);
		}
	}
	
}

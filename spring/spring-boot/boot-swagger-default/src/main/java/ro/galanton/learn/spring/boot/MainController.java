package ro.galanton.learn.spring.boot;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	public MainController() {
		System.out.println("### main controller created");
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void test() {
		System.out.println("### main");
	}

}

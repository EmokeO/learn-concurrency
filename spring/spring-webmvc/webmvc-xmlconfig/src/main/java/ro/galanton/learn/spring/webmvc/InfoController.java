package ro.galanton.learn.spring.webmvc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = "application/json")
public class InfoController {

	
	@RequestMapping("/api/v1/info")
	public Map<String, String> info(@AuthenticationPrincipal Authentication authentication) {
		String username = "<unknown>";
		Object principal = authentication.getPrincipal();
		if (principal instanceof User) {
			User user = (User) principal;
			username = user.getUsername();
		}
		HashMap<String, String> result = new HashMap<>();
		result.put("info", "You authenticated with OAuth 2: " + username);
		return result;
	}
	
}

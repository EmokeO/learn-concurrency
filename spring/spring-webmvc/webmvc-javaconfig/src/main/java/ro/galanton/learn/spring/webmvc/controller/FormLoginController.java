package ro.galanton.learn.spring.webmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormLoginController {

	public FormLoginController() {
		System.out.println("### FormLoginController created");
	}
	
	@RequestMapping("/form-login")
	public String index() {
		System.out.println("### FormLoginController.index called");
		return "form-login-view";
	}
	
}

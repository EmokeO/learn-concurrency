package ro.galanton.learn.spring.webmvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

@Configuration
@EnableWebMvc // callbacks are not called if this is missing
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**")
					.addResourceLocations("/")
					.setCacheControl(CacheControl.noStore());
	}
	
	@Bean
	public ITemplateResolver templateResolver() {
		ServletContextTemplateResolver result = new ServletContextTemplateResolver();
		result.setPrefix("/WEB-INF/templates/");
		result.setSuffix(".html");
		result.setTemplateMode("HTML5");
		return result;
	}
	
	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine result = new SpringTemplateEngine();
		result.setTemplateResolver(templateResolver());
		return result;
	}
	
	@Bean
	public ThymeleafViewResolver thymeleafViewResolver() {
		ThymeleafViewResolver result = new ThymeleafViewResolver();
		result.setTemplateEngine(templateEngine());
		result.setOrder(1);
		result.setViewNames(new String[] {"*"});
		result.setCache(false);
		return result;
	}

}

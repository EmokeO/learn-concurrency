package ro.galanton.learn.spring.webmvc.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.UrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
import org.springframework.security.oauth2.provider.client.ClientDetailsUserDetailsService;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpointHandlerMapping;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@EnableWebSecurity
public class SecurityConfig {

// TODO [note] with this we can configure an authentication manager that can be shared by multiple filter chains
//	@Autowired
//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//				.withUser("petru").password("123").authorities("ROLE_USER");
//	}
	
	// TODO [note] with this we can also configure a shared authentication manager, but we can also specify the bean name
	@Bean
	public AuthenticationManager basicAuthenticationManager(ObjectPostProcessor<Object> objectPostProcessor) throws Exception {
		AuthenticationManagerBuilder auth = new AuthenticationManagerBuilder(objectPostProcessor);
		auth.inMemoryAuthentication()
				.withUser("petru").password("123").authorities("ROLE_USER");
		return auth.build();
	}
	
	@Bean
	public AuthenticationManager clientAuthenticationManager(ObjectPostProcessor<Object> objectPostProcessor) throws Exception {
		return new AuthenticationManagerBuilder(objectPostProcessor)
			.userDetailsService(clientDetailsUserService())
			.and().build();
	}
	
	@Bean
	public UserDetailsService clientDetailsUserService() throws Exception {
		return new ClientDetailsUserDetailsService(clientDetailsService());
	}
	
	@Bean
	public ClientDetailsService clientDetailsService() throws Exception {
		return new InMemoryClientDetailsServiceBuilder()
				.withClient("client")
					.secret("456")
					.authorizedGrantTypes("password")
					.scopes("default")
					.and().build();
				
	}
	
	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}
	
	@Bean
	public DefaultTokenServices tokenServices() throws Exception {
		DefaultTokenServices result = new DefaultTokenServices();
		result.setTokenStore(tokenStore());
		result.setSupportRefreshToken(false);
		result.setAccessTokenValiditySeconds(4000);
		result.setClientDetailsService(clientDetailsService());
		return result;
	}
	
	@Bean
	public AccessDecisionManager accessDecisionManager() {
		AuthenticatedVoter voter = new AuthenticatedVoter();
		return new UnanimousBased(Arrays.asList(voter));
	}
	
	@Bean
	public OAuth2RequestFactory oAuth2AuthorizationRequestManager() throws Exception {
		return new DefaultOAuth2RequestFactory(clientDetailsService());
	}
	
	@Bean
	public TokenGranter oauth2TokenGranter(@Qualifier("basicAuthenticationManager") AuthenticationManager basicAuthenticationManager) throws Exception {
		return new ResourceOwnerPasswordTokenGranter(basicAuthenticationManager, tokenServices(), clientDetailsService(), oAuth2AuthorizationRequestManager());
	}
	
	@Bean
	public TokenEndpoint oauth2TokenEndpoint(@Qualifier("oauth2TokenGranter") TokenGranter tokenGranter) throws Exception {
		TokenEndpoint tokenEndpoint = new TokenEndpoint();
		tokenEndpoint.setClientDetailsService(clientDetailsService());
		tokenEndpoint.setTokenGranter(tokenGranter);
		return tokenEndpoint;
	}
	
	@Bean
	public FrameworkEndpointHandlerMapping oauth2HandlerMapping() {
		Map<String, String> mappings = new HashMap<>();
		mappings.put("/oauth/token", "/api/v1/oauth/token");
		
		FrameworkEndpointHandlerMapping handlerMapping = new FrameworkEndpointHandlerMapping();
		handlerMapping.setMappings(mappings);
		return handlerMapping;
	}
	
	@Bean
	public AccessDeniedHandler oauthAccessDeniedHandler() {
		return new OAuth2AccessDeniedHandler();
	}
	
	@Bean
	public AuthenticationEntryPoint clientAuthenticationEntryPoint() {
		OAuth2AuthenticationEntryPoint result = new OAuth2AuthenticationEntryPoint();
		result.setRealmName("Runtime");
		result.setTypeName("Basic");
		// TODO [note] to customize error messages, set an exception translator that translates 
		// AuthenticationException and/or some of its subclasses
		// result.setExceptionTranslator(exceptionTranslator);
		return result;
	}
	
	@Bean
	public AuthenticationEntryPoint oauthAuthenticationEntryPoint() {
		OAuth2AuthenticationEntryPoint result = new OAuth2AuthenticationEntryPoint();
		result.setRealmName("Runtime/Client");
		return result;
	}
	
	@Bean
	public Filter resourceServerFilter() throws Exception {
		OAuth2AuthenticationManager authenticationManager = new OAuth2AuthenticationManager();
		authenticationManager.setTokenServices(tokenServices());
		
		OAuth2AuthenticationProcessingFilter filter = new OAuth2AuthenticationProcessingFilter();
		filter.setAuthenticationManager(authenticationManager);
		
		return filter;
	}
	
	@Configuration
	@Order(10)
	public static class BasicConfig extends WebSecurityConfigurerAdapter {
		
		@Autowired
		@Qualifier("basicAuthenticationManager")
		private AuthenticationManager authenticationManager;
		
		@Autowired
		private AccessDecisionManager accessDecisionManager;
		
// TODO [note] with this method we can specify a different authentication manager for this filter chain
//		@Override
//		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//			auth.inMemoryAuthentication()
//					.withUser("petru").password("123").authorities("ROLE_USER");
//		}
		
// TODO [note] with this method we can specify which authentication manager to use in this filter chain
//		       when the authentication manager is shared
		@Override
		protected AuthenticationManager authenticationManager() throws Exception {
			return authenticationManager;
		}
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
// TODO [note] this uses expressions, which are not recognized by AuthenticatedVoter
//			http
//				.antMatcher("/basic/**")
//				.authorizeRequests()
//					.accessDecisionManager(accessDecisionManager())
//					.antMatchers("/**")
//						.fullyAuthenticated()
//					.and()
//				.httpBasic();
			
			http
				.antMatcher("/basic/**")
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and()
				.apply(new UrlAuthorizationConfigurer<>()).getRegistry()
					.accessDecisionManager(accessDecisionManager)
					.antMatchers("/**")
						.access(AuthenticatedVoter.IS_AUTHENTICATED_FULLY)
					.and()
				.httpBasic();
		}

	}
	
	@Configuration
	@Order(20)
	public static class FormLoginConfig extends WebSecurityConfigurerAdapter {
		
		@Autowired
		@Qualifier("basicAuthenticationManager")
		private AuthenticationManager authenticationManager;
		
		@Autowired
		private AccessDecisionManager accessDecisionManager;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.antMatcher("/form-login/**")
				.csrf().disable()
				.apply(new UrlAuthorizationConfigurer<>()).getRegistry()
					.accessDecisionManager(accessDecisionManager)
					.antMatchers("/**")
						.access(AuthenticatedVoter.IS_AUTHENTICATED_FULLY)
					.and()
				.formLogin()
					.loginPage("/resources/login.html")
					.loginProcessingUrl("/form-login/login")
					.usernameParameter("username")
					.passwordParameter("password")
					.defaultSuccessUrl("/form-login", true)
					.and()
				.logout()
					.logoutUrl("/form-login/logout");
		}
		
		@Override
		protected AuthenticationManager authenticationManager() throws Exception {
			return authenticationManager;
		}
		
	}
	
	@Configuration
	@Order(30)
	public static class OAuthFilterChain extends WebSecurityConfigurerAdapter {
		
		@Autowired
		@Qualifier("oauthAccessDeniedHandler")
		private AccessDeniedHandler accessDeniedHandler;
		
		@Autowired
		@Qualifier("clientAuthenticationEntryPoint")
		private AuthenticationEntryPoint authenticationEntryPoint;
		
		@Autowired
		@Qualifier("clientAuthenticationManager")
		private AuthenticationManager authenticationManager;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.antMatcher("/api/v1/oauth/token")
				.csrf().disable()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and()
				.apply(new UrlAuthorizationConfigurer<>()).getRegistry()
					.antMatchers("/**").access(AuthenticatedVoter.IS_AUTHENTICATED_FULLY)
					.and()
				.anonymous().disable()
				.exceptionHandling()
					.accessDeniedHandler(accessDeniedHandler)
					.and()
				.httpBasic()
					.authenticationEntryPoint(authenticationEntryPoint);
		}
		
		@Override
		protected AuthenticationManager authenticationManager() throws Exception {
			return authenticationManager;
		}
	}
	
	@Configuration
	@Order(40)
	public static class RestFilterChain extends WebSecurityConfigurerAdapter {
		
		@Autowired
		@Qualifier("clientAuthenticationManager")
		private AuthenticationManager clientAuthenticationManager;
		
		@Autowired
		@Qualifier("accessDecisionManager")
		private AccessDecisionManager accessDecisionManager;
		
		@Autowired
		@Qualifier("oauthAuthenticationEntryPoint")
		private AuthenticationEntryPoint oauthAuthenticationEntryPoint;
		
		@Autowired
		@Qualifier("resourceServerFilter")
		private Filter resourceServerFilter;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.antMatcher("/api/v1/**")
				.csrf().disable()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and()
				.addFilterBefore(resourceServerFilter, AbstractPreAuthenticatedProcessingFilter.class)
				.apply(new UrlAuthorizationConfigurer<>()).getRegistry()
					.accessDecisionManager(accessDecisionManager)
					.antMatchers("/**").access(AuthenticatedVoter.IS_AUTHENTICATED_FULLY)
					.and()
				.exceptionHandling()
					.authenticationEntryPoint(oauthAuthenticationEntryPoint);
				
		}
		
		@Override
		protected AuthenticationManager authenticationManager() throws Exception {
			return clientAuthenticationManager;
		}
	}
	
}

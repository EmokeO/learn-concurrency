package ro.galanton.learn.spring.webmvc.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	private MainService mainService;

// TODO [note] this does not work because ChildService is not defined in the root context
//	@Autowired
//	private ChildService childSerivce;
	
	@Autowired
	public MainController(MainService mainService) {
		System.out.println("### main controller created");
		this.mainService = mainService;
	}
	
	@RequestMapping("/test")
	public void test() {
		System.out.println("### main");
		mainService.test();
	}
	
}

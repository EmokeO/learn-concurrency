package ro.galanton.learn.concurrency;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by Emőke on 12.03.2017.
 */
public class AtomicIntegerFieldUpdaterTest {

    @Test
    public void test_setter(){
        AtomicIntegerFieldUpdater<IntFieldHolder> fieldUpdater = AtomicIntegerFieldUpdater.newUpdater(IntFieldHolder.class, "intField");

        IntFieldHolder intFieldHolder = new IntFieldHolder(0);
        assertThat(intFieldHolder.getIntField(), is(0));

        fieldUpdater.set(intFieldHolder, 3);
        assertThat(intFieldHolder.getIntField(), is(3));
    }

    @Test
    public void test_basic_operations(){
        AtomicIntegerFieldUpdater<IntFieldHolder> fieldUpdater = AtomicIntegerFieldUpdater.newUpdater(IntFieldHolder.class, "intField");

        IntFieldHolder intFieldHolder = new IntFieldHolder(0);
        assertThat(intFieldHolder.getIntField(), is(0));

        fieldUpdater.addAndGet(intFieldHolder, 1);
        assertThat(intFieldHolder.getIntField(), is(1));

        Boolean fieldWasUpdated = fieldUpdater.compareAndSet(intFieldHolder, 1,2);
        assertThat(fieldWasUpdated, is(true));
        assertThat(intFieldHolder.getIntField(), is(2));

        fieldWasUpdated = fieldUpdater.compareAndSet(intFieldHolder, 1,3);
        assertThat(fieldWasUpdated, is(false));
        assertThat(intFieldHolder.getIntField(), is(2));

        int updatedValue = fieldUpdater.decrementAndGet(intFieldHolder);
        assertThat(updatedValue, is(1));

        updatedValue = fieldUpdater.get(intFieldHolder);
        assertThat(updatedValue, is(1));

        int prevValue = fieldUpdater.getAndAdd(intFieldHolder,2);
        assertThat(prevValue, is(1));

        prevValue = fieldUpdater.getAndDecrement(intFieldHolder);
        assertThat(prevValue, is(3));
        assertThat(intFieldHolder.getIntField(), is(2));

        prevValue = fieldUpdater.getAndIncrement(intFieldHolder);
        assertThat(prevValue, is(2));
        assertThat(intFieldHolder.getIntField(), is(3));

        prevValue = fieldUpdater.getAndSet(intFieldHolder, 5);
        assertThat(prevValue, is(3));
        assertThat(intFieldHolder.getIntField(), is(5));

        updatedValue = fieldUpdater.incrementAndGet(intFieldHolder);
        assertThat(updatedValue, is(6));

    };
}

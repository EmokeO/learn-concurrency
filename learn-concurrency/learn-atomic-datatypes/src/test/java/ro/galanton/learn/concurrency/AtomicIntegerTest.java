package ro.galanton.learn.concurrency;


import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by Emőke on 05.02.2017.
 */

//for Long the same test case can be used
public class AtomicIntegerTest {

 //   compareAndSet
   @Test
   public void set_value_only_if_curent_value_equals_to_expected(){
      AtomicInteger atomicInteger = new AtomicInteger(0);

      Boolean valueWasUpdated = atomicInteger.compareAndSet(0,1);
      assertThat(valueWasUpdated, is(true));
      assertThat(atomicInteger.get(), is(1));

      valueWasUpdated = atomicInteger.compareAndSet(0,2);
      assertThat(valueWasUpdated, is(false));
      assertThat(atomicInteger.get(), is(1));
   }

   @Test
   public void test_non_atomic_integer_increment_fails() throws InterruptedException{
      Calculator calc = new Calculator();
      calc.enter(10);

      IntegerWorker integerWorker1 = new IntegerWorker(1, calc, 60000, 60000);
      IntegerWorker integerWorker2 = new IntegerWorker(2, calc, 50000, 50000);
      IntegerWorker integerWorker3 = new IntegerWorker(3, calc, 90000, 90000);
      IntegerWorker integerWorker4 = new IntegerWorker(4, calc, 50000, 50000);
      IntegerWorker integerWorker5 = new IntegerWorker(5, calc, 15000, 15000);
      IntegerWorker integerWorker6 = new IntegerWorker(6, calc, 30000, 30000);

      integerWorker1.start();
      integerWorker2.start();
      integerWorker3.start();
      integerWorker4.start();
      integerWorker5.start();
      integerWorker6.start();

      integerWorker1.join();
      integerWorker2.join();
      integerWorker3.join();
      integerWorker4.join();
      integerWorker5.join();
      integerWorker6.join();

      System.out.println("The result was " + calc.equals() + " expected is 10");
      //not using thread safe atomic operations multiple threads may modify the value in the same time
      assertThat(calc.equals(), not(10));

   }

   @Test
   public void test_atomic_integer_increment_always_correct() throws InterruptedException{
      AtomicCalculator calc = new AtomicCalculator();
      calc.enter(10);

      IntegerWorker integerWorker1 = new IntegerWorker(1, calc, 60000, 60000);
      IntegerWorker integerWorker2 = new IntegerWorker(2, calc, 50000, 50000);
      IntegerWorker integerWorker3 = new IntegerWorker(3, calc, 90000, 90000);
      IntegerWorker integerWorker4 = new IntegerWorker(4, calc, 50000, 50000);
      IntegerWorker integerWorker5 = new IntegerWorker(5, calc, 15000, 15000);
      IntegerWorker integerWorker6 = new IntegerWorker(6, calc, 30000, 30000);

      integerWorker1.start();
      integerWorker2.start();
      integerWorker3.start();
      integerWorker4.start();
      integerWorker5.start();
      integerWorker6.start();

      integerWorker1.join();
      integerWorker2.join();
      integerWorker3.join();
      integerWorker4.join();
      integerWorker5.join();
      integerWorker6.join();

      System.out.println("The result was " + calc.equals() + " expected is 10");
      //using atomic integer the result is always consistent
      assertThat(calc.equals(), is(10));

   }

}

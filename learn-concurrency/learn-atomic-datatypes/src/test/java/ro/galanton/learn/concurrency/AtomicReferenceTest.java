package ro.galanton.learn.concurrency;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by Emőke on 10.02.2017.
 */
public class AtomicReferenceTest {

    @Test
    public void test_set_value_only_if_curent_value_equals_to_expected(){
        AtomicReference<String> atomicReference = new AtomicReference<String>("HelloWorld");

        Boolean valueWasUpdated = atomicReference.compareAndSet("HelloWorld", "HelloPeople");
        assertThat(valueWasUpdated, is(true));
        assertThat(atomicReference.get(), is("HelloPeople"));

        valueWasUpdated = atomicReference.compareAndSet("HelloWorld", "fooBar");
        assertThat(valueWasUpdated, is(false));
        assertThat(atomicReference.get(), is("HelloPeople"));
    }

    @Test
    public void test_get_and_set(){
        AtomicReference<String> atomicReference = new AtomicReference<String>("HelloWorld");

        String oldValue = atomicReference.getAndSet( "HelloPeople");
        assertThat(oldValue, is("HelloWorld"));
        assertThat(atomicReference.get(), is("HelloPeople"));

    }

    //lazy set, weekCompareAndSet??

}

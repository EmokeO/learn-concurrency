package ro.galanton.learn.concurrency;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicMarkableReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Emőke on 12.03.2017.
 */
public class AtomicMarkableReferenceTest {

    @Test
    public void test_attempt_mark(){
        AtomicMarkableReference<String> markableReference = new AtomicMarkableReference<String>("Test", false);

        Boolean markingSucceded = markableReference.attemptMark("Test", true);
        assertThat(markingSucceded, is(true));
        assertThat(markableReference.isMarked(), is(true));

        markingSucceded = markableReference.attemptMark("Test123", false);
        assertThat(markingSucceded, is(false));
        assertThat(markableReference.isMarked(), is(true));
    }

    @Test
    public void test_compare_and_set(){
        AtomicMarkableReference<String> markableReference = new AtomicMarkableReference<String>("TestTestTest", true);
        assertThat(markableReference.isMarked(), is(true));
        assertThat(markableReference.getReference(), is("TestTestTest"));

        markableReference.set("Test", false);
        assertThat(markableReference.isMarked(), is(false));
        assertThat(markableReference.getReference(), is("Test"));

        Boolean markingSucceded = markableReference.compareAndSet("Test1", "TestNew", true, true);
        assertThat(markingSucceded, is(false));
        assertThat(markableReference.isMarked(), is(false));
        assertThat(markableReference.getReference(), is("Test"));

        markingSucceded = markableReference.compareAndSet("Test", "TestNew", true, true);
        assertThat(markingSucceded, is(false));
        assertThat(markableReference.isMarked(), is(false));
        assertThat(markableReference.getReference(), is("Test"));

        markingSucceded = markableReference.compareAndSet("Test1", "TestNew", false, true);
        assertThat(markingSucceded, is(false));
        assertThat(markableReference.isMarked(), is(false));
        assertThat(markableReference.getReference(), is("Test"));

        markingSucceded = markableReference.compareAndSet("Test", "TestNew", false, true);
        assertThat(markingSucceded, is(true));
        assertThat(markableReference.isMarked(), is(true));
        assertThat(markableReference.getReference(), is("TestNew"));

    }

    @Test
    public void get_mark_and_reference(){
        AtomicMarkableReference<String> markableReference = new AtomicMarkableReference<String>("Test", false);

        boolean[] holder = new boolean[1];
        String reference = markableReference.get(holder);

        assertThat(holder[0], is(false));
        assertThat(reference, is("Test"));
    }
}

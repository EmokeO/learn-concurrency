package ro.galanton.learn.concurrency;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicMarkableReference;
import java.util.concurrent.atomic.AtomicStampedReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Emőke on 13.03.2017.
 */
public class AtomicStampedReferenceTest {

    @Test
    public void test_attempt_mark(){
        AtomicStampedReference<String> stampedReference = new AtomicStampedReference("Test", 0);

        Boolean stampSucceded = stampedReference.attemptStamp("Test", 2);
        assertThat(stampSucceded, is(true));
        assertThat(stampedReference.getStamp(), is(2));

        stampSucceded = stampedReference.attemptStamp("Test123", 4);
        assertThat(stampSucceded, is(false));
        assertThat(stampedReference.getStamp(), is(2));
    }


    @Test
    public void test_compare_and_set(){
        AtomicStampedReference<String> stampedReference = new AtomicStampedReference<String>("TestTestTest", 0);
        assertThat(stampedReference.getStamp(), is(0));
        assertThat(stampedReference.getReference(), is("TestTestTest"));

        stampedReference.set("Test", 1);
        assertThat(stampedReference.getStamp(), is(1));
        assertThat(stampedReference.getReference(), is("Test"));

        Boolean markingSucceded = stampedReference.compareAndSet("Test1", "TestNew", 2, 10);
        assertThat(markingSucceded, is(false));
        assertThat(stampedReference.getStamp(), is(1));
        assertThat(stampedReference.getReference(), is("Test"));

        markingSucceded = stampedReference.compareAndSet("Test", "TestNew", 2, 10);
        assertThat(markingSucceded, is(false));
        assertThat(stampedReference.getStamp(), is(1));
        assertThat(stampedReference.getReference(), is("Test"));

        markingSucceded = stampedReference.compareAndSet("Test1", "TestNew", 1, 10);
        assertThat(markingSucceded, is(false));
        assertThat(stampedReference.getStamp(), is(1));
        assertThat(stampedReference.getReference(), is("Test"));

        markingSucceded = stampedReference.compareAndSet("Test", "TestNew", 1, 10);
        assertThat(markingSucceded, is(true));
        assertThat(stampedReference.getStamp(), is(10));
        assertThat(stampedReference.getReference(), is("TestNew"));

    }

    @Test
    public void get_mark_and_reference(){
        AtomicStampedReference<String> markableReference = new AtomicStampedReference<String>("Test", 0);

        int[] holder = new int[1];
        String reference = markableReference.get(holder);

        assertThat(holder[0], is(0));
        assertThat(reference, is("Test"));
    }

}

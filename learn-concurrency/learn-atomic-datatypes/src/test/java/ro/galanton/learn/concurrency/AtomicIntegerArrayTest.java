package ro.galanton.learn.concurrency;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static ro.galanton.learn.concurrency.matchers.Matchers.allElementsAreEqualTo;

/**
 * Created by Emőke on 05.02.2017.
 */

//The AtomicIntegerArray class may be very useful when there is need to update simultaneously the elements of an array.


// As usage it prevents the same problem - changing the value of elements as the AtomicInteger but these classes are
// likely to be more efficient on memory and nicer to the garbage collector, since the underlying array is a single
// object rather than several individual AtomicInteger (etc) objects.
public class AtomicIntegerArrayTest {

   @Test(expected = AssertionError.class)
   public void test_non_atomic_integer_array_increment_fails() throws InterruptedException{
      Calculator calc = new Calculator();
      int size = 500;
      boolean sync = false;
      int[] array = new int[size];
      calc.enter(array);

      IntegerArrayWorker integerExecutorThread1 = new IntegerArrayWorker(1, calc, size, sync,60000, 60000);
      IntegerArrayWorker integerExecutorThread2 = new IntegerArrayWorker(2, calc, size, sync,50000, 50000);
      IntegerArrayWorker integerExecutorThread3 = new IntegerArrayWorker(3, calc, size, sync,90000, 90000);
      IntegerArrayWorker integerExecutorThread4 = new IntegerArrayWorker(4, calc, size, sync,50000, 50000);
      IntegerArrayWorker integerExecutorThread5 = new IntegerArrayWorker(5, calc, size, sync,15000, 15000);
      IntegerArrayWorker integerExecutorThread6 = new IntegerArrayWorker(6, calc, size, sync,30000, 30000);

      integerExecutorThread1.start();
      integerExecutorThread2.start();
      integerExecutorThread3.start();
      integerExecutorThread4.start();
      integerExecutorThread5.start();
      integerExecutorThread6.start();

      integerExecutorThread1.join();
      integerExecutorThread2.join();
      integerExecutorThread3.join();
      integerExecutorThread4.join();
      integerExecutorThread5.join();
      integerExecutorThread6.join();


      assertThat(calc.getArray(), allElementsAreEqualTo(0));
   }

   @Test
   public void test_sync_non_atomic_integer_array_increment_passes() throws InterruptedException{
      Calculator calc = new Calculator();
      int size = 500;
      boolean sync = true;
      int[] array = new int[size];
      calc.enter(array);

      IntegerArrayWorker integerExecutorThread1 = new IntegerArrayWorker(1, calc, size, sync, 60000, 60000);
      IntegerArrayWorker integerExecutorThread2 = new IntegerArrayWorker(2, calc, size, sync,50000, 50000);
      IntegerArrayWorker integerExecutorThread3 = new IntegerArrayWorker(3, calc, size, sync,90000, 90000);
      IntegerArrayWorker integerExecutorThread4 = new IntegerArrayWorker(4, calc, size, sync,50000, 50000);
      IntegerArrayWorker integerExecutorThread5 = new IntegerArrayWorker(5, calc, size, sync,15000, 15000);
      IntegerArrayWorker integerExecutorThread6 = new IntegerArrayWorker(6, calc, size, sync,30000, 30000);

      integerExecutorThread1.start();
      integerExecutorThread2.start();
      integerExecutorThread3.start();
      integerExecutorThread4.start();
      integerExecutorThread5.start();
      integerExecutorThread6.start();

      integerExecutorThread1.join();
      integerExecutorThread2.join();
      integerExecutorThread3.join();
      integerExecutorThread4.join();
      integerExecutorThread5.join();
      integerExecutorThread6.join();


      assertThat(calc.getArray(), allElementsAreEqualTo(0));
   }

   @Test
   public void test_atomic_integer_array_increment_always_passes() throws InterruptedException{
      AtomicCalculator calc = new AtomicCalculator();
      int size = 500;
      boolean sync = false;
      int[] array = new int[size];
      calc.enter(array);

      IntegerArrayWorker integerExecutorThread1 = new IntegerArrayWorker(1, calc, size, sync, 60000, 60000);
      IntegerArrayWorker integerExecutorThread2 = new IntegerArrayWorker(2, calc, size, sync,50000, 50000);
      IntegerArrayWorker integerExecutorThread3 = new IntegerArrayWorker(3, calc, size, sync,90000, 90000);
      IntegerArrayWorker integerExecutorThread4 = new IntegerArrayWorker(4, calc, size, sync,50000, 50000);
      IntegerArrayWorker integerExecutorThread5 = new IntegerArrayWorker(5, calc, size, sync,15000, 15000);
      IntegerArrayWorker integerExecutorThread6 = new IntegerArrayWorker(6, calc, size, sync,30000, 30000);

      integerExecutorThread1.start();
      integerExecutorThread2.start();
      integerExecutorThread3.start();
      integerExecutorThread4.start();
      integerExecutorThread5.start();
      integerExecutorThread6.start();

      integerExecutorThread1.join();
      integerExecutorThread2.join();
      integerExecutorThread3.join();
      integerExecutorThread4.join();
      integerExecutorThread5.join();
      integerExecutorThread6.join();


      assertThat(calc.getArray(), allElementsAreEqualTo(0));
   }
}

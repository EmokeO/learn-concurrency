package ro.galanton.learn.concurrency;

/**
 * Created by Emőke on 05.02.2017.
 */
public interface ICalculator {

    public void enter(int x);

    public void add(int x);

    public void substract(int x);

    public int equals();

    public void enter(int[] array);

    public void incrementElement(int index, boolean sync);

    public void decrementElement(int index, boolean sync);

    public int[] getArray();
}

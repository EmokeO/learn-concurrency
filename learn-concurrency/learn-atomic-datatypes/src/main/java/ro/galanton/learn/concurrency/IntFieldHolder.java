package ro.galanton.learn.concurrency;

/**
 * Created by Emőke on 12.03.2017.
 */
public class IntFieldHolder {

    volatile int intField;

    public IntFieldHolder(int currentValue){
        intField = currentValue;
    }

    public int getIntField() {
        return intField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }
}


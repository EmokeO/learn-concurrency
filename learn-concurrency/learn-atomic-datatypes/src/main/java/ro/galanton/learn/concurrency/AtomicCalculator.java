package ro.galanton.learn.concurrency;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class AtomicCalculator implements ICalculator {

	private AtomicInteger atomicInteger;
	private AtomicIntegerArray atomicIntegerArray;

	public AtomicCalculator(){
		atomicInteger = new AtomicInteger();
	}

	public void enter(int x) {
		atomicInteger.set(x);
	}

	public void enter(int[] array) {

		atomicIntegerArray = new AtomicIntegerArray(array);
	}


	
	public void add(int x) {
		atomicInteger.addAndGet(x);
	}

	public void substract(int x) {
		atomicInteger.addAndGet(-x);
	}

	public int equals() {
		return atomicInteger.get();
	}

	public synchronized void incrementElement(int index, boolean sync){

		atomicIntegerArray.incrementAndGet(index);
	}

	public synchronized void decrementElement(int index, boolean sync){
		atomicIntegerArray.decrementAndGet(index);
	}

	public int[] getArray(){
		int[] array = new int[atomicIntegerArray.length()];
		for (int i=0; i< atomicIntegerArray.length(); i++){
			array[i] = atomicIntegerArray.get(i);
		}
		return array;
	};
}

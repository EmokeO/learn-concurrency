package ro.galanton.learn.concurrency;

/**
 * Created by Emőke on 05.02.2017.
 */
public class IntegerWorker extends Thread{

    int threadId;
    ICalculator calculator;
    int incrementIterationNumber;
    int decrementIterationNumber;

    public IntegerWorker(int threadId, ICalculator calculator, int incrementIterationNumber, int decrementIterationNumber) {
        this.threadId = threadId;
        this.calculator = calculator;
        this.incrementIterationNumber = incrementIterationNumber;
        this.decrementIterationNumber = decrementIterationNumber;
    }


    public void run() {
        for (int i = 0; i< incrementIterationNumber; i++) {
            calculator.add(1);
//            System.out.println(threadId + "+1");
        }
        for (int i = 0; i< decrementIterationNumber; i++) {
            calculator.substract(1);
//            System.out.println(threadId + "-1");
        }
    }
}

package ro.galanton.learn.concurrency.matchers;

import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;

/**
 * Created by Emőke on 10.02.2017.
 */
public class AllElementsAreEqualTo extends DiagnosingMatcher<int[]>{

    private int equalTo;

    public AllElementsAreEqualTo(int equalTo){
        this.equalTo = equalTo;
    }

    protected boolean matches(Object array, Description mismatchDescription) {
        int[] arr = (int[]) array;
        boolean result = true;
        for( int i = 0; i< arr.length; i++) {
            if (arr[i] != equalTo) {
                mismatchDescription.appendText("Element was: " + arr[i] + " expected was: " + equalTo + "\n");
                result = false;
            }
        }
        return result;
    }

    public void describeTo(Description description) {

    }
}

package ro.galanton.learn.concurrency.matchers;

import org.hamcrest.Matcher;
/**
 * Created by Emőke on 10.02.2017.
 */
public class Matchers {

    public static Matcher<int[]> allElementsAreEqualTo(int equalTo) {
        return new AllElementsAreEqualTo(equalTo);
    }
}

package ro.galanton.learn.concurrency;

import com.sun.org.apache.xalan.internal.xsltc.util.IntegerArray;

public class Calculator implements ICalculator{

	private int value;
	private IntegerArray integerArray;
	
	public void enter(int x) {
		this.value = x;
	}

	public void enter(int[] array){
		integerArray = new IntegerArray(array);
	}
	
	public void add(int x) {
		//non atomic operation
		value = value + x;
	}

	public void substract(int x) {
		//non atomic operation
		value = value - x;
	}

	public int equals() {
		return value;
	}


	public void incrementElement(int index, boolean sync){
		if (sync) {
			synchronized (this) {
				integerArray.set(index, integerArray.at(index) + 1);
			}
		} else {
			integerArray.set(index, integerArray.at(index) + 1);
		}

	}

	public void decrementElement(int index, boolean sync){

		if (sync) {
			synchronized (this ) {
				integerArray.set(index, integerArray.at(index) - 1);
			}
		}
		else {
			integerArray.set(index, integerArray.at(index) - 1);
		}
	}

	public int[] getArray(){
		return integerArray.toIntArray();
	};
}

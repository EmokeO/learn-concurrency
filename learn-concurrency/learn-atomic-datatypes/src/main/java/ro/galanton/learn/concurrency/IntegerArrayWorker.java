package ro.galanton.learn.concurrency;

/**
 * Created by Emőke on 05.02.2017.
 */
public class IntegerArrayWorker extends Thread{

    int threadId;
    ICalculator calculator;
    int size;
    boolean sync;
    int incrementIterationNumber;
    int decrementIterationNumber;

    public IntegerArrayWorker(int threadId, ICalculator calculator, int size, boolean sync, int incrementIterationNumber, int decrementIterationNumber) {
        this.threadId = threadId;
        this.calculator = calculator;
        this.sync = sync;
        this.size = size;
        this.incrementIterationNumber = incrementIterationNumber;
        this.decrementIterationNumber = decrementIterationNumber;
    }


    public void run() {
        for (int s = 0 ; s < size; s++) {
            for (int i = 0; i < incrementIterationNumber; i++) {
                calculator.incrementElement(s, sync);
//            System.out.println(threadId + "+1");
            }
            for (int i = 0; i < decrementIterationNumber; i++) {
                calculator.decrementElement(s, sync);
//            System.out.println(threadId + "-1");
            }
        }
    }
}

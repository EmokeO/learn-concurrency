package ro.galanton.learn.quartz.jobs;

import org.quartz.Job;

/**
 * Represents the job which prints a custom message.
 * 
 * <p>
 * Works in tandem with CustomJobFactory. Jobs which are wrapped in proxies (such as those using 
 * @Transactional annotations), should have an interface and the job class should be set to 
 * the interface rather than the implementation. Otherwise, there will be errors when trying to 
 * obtain the prototype-scoped bean by class name.
 * </p>
 * @author Petru Galanton
 */
public interface MessageJob extends Job {

	void setMessage(String message);

}
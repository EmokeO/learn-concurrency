package ro.galanton.learn.quartz.jobs.impl;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ro.galanton.learn.quartz.jobs.MessageJob;
import ro.galanton.learn.quartz.service.DemoService;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MessageJobImpl implements Job, MessageJob, InitializingBean, ApplicationContextAware {

	@SuppressWarnings("unused")
	private String message;
	
	public MessageJobImpl() {
		System.out.println("MessageJobImpl instantiated.");
	}
	
	@Override
	public void setMessage(String message) {
		// this property is no longer set, since we're not longer using SpringBeanJobFactory
		// TODO it would be nice if we could make this work as well, even if we can get the parameter manually from the job execution context
		// see PropertySettingJobFactory, which could be used to achieve this
		this.message = message;
	}

	@Override
	@Transactional
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO set a breakpoint here and inspect the stack to ensure this method is transactional
		
		// this.message is null, but we can get the parameter manually from the context
		String message = context.getMergedJobDataMap().getString("message");
		System.out.println(message);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("ApplicationContextAware works");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("InitializingBean works");
	}
	
	@Autowired
	public void autowiringWorks(DemoService service) {
		service.demo();
	}
}

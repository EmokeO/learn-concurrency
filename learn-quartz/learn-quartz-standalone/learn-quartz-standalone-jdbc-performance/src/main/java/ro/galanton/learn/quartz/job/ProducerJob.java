package ro.galanton.learn.quartz.job;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class ProducerJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			System.out.println("### scheduling consumer job");
			Scheduler consumerScheduler = (Scheduler) context.getScheduler().getContext().get("consumerScheduler");
			
			JobDetail jobDetail = JobBuilder.newJob(ConsumerJob.class).build();
			Trigger trigger = TriggerBuilder.newTrigger()
					.startAt(new Date(System.currentTimeMillis() + 1000))
					.build();
			consumerScheduler.scheduleJob(jobDetail, trigger);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}

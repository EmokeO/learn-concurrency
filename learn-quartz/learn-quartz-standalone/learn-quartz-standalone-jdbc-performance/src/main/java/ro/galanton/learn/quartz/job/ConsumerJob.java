package ro.galanton.learn.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ConsumerJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			Thread.sleep(1000);
			System.out.println("Consumer job executed : " + context.getFireInstanceId());
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}

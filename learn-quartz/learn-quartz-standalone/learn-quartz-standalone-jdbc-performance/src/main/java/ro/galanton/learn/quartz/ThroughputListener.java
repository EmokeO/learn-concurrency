package ro.galanton.learn.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

public class ThroughputListener implements JobListener {

	private int jobsExecuted = 0;
	
	@Override
	public String getName() {
		return "throughput listener";
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public synchronized void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		jobsExecuted++;
	}
	
	public int getJobsExecuted() {
		return jobsExecuted;
	}

}

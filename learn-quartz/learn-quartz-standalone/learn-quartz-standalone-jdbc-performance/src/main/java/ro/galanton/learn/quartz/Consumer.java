package ro.galanton.learn.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class Consumer {

	public static void main(String[] args) throws Exception {
		ThroughputListener listener = new ThroughputListener();
		SchedulerFactory factory = new StdSchedulerFactory("quartz-consumer.properties");
		Scheduler scheduler = factory.getScheduler();
		scheduler.getListenerManager().addJobListener(listener);
		scheduler.start();
		
	}
}

package ro.galanton.learn.quartz;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class Application {

	
	public static void main(String[] args) throws Exception {
		StdSchedulerFactory factory = new StdSchedulerFactory();
		Scheduler scheduler = factory.getScheduler();
		scheduler.start();
		
		JobDetail jobDetail = JobBuilder.newJob(ExampleJob.class).build();
		SimpleTrigger trigger = TriggerBuilder.newTrigger()
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(10).repeatForever())
					.build();
		
		scheduler.scheduleJob(jobDetail, trigger);
		
		Thread.sleep(60_000);
		
		scheduler.shutdown(true);
		
	}
}

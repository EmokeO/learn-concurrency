package ro.galanton.learn.quartz.jobs;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;

public class ForeverRepeatingJob extends AbstractJob  {

	public static final String NAME = "forever-repeating-job";

	public static void schedule(Scheduler scheduler) throws Exception {
		JobKey jobKey = JobKey.jobKey(ForeverRepeatingJob.NAME);
		if (!scheduler.checkExists(jobKey)) {
			JobDetail jobDetail = JobBuilder.newJob(ForeverRepeatingJob.class)
					.withIdentity(ForeverRepeatingJob.NAME)
					.build();
			
			SimpleTrigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(ForeverRepeatingJob.NAME)
					.withSchedule(SimpleScheduleBuilder.simpleSchedule()
							.withIntervalInSeconds(10)
							.repeatForever())
					.build();
			
			scheduler.scheduleJob(jobDetail, trigger);
		}
	}
}

package ro.galanton.learn.quartz.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractJob implements Job {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractJob.class);
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.info("executing job: {}", context.getJobDetail().getKey());
	}

}

package ro.galanton.learn.quartz.jobs;

import java.util.Date;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

public class SelfReschedulingJob extends AbstractJob {

	public static final String NAME = "self-rescheduling-job";
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		super.execute(context);
		try {
			reschedule(context);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}
	
	private void reschedule(JobExecutionContext context) throws Exception {
		Scheduler scheduler = context.getScheduler();
		JobDetail jobDetail = context.getJobDetail();
		
		Trigger trigger = TriggerBuilder.newTrigger()
			.withIdentity(NAME)
			.forJob(jobDetail)
			.startAt(new Date(System.currentTimeMillis() + 7_000))
			.build();
		
		scheduler.rescheduleJob(new TriggerKey(NAME), trigger);
	}
	
	public static void schedule(Scheduler scheduler) throws Exception {
		JobKey jobKey = new JobKey(NAME);
		if (!scheduler.checkExists(jobKey)) {
			JobDetail jobDetail = JobBuilder.newJob(SelfReschedulingJob.class)
					.withIdentity(NAME)
					.build();
			
			Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(NAME)
					.startAt(new Date(System.currentTimeMillis() + 7_000))
					.build();
			
			scheduler.scheduleJob(jobDetail, trigger);
		}
	}
}

package ro.galanton.learn.quartz.jobs;

import java.util.Date;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class OneOffJob extends AbstractJob {

	public static final String NAME = "one-off-job";

	public static void schedule(Scheduler scheduler) throws Exception {
		JobKey jobKey = new JobKey(NAME);
		if (!scheduler.checkExists(jobKey)) {
			JobDetail jobDetail = JobBuilder.newJob(OneOffJob.class)
					.withIdentity(OneOffJob.NAME)
					.build();
			
			Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(OneOffJob.NAME)
					.startAt(new Date(System.currentTimeMillis() + 5_000))
					.build();
			
			scheduler.scheduleJob(jobDetail, trigger);
		}
	}
}

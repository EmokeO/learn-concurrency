package ro.galanton.learn.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import ro.galanton.learn.quartz.jobs.ForeverRepeatingJob;
import ro.galanton.learn.quartz.jobs.NodeCrashingJob;
import ro.galanton.learn.quartz.jobs.OneOffJob;
import ro.galanton.learn.quartz.jobs.SelfReschedulingJob;
import ro.galanton.learn.quartz.listener.LoggingJobListener;
import ro.galanton.learn.quartz.listener.LoggingTriggerListener;

public class Application {

	public static void main(String[] args) throws Exception {
		SchedulerFactory factory = new StdSchedulerFactory();
		Scheduler scheduler = factory.getScheduler();
		scheduler.getListenerManager().addJobListener(new LoggingJobListener());
		scheduler.getListenerManager().addTriggerListener(new LoggingTriggerListener());
		scheduler.start();

		ForeverRepeatingJob.schedule(scheduler);
		OneOffJob.schedule(scheduler);
		SelfReschedulingJob.schedule(scheduler);
		NodeCrashingJob.schedule(scheduler);
		
		Thread.sleep(60_000);
		scheduler.shutdown(true);
	}
	
}

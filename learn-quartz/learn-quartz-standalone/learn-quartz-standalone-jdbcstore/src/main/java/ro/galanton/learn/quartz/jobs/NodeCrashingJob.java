package ro.galanton.learn.quartz.jobs;

import java.util.Date;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class NodeCrashingJob extends AbstractJob {

	private static final String NAME = "node-crashing-job";
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		super.execute(context);
		if ("node1".equals(System.getProperty("node.name"))) {
			System.out.println("##############################################################################");
			System.out.println("####                           CRASH                                      ####");
			System.out.println("##############################################################################");
			Runtime.getRuntime().halt(0);
		}
	}
	
	public static void schedule(Scheduler scheduler) throws Exception {
		JobKey jobKey = new JobKey(NAME);
		if (!scheduler.checkExists(jobKey)) {
			JobDetail jobDetail = JobBuilder.newJob(NodeCrashingJob.class)
					.withIdentity(NAME)
					.requestRecovery(true)
					.build();
			Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(NAME)
					.startAt(new Date(System.currentTimeMillis() + 6_300))
					.build();
			scheduler.scheduleJob(jobDetail, trigger);
		}
	}
}

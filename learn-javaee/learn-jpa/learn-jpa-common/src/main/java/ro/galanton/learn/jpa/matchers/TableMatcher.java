package ro.galanton.learn.jpa.matchers;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class TableMatcher extends TypeSafeDiagnosingMatcher<DatabaseMetaData> {

	private String tableName;

	public TableMatcher(String tableName) {
		this.tableName = tableName;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("has table ").appendValue(tableName);
	}

	@Override
	protected boolean matchesSafely(DatabaseMetaData item, Description mismatchDescription) {
		try (ResultSet rs = item.getTables(null, null, tableName, null)) {
			while (rs.next()) {
				if (tableName.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					return true;
				}
			}
		} catch (SQLException e) {
			throw new AssertionError(e.getMessage(), e);
		}

		mismatchDescription.appendText("table ").appendValue(tableName).appendText(" not found");
		return false;
	}

	public ColumnNameMatcher withColumn(String columnName) {
		return new ColumnNameMatcher(this, "with", tableName, columnName);
	}
	
	public PrimaryKeyMatcher andPrimaryKey(String... columnNamesInOrder) {
		return new PrimaryKeyMatcher(this, tableName, columnNamesInOrder);
	}
}

package ro.galanton.learn.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class JpaTestBase {

	protected static EntityManagerFactory emf;
	protected EntityManager em;
	protected EntityTransaction tx;
	
	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory("mysql");
	}
	
	@AfterClass
	public static void destroy() {
		emf.close();
	}
	
	@Before
	public void setUp() {
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
	}
	
	@After
	public void tearDown() {
		tx.rollback();
		em.close();
	}
}

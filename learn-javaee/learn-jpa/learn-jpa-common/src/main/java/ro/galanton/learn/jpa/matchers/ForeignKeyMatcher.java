package ro.galanton.learn.jpa.matchers;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.core.CombinableMatcher;

public class ForeignKeyMatcher extends CombinableMatcher<DatabaseMetaData>{

	private String tableName;
	private String columnName;
	private String fkTableName;
	private String fkColumnName;
	
	public ForeignKeyMatcher(Matcher<? super DatabaseMetaData> matcher, String tableName, String columnName, String fkTableName, String fkColumnName) {
		super(matcher);
		this.tableName = tableName;
		this.columnName = columnName;
		this.fkTableName = fkTableName;
		this.fkColumnName = fkColumnName;
	}
	
	@Override
	public void describeTo(Description description) {
		super.describeTo(description);
		
		description
			.appendText(" having foreign key to ")
			.appendValue(String.format("%s.%s", fkTableName, fkColumnName));
	}
	
	@Override
	protected boolean matchesSafely(DatabaseMetaData item, Description mismatch) {
		if (!super.matchesSafely(item, mismatch)) {
			return false;
		}
		
		try (ResultSet rs = item.getCrossReference(null, null, fkTableName, null, null, tableName)) {
			while (rs.next()) {
				String parentColumn = rs.getString("PKCOLUMN_NAME");
				String childColumn = rs.getString("FKCOLUMN_NAME");
				if (parentColumn.equalsIgnoreCase(fkColumnName) && childColumn.equalsIgnoreCase(columnName)) {
					return true;
				}
			}
		} catch (SQLException e) {
			throw new AssertionError(e.getMessage(), e);
		}
		
		mismatch
			.appendText(" column ")
			.appendValue(String.format("%s.%s", tableName, columnName))
			.appendText(" did not have a foreign key to ")
			.appendValue(String.format("%s.%s", fkTableName, fkColumnName));
			
		return false;
	}

	public ColumnNameMatcher andColumn(String columnName) {
		return new ColumnNameMatcher(this, "and", tableName, columnName);
	}
	
	public PrimaryKeyMatcher andPrimaryKey(String... columnNamesInOrder) {
		return new PrimaryKeyMatcher(this, tableName, columnNamesInOrder);
	}
	
	public PrimaryKeyMatcher andNoPrimaryKey() {
		return new PrimaryKeyMatcher(this, tableName, new String[0]);
	}
}

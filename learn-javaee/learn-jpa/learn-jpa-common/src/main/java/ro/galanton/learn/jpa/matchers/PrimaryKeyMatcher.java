package ro.galanton.learn.jpa.matchers;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.core.CombinableMatcher;

public class PrimaryKeyMatcher extends CombinableMatcher<DatabaseMetaData> {

	private final String tableName;
	private final List<String> pkColumnsInOrder;
	
	public PrimaryKeyMatcher(Matcher<? super DatabaseMetaData> matcher, String tableName, String[] pkColumnsInOrder) {
		super(matcher);
		this.tableName = tableName;
		this.pkColumnsInOrder = Arrays.asList(pkColumnsInOrder);
	}
	
	@Override
	public void describeTo(Description description) {
		super.describeTo(description);
		description.appendText(" and primary key ").appendValue(pkColumnsInOrder);
	}

	@Override
	protected boolean matchesSafely(DatabaseMetaData item, Description mismatch) {
		if (!super.matchesSafely(item, mismatch)) {
			return false;
		}

		List<String> actualPkColumns = new ArrayList<>();
		try (ResultSet rs = item.getPrimaryKeys(null, null, tableName)) {
			while (rs.next()) {
				String columnName = rs.getString("COLUMN_NAME");
				int keyIndex = rs.getShort("KEY_SEQ") - 1;
				if (actualPkColumns.size() <= keyIndex) {
					fill(actualPkColumns, actualPkColumns.size(), keyIndex);
				}
				actualPkColumns.set(keyIndex, columnName);
			}
		} catch (SQLException e) {
			throw new AssertionError(e.getMessage(), e);
		}
		
		if (!actualPkColumns.equals(pkColumnsInOrder)) {
			mismatch
				.appendText(" primary key was ")
				.appendValue(actualPkColumns);
			
			return false;
		}
		
		return true;
	}
	
	private void fill(List<String> list, int fromIndex, int toIndex) {
		for (int i = fromIndex; i <= toIndex; i++) {
			list.add(i, null);
		}
	}
}

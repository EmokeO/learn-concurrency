package ro.galanton.learn.jpa.matchers;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.core.CombinableMatcher;

public class ColumnNameMatcher extends CombinableMatcher<DatabaseMetaData> {

	private String connector;
	private String tableName;
	private String columnName;
	
	
	public ColumnNameMatcher(Matcher<? super DatabaseMetaData> matcher, String connector, String tableName, String columnName) {
		super(matcher);
		this.connector = String.format(" %s column ", connector);
		this.tableName = tableName;
		this.columnName = columnName;
	}

	@Override
	public void describeTo(Description description) {
		super.describeTo(description);
		description.appendText(connector).appendValue(columnName);
	}

	@Override
	protected boolean matchesSafely(DatabaseMetaData item, Description mismatchDescription) {
		if (!super.matchesSafely(item, mismatchDescription)) {
			return false;
		}
		
		try (ResultSet rs = item.getColumns(null, null, tableName, columnName)) {
			while (rs.next()) {
				if (columnName.equalsIgnoreCase(rs.getString("COLUMN_NAME"))) {
					return true;
				}
			}
		} catch (SQLException e) {
			throw new AssertionError(e.getMessage(), e);
		}
		
		mismatchDescription.appendText("column ").appendValue(columnName).appendText(" not found");
		return false;
	}
	
	public ColumnNameMatcher andColumn(String columnName) {
		return new ColumnNameMatcher(this, "and", tableName, columnName);
	}
	
	public PrimaryKeyMatcher andPrimaryKey(String... columnNamesInOrder) {
		return new PrimaryKeyMatcher(this, tableName, columnNamesInOrder);
	}
	
	public PrimaryKeyMatcher andNoPrimaryKey() {
		return new PrimaryKeyMatcher(this, tableName, new String[0]);
	}

	public ForeignKeyMatcher withForeignKeyTo(String tableName, String columnName) {
		return new ForeignKeyMatcher(this, this.tableName, this.columnName, tableName, columnName);
	}
}

package ro.galanton.learn.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import ro.galanton.learn.jpa.annotation.ExcludePersistenceUnits;
import ro.galanton.learn.jpa.annotation.PersistenceUnitName;
import ro.galanton.learn.jpa.annotation.TestsPersistenceUnits;
import ro.galanton.learn.jpa.model.generated.AutoGenerationStrategy;
import ro.galanton.learn.jpa.model.generated.IdentityGenerationStrategy;
import ro.galanton.learn.jpa.model.generated.NonGenerated;
import ro.galanton.learn.jpa.model.generated.SequenceGenerationStrategy;
import ro.galanton.learn.jpa.model.generated.TableGenerationStrategy;

/**
 * Tests that we can connect to all databases.
 * 
 * @author Petru Galanton
 */
@TestsPersistenceUnits({"mssql", "oracle", "mysql"})
public class KeyGenerationTest {
	
	@ClassRule
	public static EntityManagerFactoryRule entityManagerFactoryRule = new EntityManagerFactoryRule();
	
	@Rule
	public ExcludePersistenceUnitRule testExclusionRule = new ExcludePersistenceUnitRule();
	
	@PersistenceUnit
	public static EntityManagerFactory emf;
	
	@PersistenceUnitName
	public static String persistenceUnit;
	
	private EntityManager em;
	private EntityTransaction tx;
	
	@Before
	public void setUp() {
		// create an entity manager for the current test
		em = emf.createEntityManager();
		
		// start a transaction for the current test
		tx = em.getTransaction();
		tx.begin();
	}
	
	@After
	public void tearDown() {
		tx.rollback();
		em.close();
	}
	
	@Test
	public void testNonGenerated() {
		// setup
		NonGenerated expected = new NonGenerated();
		expected.setId(1);
		expected.setName("petru");
		em.persist(expected);
		em.flush();
		
		// test
		// this actually reads from the 1st level cache
		NonGenerated actual = em.find(NonGenerated.class, 1);
		
		// verify
		// TODO hamcrest
		assertNotNull("entity not found", actual);
		assertEquals("id mismatch", expected.getId(), actual.getId());
		assertEquals("name mismatch", expected.getName(), actual.getName());
	}
	
	@Test
	public void testAutoGenerationStrategy() {
		// setup
		AutoGenerationStrategy expected = new AutoGenerationStrategy();
		expected.setName("petru");
		em.persist(expected);
		em.flush();
		
		// test
		// this actually reads from the 1st level cache
		AutoGenerationStrategy actual = em.find(AutoGenerationStrategy.class, expected.getId());
		
		// verify
		assertNotNull("entity not found", actual);
		assertEquals("id mismatch", expected.getId(), actual.getId());
		assertEquals("name mismatch", expected.getName(), actual.getName());
	}
	
	// TODO oracle 12c + identity strategy works only in Hibernate 5 which has the Oracle12cDialect
	@Test
	@ExcludePersistenceUnits("oracle")
	public void testIdentityGenerationStrategy() {
		// setup
		IdentityGenerationStrategy expected = new IdentityGenerationStrategy();
		expected.setName("petru");
		em.persist(expected);
		em.flush();
		
		// test
		IdentityGenerationStrategy actual = em.find(IdentityGenerationStrategy.class, expected.getId());
		
		// verify
		assertNotNull("entity not found", actual);
		assertEquals("id mismatch", expected.getId(), actual.getId());
		assertEquals("name mismatch", expected.getName(), actual.getName());
	}
	
	// TODO mssql 2012 + sequence generator work only in Hibernate 5
	@Test
	@ExcludePersistenceUnits({"mssql", "mysql"})
	public void testSequenceGenerationStrategy() {
		// setup
		SequenceGenerationStrategy expected = new SequenceGenerationStrategy();
		expected.setName("petru");
		em.persist(expected);
		em.flush();
		
		// test
		SequenceGenerationStrategy actual = em.find(SequenceGenerationStrategy.class, expected.getId());
		
		// verify
		assertNotNull("entity not found", actual);
		assertEquals("id mismatch", expected.getId(), actual.getId());
		assertEquals("name mismatch", expected.getName(), actual.getName());
	}
	
	@Test
	public void testTableGenerationStrategy() {
		// setup
		TableGenerationStrategy expected = new TableGenerationStrategy();
		expected.setName("petru");
		em.persist(expected);
		em.flush();
		
		// test
		TableGenerationStrategy actual = em.find(TableGenerationStrategy.class, expected.getId());
		
		// verify
		assertNotNull("entity not found", actual);
		assertEquals("id mismatch", expected.getId(), actual.getId());
		assertEquals("name mismatch", expected.getName(), actual.getName());
	}
	
}

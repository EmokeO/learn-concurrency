package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @EmbeddedId) which is also a full foreign key.
 */
@Entity
public class Derived2b {

	@EmbeddedId
	private Parent2Key pk;
	
	@MapsId
	@OneToOne
	private Parent2 parent;
	
	private String name;

	public Parent2Key getPk() {
		return pk;
	}

	public void setPk(Parent2Key pk) {
		this.pk = pk;
	}

	public Parent2 getParent() {
		return parent;
	}

	public void setParent(Parent2 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

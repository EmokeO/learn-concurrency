package ro.galanton.learn.jpa.model.composite;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @IdClass) which is also a partial composite foreign key.
 */
@Entity
@IdClass(Derived6aKey.class)
public class Derived6a {
	
	@Id
	@OneToOne
	private Parent6 parent;
	
	@Id
	private long derivedId;

	private String name;

	public Derived6a() {
	}

	public Derived6a(Parent6 parent, long derivedId, String name) {
		super();
		this.parent = parent;
		this.derivedId = derivedId;
		this.name = name;
	}

	public Parent6 getParent() {
		return parent;
	}

	public void setParent(Parent6 parent) {
		this.parent = parent;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

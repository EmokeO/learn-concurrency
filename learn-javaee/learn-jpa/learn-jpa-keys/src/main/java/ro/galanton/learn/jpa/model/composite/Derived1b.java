package ro.galanton.learn.jpa.model.composite;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

/**
 * An entity with a primary key that is also a foreign key. The id is extracted 
 * from the relationship into the @Id field using @MapsId
 */
@Entity
public class Derived1b {

	@Id
	private long id;

	@MapsId
	@OneToOne
	private Parent1 parent;
	
	private String name;

	// not setter for id
	public long getId() {
		return id;
	}

	public Parent1 getParent() {
		return parent;
	}

	public void setParent(Parent1 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}

package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Parent6 {

	@EmbeddedId
	private Parent6Key pk;
	
	private String name;

	public Parent6() {
	}

	public Parent6(Parent6Key pk, String name) {
		super();
		this.pk = pk;
		this.name = name;
	}

	public Parent6Key getPk() {
		return pk;
	}

	public void setPk(Parent6Key pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

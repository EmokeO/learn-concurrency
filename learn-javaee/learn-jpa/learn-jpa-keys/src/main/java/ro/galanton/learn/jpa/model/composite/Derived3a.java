package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @IdClass) which is also a full foreign key.
 */
@Entity
@IdClass(Parent3Key.class)
public class Derived3a implements Serializable {

	// must implement Serializable (bug in Hibernate ??)
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	private Parent3 parent;
	
	private String name;

	public Parent3 getParent() {
		return parent;
	}

	public void setParent(Parent3 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @EmbeddedId) which is also a partial composite foreign key.
 */
@Entity
public class Derived6b {

	@EmbeddedId
	private Derived6bKey pk;
	
	@MapsId("parentId") // parentId = name of field in Derived6bKey
	@OneToOne
	private Parent6 parent;
	
	private String name;

	public Derived6b() {
	}

	public Derived6b(Derived6bKey pk, Parent6 parent, String name) {
		this.pk = pk;
		this.parent = parent;
		this.name = name;
	}

	public Derived6bKey getPk() {
		return pk;
	}

	public void setPk(Derived6bKey pk) {
		this.pk = pk;
	}

	public Parent6 getParent() {
		return parent;
	}

	public void setParent(Parent6 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

public class Derived6aKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private Parent6Key parent; // matches name of @Id field in Derived6a and type of @Id field in Parent6
	
	private long derivedId; // matches name and type of @Id field in Derived6a

	public Derived6aKey() {
	}

	public Derived6aKey(Parent6Key parent, long derivedId) {
		super();
		this.parent = parent;
		this.derivedId = derivedId;
	}

	public Parent6Key getParent() {
		return parent;
	}

	public void setParent(Parent6Key parent) {
		this.parent = parent;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (derivedId ^ (derivedId >>> 32));
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Derived6aKey other = (Derived6aKey) obj;
		if (derivedId != other.derivedId)
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}
	
}

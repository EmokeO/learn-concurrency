package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Derived4bKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private long parentId; // must be same type as @Id of Parent4, can have any name
	
	private long derivedId;

	public Derived4bKey() {
	}

	public Derived4bKey(long parentId, long derivedId) {
		super();
		this.parentId = parentId;
		this.derivedId = derivedId;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (derivedId ^ (derivedId >>> 32));
		result = prime * result + (int) (parentId ^ (parentId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Derived4bKey other = (Derived4bKey) obj;
		if (derivedId != other.derivedId)
			return false;
		if (parentId != other.parentId)
			return false;
		return true;
	}
	
}

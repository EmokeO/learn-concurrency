package ro.galanton.learn.jpa.model.composite;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(IdClassKey.class)
public class IdClassEntity {

	// name and type must be the same as in IdClassKey (JPA2.1Spec - 2.4)
	@Id
	private long id1;
	
	// name and type must be the same as in IdClassKey (JPA2.1Spec - 2.4)
	@Id
	private long id2;
	
	private String name;

	public long getId1() {
		return id1;
	}

	public void setId1(long id1) {
		this.id1 = id1;
	}

	public long getId2() {
		return id2;
	}

	public void setId2(long id2) {
		this.id2 = id2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

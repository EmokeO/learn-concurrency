package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class EmbeddedIdEntity {

	@EmbeddedId
	private EmbeddableCompositeKey pk;
	
	private String name;

	public EmbeddableCompositeKey getPk() {
		return pk;
	}

	public void setPk(EmbeddableCompositeKey pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

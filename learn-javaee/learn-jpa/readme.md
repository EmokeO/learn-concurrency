# Instructions

### Oracle JDBC Driver
Download the Oracle JDBC driver for Oracle 12 (ojdbc7.jar) from: http://www.oracle.com/technetwork/database/features/jdbc/jdbc-drivers-12c-download-1958347.html

Run mvn install:install-file -Dfile=ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar 
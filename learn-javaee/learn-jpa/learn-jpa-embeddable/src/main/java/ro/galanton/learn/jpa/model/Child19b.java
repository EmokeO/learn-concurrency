package ro.galanton.learn.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Child19b {

	@Id
	private long id;
	
	private String name;

	public Child19b() {
	}

	public Child19b(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}

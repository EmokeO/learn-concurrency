package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Child2 {

	private String childName;

	public Child2() {
	}

	public Child2(String childName) {
		super();
		this.childName = childName;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}
	
}

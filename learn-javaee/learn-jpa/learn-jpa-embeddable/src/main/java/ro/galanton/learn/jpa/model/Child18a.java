package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToMany;

@Embeddable
public class Child18a {

	@Column(name = "childName")
	private String name;

	@OneToMany
	private List<Child18b> children;

	public Child18a() {
	}

	public Child18a(String name, List<Child18b> children) {
		super();
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child18b> getChildren() {
		return children;
	}
	
	public void setChildren(List<Child18b> children) {
		this.children = children;
	}
	
}

package ro.galanton.learn.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Child18b {

	@Id
	private long id;
	
	private String name;

	public Child18b() {
	}

	public Child18b(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

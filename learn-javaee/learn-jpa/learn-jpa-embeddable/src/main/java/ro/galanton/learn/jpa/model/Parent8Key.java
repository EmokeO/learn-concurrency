package ro.galanton.learn.jpa.model;

import java.io.Serializable;

public class Parent8Key implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id1;
	private long id2;
	
	public Parent8Key() {
	}

	public Parent8Key(long id1, long id2) {
		this.id1 = id1;
		this.id2 = id2;
	}

	public long getId1() {
		return id1;
	}

	public void setId1(long id1) {
		this.id1 = id1;
	}

	public long getId2() {
		return id2;
	}

	public void setId2(long id2) {
		this.id2 = id2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id1 ^ (id1 >>> 32));
		result = prime * result + (int) (id2 ^ (id2 >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parent8Key other = (Parent8Key) obj;
		if (id1 != other.id1)
			return false;
		if (id2 != other.id2)
			return false;
		return true;
	}
	
}

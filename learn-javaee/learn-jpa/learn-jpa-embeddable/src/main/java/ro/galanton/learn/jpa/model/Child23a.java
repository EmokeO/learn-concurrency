package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.OneToMany;

@Embeddable
public class Child23a {

	private String name;
	
	@OneToMany
	private List<Child23b> children;

	public Child23a() {
	}

	public Child23a(String name, List<Child23b> children) {
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child23b> getChildren() {
		return children;
	}

	public void setChildren(List<Child23b> children) {
		this.children = children;
	}
	
}

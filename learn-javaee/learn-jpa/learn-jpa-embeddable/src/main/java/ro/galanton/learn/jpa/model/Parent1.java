package ro.galanton.learn.jpa.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent1 {

	@Id
	private long id;
	
	private String parentName;
	
	@Embedded
	private Child1 child;

	public Parent1() {
	}

	public Parent1(long id, String name, Child1 child) {
		super();
		this.id = id;
		this.parentName = name;
		this.child = child;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String name) {
		this.parentName = name;
	}

	public Child1 getChild() {
		return child;
	}

	public void setChild(Child1 child) {
		this.child = child;
	}
	
}

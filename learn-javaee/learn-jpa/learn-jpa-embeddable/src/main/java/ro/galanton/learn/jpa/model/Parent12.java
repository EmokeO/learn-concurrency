package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;

@Entity
public class Parent12 {

	@Id
	private long id;
	
	private String name;
	
	@MapKeyColumn(name = "child_key")
	@AttributeOverride(
		name = "value.name",
		column = @Column(name = "child_name")
	)
	@ElementCollection
	@CollectionTable( // for customizing the table
		name = "parent12_children", // custom table name
		joinColumns = @JoinColumn(name = "parent12_id") // customize fk column name
	)
	private Map<String, Child12> children = new HashMap<>();

	public Parent12() {
	}

	public Parent12(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Child12> getChildren() {
		return children;
	}

	public void setChildren(Map<String, Child12> children) {
		this.children = children;
	}
	
}

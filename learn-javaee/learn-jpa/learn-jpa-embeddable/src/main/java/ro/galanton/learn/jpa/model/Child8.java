package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Child8 {

	private String childName;

	public Child8() {
	}

	public Child8(String childName) {
		this.childName = childName;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}
	
}

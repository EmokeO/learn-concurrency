package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@SuppressWarnings("unused")
public class Parent21 {

	@Id
	private long id;
	
	@Column(name = "parentName")
	private String name;
	
	// uncomment @ElementCollection and comment @Transient
	//@ElementCollection
	@Transient
	private List<Child21> children;
	
}

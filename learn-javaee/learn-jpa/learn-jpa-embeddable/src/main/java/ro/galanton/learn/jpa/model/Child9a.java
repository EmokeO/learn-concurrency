package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Embeddable
public class Child9a {

	@Column(name = "child9a_name")
	private String name;
	
	@OneToOne
	@JoinColumn(name = "child9b_id")
	private Child9b child;

	public Child9a() {
	}

	public Child9a(String name, Child9b child) {
		this.name = name;
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child9b getChild() {
		return child;
	}

	public void setChild(Child9b child) {
		this.child = child;
	}
	
}

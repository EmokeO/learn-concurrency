package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class Child24a {

	private String name;
	
	@ManyToOne
	private Child24b child;

	public Child24a() {
	}

	public Child24a(String name, Child24b child) {
		this.name = name;
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child24b getChild() {
		return child;
	}

	public void setChild(Child24b child) {
		this.child = child;
	}
	
}

package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

// implements equals and hashCode because it is used as a map key
@Embeddable
public class Child17a {

	@Column(name = "child_key")
	private String name;

	public Child17a() {
	}

	public Child17a(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child17a other = (Child17a) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}

package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Child7b {

	@Column(name = "child7bName")
	private String name;

	public Child7b() {
	}

	public Child7b(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

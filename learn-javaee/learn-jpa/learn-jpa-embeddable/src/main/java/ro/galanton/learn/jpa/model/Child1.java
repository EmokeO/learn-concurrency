package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Child1 {

	private String childName;

	public Child1() {
	}

	public Child1(String name) {
		super();
		this.childName = name;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String name) {
		this.childName = name;
	}
	
	
}

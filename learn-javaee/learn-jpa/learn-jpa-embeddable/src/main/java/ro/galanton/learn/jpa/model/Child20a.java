package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToMany;

@Embeddable
public class Child20a {

	@Column(name = "childName")
	private String name;
	
	@ManyToMany
	private List<Child20b> children;

	public Child20a() {
	}

	public Child20a(String name, List<Child20b> children) {
		super();
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child20b> getChildren() {
		return children;
	}

	public void setChildren(List<Child20b> children) {
		this.children = children;
	}
	
}

package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
@SuppressWarnings("unused")
public class Parent13 {

	@Id
	private long id;
	
	private String name;
	
	@Column(name = "child_value") // overriden the name of the column which stores the map value
	@ElementCollection
	@CollectionTable(
		name = "parent13_children",
		joinColumns = @JoinColumn(name = "parent13_id")
	)
//	FIXME bug in hibernate ? if this is added then a 'hash' column appears and the @Column annotation is ignored
//	for now, the @Column annotation in @AttributeOverride has been placed directly on Child13.name	
//	@AttributeOverride( 
//		name = "key.name",
//		column = @Column(name = "child_key")
//	)
	private Map<Child13, String> children = new HashMap<>();

	public Parent13() {
	}

	public Parent13(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Child13, String> getChildren() {
		return children;
	}

	public void setChildren(Map<Child13, String> children) {
		this.children = children;
	}
	
}

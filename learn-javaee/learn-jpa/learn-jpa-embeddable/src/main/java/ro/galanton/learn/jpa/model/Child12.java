package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Child12 {

	private String name;

	public Child12() {
	}

	public Child12(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

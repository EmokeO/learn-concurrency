package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent16 {

	@Id
	private long id;
	
	private String parentName;
	
	@ElementCollection
	private Map<Child16, String> children = new HashMap<>();

	public Parent16() {
	}

	public Parent16(long id, String parentName) {
		super();
		this.id = id;
		this.parentName = parentName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Map<Child16, String> getChildren() {
		return children;
	}

	public void setChildren(Map<Child16, String> children) {
		this.children = children;
	}
	
	
}

package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Child17b {

	@Column(name = "child_value")
	private String name;

	public Child17b() {
	}

	public Child17b(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

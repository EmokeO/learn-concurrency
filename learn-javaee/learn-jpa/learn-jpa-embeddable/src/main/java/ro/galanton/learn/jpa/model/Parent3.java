package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent3 {

	@Id
	private long id;
	
	private String parentName;
	
	@ElementCollection
	private List<String> childNames;

	public Parent3() {
	}

	public Parent3(long id, String parentName, List<String> childNames) {
		super();
		this.id = id;
		this.parentName = parentName;
		this.childNames = childNames;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<String> getChildNames() {
		return childNames;
	}

	public void setChildNames(List<String> childNames) {
		this.childNames = childNames;
	}
	
}

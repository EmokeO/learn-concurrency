package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent24 {

	@Id
	private long id;
	
	private String name;
	
	@ElementCollection
	private List<Child24a> children;

	public Parent24() {
	}

	public Parent24(long id, String name, List<Child24a> children) {
		super();
		this.id = id;
		this.name = name;
		this.children = children;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child24a> getChildren() {
		return children;
	}

	public void setChildren(List<Child24a> children) {
		this.children = children;
	}
	
}

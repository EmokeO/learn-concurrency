package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ForwardToNamed extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log("handling request: " + req.getRequestURI());
		
		resp.setContentType("text/plain");
		
		getServletContext().getNamedDispatcher("pageContent").forward(req, resp);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log("handling request: " + req.getRequestURI());
		
		resp.setContentType("text/plain");
		
		getServletContext().getNamedDispatcher("pageContent").forward(req, resp);
	}
}

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		String test = config.getInitParameter("test");
		System.out.println("Parameter 'test' in servlet PageServlet = " + test);
	}
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		
		resp.getWriter().print("header");
		req.getRequestDispatcher("/menu.txt").include(req, resp);
		try {
			getServletContext().getNamedDispatcher("pageContent").include(req, resp);
			req.getRequestDispatcher("/pageContent").include(req, resp);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		resp.getWriter().print(" footer");
		
	}
}

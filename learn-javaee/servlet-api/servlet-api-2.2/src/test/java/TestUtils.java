import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class TestUtils {

	public static String get(String uri) throws Exception {
		URL url = new URL(uri);
		URLConnection con = url.openConnection();
		InputStream in = (InputStream) con.getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String content = "";
		for (;;) {
			String line = reader.readLine();
			if (line == null) break;
			content += line;
		}
		return content;
	}
	
}

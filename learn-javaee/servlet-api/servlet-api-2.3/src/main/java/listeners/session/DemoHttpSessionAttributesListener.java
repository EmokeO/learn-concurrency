package listeners.session;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import util.LogUtil;

public class DemoHttpSessionAttributesListener implements HttpSessionAttributeListener {

	public void attributeAdded(HttpSessionBindingEvent evt) {
		LogUtil.log("attribute " + evt.getName() + " with value " + evt.getValue() + " added to session " + evt.getSession().getId());
	}

	public void attributeRemoved(HttpSessionBindingEvent evt) {
		LogUtil.log("attribute " + evt.getName() + " removed from session " + evt.getSession().getId());
	}

	public void attributeReplaced(HttpSessionBindingEvent evt) {
		LogUtil.log("attribute " + evt.getName() + " replaced in session " + evt.getSession().getId());
	}

}

package listeners.context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import util.Constants;
import util.LogUtil;

public class DemoServletContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent evt) {
		LogUtil.log("context destroyed: " + evt.getServletContext().getServletContextName());
	}

	public void contextInitialized(ServletContextEvent evt) {
		LogUtil.log("context initialized: " + evt.getServletContext().getServletContextName());
		evt.getServletContext().setAttribute(Constants.CONTEXT_INIT_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
	}

}

package listeners.context;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

import util.LogUtil;

public class DemoServletContextAttributesListener implements ServletContextAttributeListener {

	public void attributeAdded(ServletContextAttributeEvent evt) {
		LogUtil.log("attribute added: " + evt.getName());
	}

	public void attributeRemoved(ServletContextAttributeEvent evt) {
		LogUtil.log("attribute removed: " + evt.getName());
	}

	public void attributeReplaced(ServletContextAttributeEvent evt) {
		LogUtil.log("attribute changed: " + evt.getName());
	}

}

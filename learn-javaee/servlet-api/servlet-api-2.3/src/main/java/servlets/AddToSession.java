package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.LogUtil;
import util.MyObject;

public class AddToSession extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("AddToSession executing");
		
		if (req.getSession().getAttribute("sessionMonitor") == null) {
			req.getSession().setAttribute("sessionMonitor", new MyObject());
		}
		
		String name = req.getParameter("name");
		String value = req.getParameter("value");
		req.getSession().setAttribute(name, value);
	}
}

package servlets;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.LogUtil;

public class PageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("PageServlet executing");
		resp.setContentType("text/plain");
		
//		resp.getWriter().print("header");
		getServletContext().getNamedDispatcher("pageContentServlet").include(req, resp);
//		req.getRequestDispatcher("/pageContentServlet").forward(req, resp);
//		resp.getWriter().print(" footer");
	}
}

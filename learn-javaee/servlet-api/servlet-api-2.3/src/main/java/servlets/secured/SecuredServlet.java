package servlets.secured;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.LogUtil;

public class SecuredServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("SecuredServlet doGet");
		
		HttpSession session = req.getSession(false);
		boolean sessionStarted = (session != null); 
		
		String user = req.getRemoteUser();
		boolean isFoo = req.isUserInRole("foo");
		boolean isAdmin = req.isUserInRole("admin");
		
		resp.setContentType("text/plain");
		PrintWriter out = resp.getWriter();
		out.println("user = " + user);
		out.println("is in foo role = " + isFoo);
		out.println("is in admin role = " + isAdmin);
		out.println("session started: " + sessionStarted);
	}
	
}

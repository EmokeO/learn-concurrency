package servlets;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.LogUtil;

/**
 * This servlet is not mapped in web.xml, but PageServlet can still include it. 
 */
public class PageContentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("PageContentServlet handling GET request: " + req.getRequestURI());
		
		resp.getWriter().print(" content get ");
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("PageContentServlet handling POST request: " + req.getRequestURI());
		
		resp.getWriter().print(" content post ");
	}
	
}

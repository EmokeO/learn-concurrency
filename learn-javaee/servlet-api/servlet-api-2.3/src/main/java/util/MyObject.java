package util;

import java.io.Serializable;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

public class MyObject implements HttpSessionActivationListener, Serializable {

	private static final long serialVersionUID = 1L;

	public void sessionDidActivate(HttpSessionEvent evt) {
		LogUtil.log("session activated: " + evt.getSession().getId());
	}

	public void sessionWillPassivate(HttpSessionEvent evt) {
		LogUtil.log("session passivating: " + evt.getSession().getId());
	}

}

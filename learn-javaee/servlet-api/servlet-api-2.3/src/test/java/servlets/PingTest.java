package servlets;

import junit.framework.TestCase;
import util.TestUtils;

public class PingTest extends TestCase {

	public PingTest(String name) {
		super(name);
	}
	
	public void testBigBang() throws Exception {
		String response = TestUtils.get("http://localhost:8080/app/ping");
		assertEquals("pong", response);
	}

}

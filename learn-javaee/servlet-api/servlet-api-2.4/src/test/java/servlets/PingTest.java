package servlets;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import util.TestUtils;

public class PingTest {

	@Test
	public void bigBang() throws Exception {
		String response = TestUtils.get("http://localhost:8080/app/ping");
		assertEquals("pong", response);
	}

}

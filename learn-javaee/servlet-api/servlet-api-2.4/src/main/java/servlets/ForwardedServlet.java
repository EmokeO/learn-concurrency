package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ForwardedServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		getServletContext().log("### forwarded servlet executing");
		
		resp.setContentType("text/plain");
		
		PrintWriter out = resp.getWriter();
		out.println("request uri: " + req.getRequestURI());
		out.println("context path: " + req.getContextPath());
		out.println("servlet path: " + req.getServletPath());
		out.println("path info: " + req.getPathInfo());
		out.println("query string: " + req.getQueryString());
		out.println("javax.servlet.forward.request_uri: " + req.getAttribute("javax.servlet.forward.request_uri"));
		out.println("javax.servlet.forward.context_path: " + req.getAttribute("javax.servlet.forward.context_path"));
		out.println("javax.servlet.forward.servlet_path: " + req.getAttribute("javax.servlet.forward.servlet_path"));
		out.println("javax.servlet.forward.path_info: " + req.getAttribute("javax.servlet.forward.path_info"));
		out.println("javax.servlet.forward.query_string: " + req.getAttribute("javax.servlet.forward.query_string"));
	}
}

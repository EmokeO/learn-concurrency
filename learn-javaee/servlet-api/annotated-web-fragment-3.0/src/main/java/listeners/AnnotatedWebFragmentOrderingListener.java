package listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AnnotatedWebFragmentOrderingListener implements ServletContextListener {

	public AnnotatedWebFragmentOrderingListener() {
		System.out.println("OrderingListener in annotated-web-fragment instantiated");
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}

package listeners;

import java.util.Set;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;
import javax.servlet.annotation.WebListener;

import servlets.AnnotatedWebFragmentServlet;

@WebListener
public class AnnotatedWebFragmentInitializer implements ServletContainerInitializer {

	@Override
	public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
		System.out.println("AnnotatedWebFragmentInitializer executing");
		Dynamic servlet = ctx.addServlet("progAnnotatedWebFragmentServlet", AnnotatedWebFragmentServlet.class);
		servlet.addMapping("/progAnnotatedWebFragmentServlet");
		servlet.setInitParameter("registrationMethod", "programmatically from ServletContainerInitializer");
	}

}

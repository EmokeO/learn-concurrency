package servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
	urlPatterns = "/annotatedWebFragmentServlet",
	loadOnStartup = 1,
	initParams = @WebInitParam(name = "registrationMethod", value = "with annotations")
)
public class AnnotatedWebFragmentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private String registrationMethod;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		this.registrationMethod = config.getInitParameter("registrationMethod");
	}
	
	@Override
	public void init() throws ServletException {
		System.out.println("AnnotatedWebFragmentServlet initialized");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("This is an annotated servlet in web fragment annotated-web-fragment-3.0 registered " + registrationMethod);
	}
}

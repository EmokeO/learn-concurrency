package listeners;

import java.util.Set;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import servlets.Greeting;
import util.LogUtil;

public class Initializer implements ServletContainerInitializer {

	@Override
	public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
		LogUtil.log("Initializer executing");
		Dynamic servlet = ctx.addServlet("initializerGreeting", Greeting.class);
		servlet.setInitParameter("greeting", "Ahoy");
		servlet.addMapping("/initializerGreeting");
	}

}

package async;

public class Job implements Runnable {

	private Monitor monitor;
	private long duration;
	
	public Job(Monitor monitor, long duration) {
		this.monitor = monitor;
		this.duration = duration;
		
		if (monitor != null) {
			monitor.register(this);
		}
	}

	@Override
	public void run() {
		try {
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
		}
		
		
		if (monitor != null) {
			monitor.finished(this);
		}
	}
}

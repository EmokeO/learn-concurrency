package async;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/async", asyncSupported = false)
public class AsyncServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ExecutorService pool = (ExecutorService) getServletContext().getAttribute("pool");
		
		AsyncContext asyncContext = req.startAsync();
		asyncContext.setTimeout(20_000);
		
		Monitor monitor = new Monitor(asyncContext);
		pool.execute(new Job(monitor, 5_000));
		pool.execute(new Job(monitor, 7_000));
		
	}
}

package async;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.AsyncContext;
import javax.servlet.ServletResponse;

public class Monitor {

	private List<Job> jobs = new ArrayList<>();
	private AsyncContext context;
	
	public Monitor(AsyncContext context) {
		this.context = context;
	}
	
	public void register(Job job) {
		this.jobs.add(job);
	}
	
	public void finished(Job job) {
		this.jobs.remove(job);
		
		if (this.jobs.isEmpty()) {
			complete();
		}
	}
	
	private void complete() {
		try {
			ServletResponse resp = context.getResponse();
			resp.setContentType("text/plain");
			
			PrintWriter out = resp.getWriter();
			out.println("Completed!");
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
		
		context.complete();
	}
}

package async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ExecutorService pool = Executors.newFixedThreadPool(2);
		sce.getServletContext().setAttribute("pool", pool);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ExecutorService pool = (ExecutorService) sce.getServletContext().getAttribute("pool");
		pool.shutdown();
	}

}

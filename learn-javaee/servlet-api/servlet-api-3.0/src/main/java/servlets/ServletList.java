package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/")
public class ServletList extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		out.print("<!doctype html>");
		out.print("<html><head></head><body><ol>");
		
		Map<String, ? extends ServletRegistration> servlets = getServletContext().getServletRegistrations();
		for (Entry<String, ? extends ServletRegistration> entry : servlets.entrySet()) {
			String servletName = entry.getKey();
			ServletRegistration registration = entry.getValue();
			Collection<String> mappings = registration.getMappings();
			
			out.print("<li>");
			if (!mappings.isEmpty()) {
				String path = getServletContext().getContextPath() + mappings.iterator().next();
				out.print("<a href='");
				out.print(path);
				out.print("' target='_blank'>");
			}
			out.print(servletName);
			out.print(" of type ");
			out.print(registration.getClassName());
			out.print(" mapped to ");
			out.print(mappings);
			if (!mappings.isEmpty()) {
				out.print("</a>");
			}
			out.print("</li>");
		}
		
		out.print("</ol></body></html>");
	}
}

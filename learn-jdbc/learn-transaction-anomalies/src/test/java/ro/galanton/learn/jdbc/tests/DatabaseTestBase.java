package ro.galanton.learn.jdbc.tests;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.util.DatabaseBuilder;

/**
 * Base class for database integration tests which drops and re-creates the database schema before 
 * each test execution and drops the database schema after each test execution.
 * 
 * @author Petru Galanton
 */
public abstract class DatabaseTestBase {

	@Before
	public void setUp() throws SQLException, IOException {
		new DatabaseBuilder()
			.script("/mssql_drop.sql", true)
			.script("/mssql_create.sql", false)
			.execute();
	}
	
	@After
	public void tearDown() throws SQLException, IOException {
		new DatabaseBuilder()
			.script("/mssql_drop.sql", false)
			.execute();
	}
	
	protected final void execute(ConcurrentTransaction... transactions) throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(transactions.length);
		for (ConcurrentTransaction tx : transactions) {
			executor.submit(tx);
		}
		executor.shutdown();
		
		if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
			fail("test did not terminate in 10 seconds");
		}

		for (ConcurrentTransaction tx : transactions) {
			if (tx.hasException()) {
				throw tx.getException();
			}
		}
	}

}

/**
 * Contains the test classes. Tests can be run either individually or in suites. To run the 
 * tests in suites see the ro.galanton.learn.jdbc.suites package.
 */
package ro.galanton.learn.jdbc.tests;
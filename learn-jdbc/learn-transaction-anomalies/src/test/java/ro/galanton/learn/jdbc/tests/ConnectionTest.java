package ro.galanton.learn.jdbc.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import ro.galanton.learn.jdbc.ConnectionUtil;

/**
 * Tests whether we can connect to the database and execute a query from an integration test.
 * 
 * @author Petru Galanton
 */
public class ConnectionTest extends DatabaseTestBase {

	@Test
	public void can_connect_to_database() throws SQLException {
		try (Connection con = ConnectionUtil.getConnection()) {
			PreparedStatement stmt = con.prepareStatement("SELECT name, amount FROM accounts WHERE id = ?");
			stmt.setInt(1, 1);
			ResultSet rs = stmt.executeQuery();

			assertTrue(rs.next());
			assertEquals("petru", rs.getString(1));
			assertEquals(123, rs.getInt(2));
		}
	}
}

package ro.galanton.learn.jdbc.tests;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;

public class ConcurrentTransactionsTest extends DatabaseTestBase {

	@Test
	public void can_execute_concurrent_transactions() throws Exception {
		CyclicBarrier barrier = new CyclicBarrier(2);
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.query("select name, amount from accounts where id = ?", 1)
				.expect("petru", 123)
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(2, TimeUnit.SECONDS)
				.query("select name, amount from accounts where id = ?", 2)
				.expect("paul", 456)
			.commit()
			.build();
			
		ExecutorService executor = Executors.newFixedThreadPool(2);

		executor.submit(tx1);
		executor.submit(tx2);
		
		executor.shutdown();
		if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
			fail("test did not terminate in 10 seconds");
		}
		
		if (tx1.hasException()) {
			throw tx1.getException();
		}
		
		if (tx2.hasException()) {
			throw tx2.getException();
		}
	}
}

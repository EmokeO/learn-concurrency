package ro.galanton.learn.jdbc.tests;

import java.sql.Connection;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * Conclusion 2: if a transaction tx-1 at isolation repeatable read has read a record, another 
 * transaction tx-2 cannot write the same record until tx-1 has finished.
 * 
 * @author Petru Galanton
 */
@RunWith(Parameterized.class)
public class Conclusion2Test extends DatabaseTestBase {

	private static final Logger log = LoggerFactory.getLogger(Conclusion2Test.class);
	
	@Parameters
	public static Object[][] parameters() {
		return new Object[][] {
			{ Connection.TRANSACTION_READ_UNCOMMITTED },
			{ Connection.TRANSACTION_READ_COMMITTED }, 
			{ Connection.TRANSACTION_REPEATABLE_READ }, 
			{ Connection.TRANSACTION_SERIALIZABLE }
		};
	}
	
	@Parameter
	public int tx2Isolation;
	
	@Test
	public void test() throws Exception {
		log.info("=== CONCLUSION 2, TX-1 ISOLATION: " + tx2Isolation + " ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.begin(Connection.TRANSACTION_REPEATABLE_READ)
				.waitFor(barrier)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.waitFor(2, TimeUnit.SECONDS)
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.begin(tx2Isolation)
				.waitFor(barrier)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200, 1)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
	
	// TODO show that at RC, tx-2 can write the record before tx-1 finishes
}

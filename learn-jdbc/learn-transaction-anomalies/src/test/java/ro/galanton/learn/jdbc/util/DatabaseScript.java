package ro.galanton.learn.jdbc.util;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Holds the SQL string of an SQL script and whether errors should be ignored when the script is executed.
 * 
 * @author Petru Galanton
 */
class DatabaseScript {

	private String sql;
	private boolean ignoreErrors;
	
	public DatabaseScript(String sql, boolean ignoreErrors) {
		this.sql = sql;
		this.ignoreErrors = ignoreErrors;
	}
	
	public void execute(Connection con) throws SQLException {
		try {
			con.prepareStatement(sql).execute();
		} catch (SQLException e) {
			if (!ignoreErrors) {
				throw e;
			}
		}
	}
}

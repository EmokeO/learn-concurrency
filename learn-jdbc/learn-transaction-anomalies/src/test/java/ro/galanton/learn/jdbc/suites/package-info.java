/**
 * Contains test suites composed of tests in the ro.galanton.learn.jdbc.tests package.
 */
package ro.galanton.learn.jdbc.suites;
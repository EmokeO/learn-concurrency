package ro.galanton.learn.jdbc.tests;

import java.sql.Connection;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * Petru's salary is 123. tx-1 increases Petru's salary by 10 and commits. 
 * At the same time, tx-2 increases Petru's salary by 77, but then changes his mind and rolls back.
 * 
 * Q: What should Petru's salary be after both tx-1 and tx-2 have finished ?
 * A: it should be 133, but because of the dirty read anomaly, his salary actually ends up at 210.
 * 
 * @author Petru Galanton
 */
public class DirtyReadTest extends DatabaseTestBase {

	private static final Logger log = LoggerFactory.getLogger(DirtyReadTest.class);
	
	@Test
	public void with_isolation_read_uncommitted() throws Exception {
		log.info("=== DIRTY READ WITH ISOLATION READ UNCOMMITTED ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 200) // dirty read
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200 + 10, 1)
			.commit()
			.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
			.expect("petru", 210) // should have been 133
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 77, 1)
				.waitFor(2, TimeUnit.SECONDS)
			.rollback()
			.build();

		execute(tx1, tx2);
	}
	
	@Test
	public void with_isolation_read_committed() throws Exception {
		log.info("=== DIRTY READ WITH ISOLATION READ COMMITTED ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_COMMITTED)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123) // no more dirty read
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 10, 1)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 133) // as it should be
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200, 1)
				.waitFor(2, TimeUnit.SECONDS)
			.rollback()
			.build();
		
		execute(tx1, tx2);
	}
	
	@Test
	public void cannot_happen_using_updates_only() throws Exception {
		log.info("=== UPDATES ONLY WITH READ UNCOMMITTED ===");
		CyclicBarrier barrier = new CyclicBarrier(2);
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = amount + 10 WHERE id = ?", 1)
			.commit()
			.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
			.expect("petru", 133)
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.update("UPDATE accounts SET amount = amount + 77 WHERE id = ?", 1)
				.waitFor(2, TimeUnit.SECONDS)
			.rollback()
			.build();
		
		execute(tx1, tx2);
	}
}

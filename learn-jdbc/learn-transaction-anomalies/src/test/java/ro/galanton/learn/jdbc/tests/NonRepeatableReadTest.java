package ro.galanton.learn.jdbc.tests;

import java.sql.Connection;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

public class NonRepeatableReadTest extends DatabaseTestBase {

	private static final Logger log = LoggerFactory.getLogger(NonRepeatableReadTest.class);
	
	@Test
	public void with_isolation_read_committed() throws Exception {
		log.info("=== NON REPEATABLE READ WITH ISOLATION READ COMMITTED ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_COMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 200) // non-repeatable read
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200, 1)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
	
	@Test
	public void with_isolation_repeatable_read() throws Exception {
		log.info("=== NON REPEATABLE READ WITH ISOLATION REPEATABLE READ ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_REPEATABLE_READ)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123) // no more non-repeatable read
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200, 1)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
	
	@Test
	public void test_marius() throws Exception {
		log.info("=== NON REPEATABLE READ WITH ISOLATION REPEATABLE READ ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_REPEATABLE_READ)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(2, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 150, 1)
				//.expect("petru", 123) // no more non-repeatable read
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200, 1)
			.commit()
			.build();
		
		execute(tx1, tx2);
		
		ConcurrentTransaction tx3 = new ConcurrentTransactionBuilder("tx-3")
				.begin(Connection.TRANSACTION_REPEATABLE_READ)
					.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
					.expect("petru", 200)
				.commit()
				.build();
		execute(tx3);
		
	}
	
}

create table accounts (
	id int, 
	name varchar(100), 
	amount int, 
	
	constraint pk_accounts primary key (id)
);

insert into accounts
	select 1, 'petru', 123 union
	select 2, 'paul', 456;
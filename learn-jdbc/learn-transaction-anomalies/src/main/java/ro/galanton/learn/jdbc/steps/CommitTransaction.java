package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.SQLException;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction step which commits the current transaction.
 * 
 * @author Petru Galanton
 */
public class CommitTransaction extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(CommitTransaction.class);
	
	// TODO documentation
	public CommitTransaction(String txName) {
		super(txName);
	}
	
	@Override
	public void execute(Connection con) throws SQLException {
		log.info(toString());
		con.commit();
		con.setAutoCommit(true);
	}

	@Override
	public String toString() {
		return "CommitTransaction [txName=" + getTransactionName() + "]";
	}

}

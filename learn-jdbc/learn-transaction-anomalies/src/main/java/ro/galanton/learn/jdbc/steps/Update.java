package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction step which executes a DML query (insert, update, delete). 
 * 
 * @author Petru Galanton
 */
public class Update extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(Update.class);
	
	private String sql;
	private Object[] parameters;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param txName TODO documentation
	 * @param sql The SQL query string.
	 * @param parameters The SQL query parameters.
	 */
	public Update(String txName, String sql, Object[] parameters) {
		super(txName);
		this.sql = sql;
		this.parameters = parameters;
	}
	
	@Override
	public void execute(Connection con) throws SQLException {
		log.info("starting " + toString());
		
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			for (int i = 0; i < parameters.length; i++) {
				stmt.setObject(i+1, parameters[i]);
			}
			
			stmt.executeUpdate();
		} catch (SQLException e) {
			log.error(e, "exception in " + toString());
			throw e;
		}
		
		log.info("completed " + toString());
	}

	@Override
	public String toString() {
		return "Update [txName=" + getTransactionName() + ", sql=" + sql + ", parameters=" + Arrays.toString(parameters) + "]";
	}

}

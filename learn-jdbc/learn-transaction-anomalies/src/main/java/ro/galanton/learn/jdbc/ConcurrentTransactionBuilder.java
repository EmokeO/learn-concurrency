package ro.galanton.learn.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import ro.galanton.learn.jdbc.steps.BeginTransaction;
import ro.galanton.learn.jdbc.steps.CommitTransaction;
import ro.galanton.learn.jdbc.steps.Expect;
import ro.galanton.learn.jdbc.steps.Query;
import ro.galanton.learn.jdbc.steps.RollbackTransaction;
import ro.galanton.learn.jdbc.steps.Update;
import ro.galanton.learn.jdbc.steps.WaitForBarrier;
import ro.galanton.learn.jdbc.steps.WaitForTime;

/**
 * Helps build {@link ConcurrentTransaction} instances using a fluent API.
 * 
 * @author Petru Galanton
 */
public class ConcurrentTransactionBuilder {

	private String txName;
	private Query lastQuery = null;
	private int lastRowNumber = 0;
	private List<TransactionStep> steps = new ArrayList<>();
	
	public ConcurrentTransactionBuilder(String txName) {
		this.txName = txName;
	}
	
	public ConcurrentTransactionBuilder begin(int isolation) {
		steps.add(new BeginTransaction(txName, isolation));
		
		return this;
	}
	
	public ConcurrentTransactionBuilder query(String sql, Object... params) {
		lastQuery = new Query(txName, sql, params);
		lastRowNumber = 0;
		
		steps.add(lastQuery);
		
		return this;
	}
	
	public ConcurrentTransactionBuilder update(String sql, Object...params) {
		steps.add(new Update(txName, sql, params));
		
		return this;
	}
	
	public ConcurrentTransactionBuilder expect(Object... values) {
		if (lastQuery == null) {
			throw new IllegalArgumentException("Cannot expect results if no query has been executed first");
		}
		
		steps.add(new Expect(txName, lastQuery, lastRowNumber, values));
		lastRowNumber++;
		
		return this;
	}
	
	public ConcurrentTransactionBuilder commit() {
		steps.add(new CommitTransaction(txName));
		
		return this;
	}
	
	public ConcurrentTransactionBuilder rollback() {
		steps.add(new RollbackTransaction(txName));
		
		return this;
	}
	
	public ConcurrentTransactionBuilder waitFor(long duration, TimeUnit unit) {
		steps.add(new WaitForTime(txName, unit.toMillis(duration)));
		
		return this;
	}
	
	public ConcurrentTransactionBuilder waitFor(CyclicBarrier barrier) {
		steps.add(new WaitForBarrier(txName, barrier));
		
		return this;
	}
	
	public ConcurrentTransaction build() {
		return new ConcurrentTransaction(steps);
	}
}

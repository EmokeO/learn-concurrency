package ro.galanton.learn.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Represents a step/query in a transaction. Breaking up a transaction into steps helps 
 * with interleaving the steps of 2 transactions in order to demonstrate transaction anomalies.
 * 
 * @author Petru Galanton
 */
public interface TransactionStep {

	/**
	 * Executes this transaction step on the specified connection.
	 * 
	 * @param con the connection on which this step should be executed.
	 * 
	 * @throws SQLException if an exception occurs while executing this step.
	 */
	public void execute(Connection con) throws SQLException;
	
}

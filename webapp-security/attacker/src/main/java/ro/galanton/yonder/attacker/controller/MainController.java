package ro.galanton.yonder.attacker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ro.galanton.yonder.attacker.model.StolenCookies;
import ro.galanton.yonder.attacker.repository.StolenCookiesRepository;

@Controller
public class MainController {

	@Autowired
	private StolenCookiesRepository stolenCookies;
	
	@RequestMapping("/")
	public String index(Model model) {
		List<StolenCookies> cookies = stolenCookies.findAll();
		model.addAttribute("stolenCookies", cookies);
		return "index";
	}
	
}

package ro.galanton.yonder.webappsecurity.vulnerableforum.service;

import java.util.Calendar;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.Comment;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.Topic;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.TopicRepository;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.UserRepository;

@Service
@Transactional
public class ForumService {

	@Autowired
	private TopicRepository topics;
	
	@Autowired
	private UserRepository users;
	
	
	public Topic createTopic(String author, String title, String content) {
		User user = users.findOne(author);
		
		Topic topic = new Topic();
		topic.setAuthor(user);
		topic.setTitle(title);
		topic.setDate(Calendar.getInstance());
		topic.setContent(content);
		
		return topics.save(topic);
	}
	
	public Comment createComment(String author, Long topicId, String content) {
		User user = users.findOne(author);
		Topic topic = topics.findOne(topicId);
		
		Comment comment = new Comment();
		comment.setAuthor(user);
		comment.setContent(content);
		comment.setDate(Calendar.getInstance());
		topic.getComments().add(comment);
		
		topics.save(topic);
		
		return comment;
	}
	
	public Iterable<Topic> listTopics() {
		return topics.list();
	}
	
	public Topic getTopic(Long topicId) {
		return topics.findOne(topicId);
	}
}

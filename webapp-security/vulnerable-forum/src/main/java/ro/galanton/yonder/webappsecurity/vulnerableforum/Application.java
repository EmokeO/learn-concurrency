package ro.galanton.yonder.webappsecurity.vulnerableforum;

import org.apache.catalina.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import ro.galanton.yonder.webappsecurity.vulnerableforum.service.ForumAuthenticationProvider;

@SpringBootApplication
public class Application implements EmbeddedServletContainerCustomizer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		((TomcatEmbeddedServletContainerFactory) container).addContextCustomizers(new TomcatContextCustomizer() {
			
			@Override
			public void customize(Context context) {
				context.setUseHttpOnly(false);
			}
		});
	}
	
	@EnableWebSecurity
	public static class FirstConfiguration extends WebSecurityConfigurerAdapter {

		@Autowired
		private ForumAuthenticationProvider authenticationProvider;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.authenticationProvider(authenticationProvider)
				.csrf().disable()
				.headers().disable()
				.sessionManagement()
				    .sessionFixation().none()
				    .and()
			    .authorizeRequests()
					.anyRequest().authenticated()
					.and()
				.formLogin()
					.defaultSuccessUrl("/", true)
					.failureUrl("/login.html")
					.loginPage("/login.html")
					.loginProcessingUrl("/login-process")
					.passwordParameter("password")
					.permitAll(true)
					.usernameParameter("username")
					.and()
				.logout()
					.invalidateHttpSession(true)
					.clearAuthentication(true)
					.logoutUrl("/logout");
		}
		
	}
	
}

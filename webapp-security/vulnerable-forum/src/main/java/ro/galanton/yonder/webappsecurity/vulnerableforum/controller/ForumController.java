package ro.galanton.yonder.webappsecurity.vulnerableforum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.Topic;
import ro.galanton.yonder.webappsecurity.vulnerableforum.service.ForumService;

@Controller
public class ForumController extends BaseController {

	@Autowired
	private ForumService service;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(@AuthenticationPrincipal String username, Model model) {
		model.addAttribute("topics", service.listTopics());
		model.addAttribute("username", username);
		return "index";
	}

	@RequestMapping(value = "/topic", method = RequestMethod.GET)
	public String topic() {
		return "new-topic";
	}

	@RequestMapping(value = "/topic", method = RequestMethod.POST)
	public String createTopic(@RequestParam("title") String title, @RequestParam("content") String content,
			@AuthenticationPrincipal String username, Model model) {
		Topic topic = service.createTopic(username, title, content);
		return "redirect:/topic/" + topic.getId();
	}

	@RequestMapping(value = "/topic/{topicId}", method = RequestMethod.GET)
	public String displayTopic(@PathVariable("topicId") Long topicId, Model model) {
		model.addAttribute("topic", service.getTopic(topicId));
		return "display-topic";
	}

}

package ro.galanton.yonder.webappsecurity.vulnerableforum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ro.galanton.yonder.webappsecurity.vulnerableforum.service.UserService;
import ro.galanton.yonder.webappsecurity.vulnerableforum.service.ValidationException;

@Controller
public class AccountController {

    @Autowired
    private UserService users;

    @RequestMapping("/account")
    public String account(@AuthenticationPrincipal String username, Model model) {
        model.addAttribute("username", username);
        return "account";
    }

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public String changePassword(@AuthenticationPrincipal String username, @RequestParam("password") String password,
            Model model) {

        model.addAttribute("username", username);
        try {
            users.changePassword(username, password);
            model.addAttribute("message", "Password changed");
        } catch (ValidationException e) {
            model.addAttribute("message", e.getMessage());
        }
        return "account";
    }
}

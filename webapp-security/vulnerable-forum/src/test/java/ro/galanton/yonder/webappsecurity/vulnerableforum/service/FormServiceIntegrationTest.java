package ro.galanton.yonder.webappsecurity.vulnerableforum.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.svenjacobs.loremipsum.LoremIpsum;
import ro.galanton.yonder.webappsecurity.vulnerableforum.Application;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.Comment;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.Topic;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.TopicRepository;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@Transactional
public class FormServiceIntegrationTest {

	@Autowired
	private ForumService service;
	
	@Autowired
	private UserRepository users;
	
	@Autowired
	private TopicRepository topics;
	
	private LoremIpsum lorem = new LoremIpsum();
	
	@Before
	public void setUp() {
		users.save(new User("petru", "123"));
		users.save(new User("paul", "123"));
		users.save(new User("pavel", "123"));
	}
	
	@Test
	public void it_creates_topics() {
		String content = lorem.getParagraphs(10);
		
		Topic topic = service.createTopic("petru", "hello", content);

		assertNotNull(topic);
		assertNotNull(topic.getId());
		
		Topic saved = topics.findOne(topic.getId());
		
		assertEquals("hello", saved.getTitle());
		assertEquals(content, saved.getContent());
		assertNotNull(saved.getAuthor());
		assertEquals("petru", saved.getAuthor().getUsername());
		assertNotNull(saved.getDate());
	}
	
	@Test
	public void it_creates_comments() {
		String content = lorem.getParagraphs(10);
		Topic topic = service.createTopic("petru", "hello", "hello world");
		
		service.createComment("paul", topic.getId(), content);
		
		Topic saved = topics.findOne(topic.getId());
		assertNotNull(saved);
		assertNotNull(saved.getComments());
		assertTrue(saved.getComments().size() == 1);
		
		Comment comment = topic.getComments().get(0);
		assertNotNull(comment);
		assertEquals(content, comment.getContent());
		assertNotNull(comment.getAuthor());
		assertEquals("paul", comment.getAuthor().getUsername());
	}
	
	@Test
	public void it_lists_topics_descending_by_date() throws Exception {
		Calendar today = Calendar.getInstance();
		topic("petru", "topic 1", "content 1", today, 0).build();
		topic("paul", "topic 2", "content 2", today, 1).build();
		
		Iterable<Topic> actual = service.listTopics();
		assertNotNull(actual);
		
		String[] authors = new String[] { "paul", "petru" };
		String[] titles = new String[] { "topic 2", "topic 1" };
		int i = 0;
		for (Topic topic : actual) {
			assertEquals(authors[i], topic.getAuthor().getUsername());
			assertEquals(titles[i], topic.getTitle());
			i++;
		}
	}
	
	@Test
	public void it_lists_comments_ascending_by_date() {
		Calendar today = Calendar.getInstance();
		Topic topic = topic("petru", "topic 1", "content 1", today, 0)
						.comment("paul", "comment 1.1", today, 1)
						.comment("petru", "comment 1.2", today, 2)
						.build();
		
		Topic actual = service.getTopic(topic.getId());
		assertNotNull(actual);
		assertEquals("petru", actual.getAuthor().getUsername());
		assertEquals("content 1", actual.getContent());
		
		String[] authors = new String[] { "paul", "petru" };
		String[] contents = new String[] { "comment 1.1", "comment 1.2" };
		int i = 0;
		for (Comment comment : actual.getComments()) {
			assertEquals(authors[i], comment.getAuthor().getUsername());
			assertEquals(contents[i], comment.getContent());
			i++;
		}
	}
	
	private TopicBuilder topic(String author, String title, String content, Calendar date, int daysOffset) {
		Topic topic = service.createTopic(author, title, content);
		Calendar newDate = (Calendar) date.clone();
		newDate.add(Calendar.DAY_OF_YEAR, daysOffset);
		topic.setDate(newDate);
		return new TopicBuilder(topics.save(topic));
	}
	
	private class TopicBuilder {
		
		private Topic topic;
		
		public TopicBuilder(Topic topic) {
			this.topic = topic;
		}
		
		public TopicBuilder comment(String author, String content, Calendar date, int daysOffset) {
			Comment comment = service.createComment(author, topic.getId(), content);
			Calendar newDate = (Calendar) date.clone();
			newDate.add(Calendar.DAY_OF_YEAR, daysOffset);
			comment.setDate(newDate);
			return this;
		}
		
		public Topic build() {
			topics.flush();
			return topic;
		}
	}
}

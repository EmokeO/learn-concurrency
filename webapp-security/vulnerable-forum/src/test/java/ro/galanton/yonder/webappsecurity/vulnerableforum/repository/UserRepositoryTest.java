package ro.galanton.yonder.webappsecurity.vulnerableforum.repository;

import static org.junit.Assert.*;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ro.galanton.yonder.webappsecurity.vulnerableforum.Application;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@Transactional
public class UserRepositoryTest {

	@Autowired
	private UserRepository repository;
	
	@Test
	public void testCreate() {
		User user = new User();
		user.setUsername("petru");
		user.setPassword("123");
		
		repository.save(user);
		
		User saved = repository.findOne("petru");
		assertNotNull(saved);
		assertEquals(user, saved);
	}
	
	@Test
	public void testUpdate() {
		User user = new User();
		user.setUsername("petru");
		user.setPassword("123");
		repository.save(user);
		
		user.setPassword("456");
		repository.save(user);
		
		User saved = repository.findOne("petru");
		assertNotNull(saved);
		assertEquals(user, saved);
	}
	
	@Test
	public void testDelete() {
		User user = new User();
		user.setUsername("petru");
		user.setPassword("123");
		repository.save(user);
		
		repository.delete("petru");
		
		assertFalse(repository.exists("petru"));
	}
}

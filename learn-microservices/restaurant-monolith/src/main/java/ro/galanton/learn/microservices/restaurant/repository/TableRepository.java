package ro.galanton.learn.microservices.restaurant.repository;

import org.springframework.data.repository.CrudRepository;

import ro.galanton.learn.microservices.restaurant.model.Table;

public interface TableRepository extends CrudRepository<Table, Integer> {

}

package ro.galanton.learn.microservices.restaurant.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ro.galanton.learn.microservices.restaurant.service.UiService;

@Controller
public class HomeController {

	private final UiService uiService;

	public HomeController(UiService uiService) {
		this.uiService = uiService;
	}
	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("menu", uiService.getMenu());
		return "index";
	}
}

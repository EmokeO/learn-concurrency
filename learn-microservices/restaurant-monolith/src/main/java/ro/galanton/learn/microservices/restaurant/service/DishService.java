package ro.galanton.learn.microservices.restaurant.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import ro.galanton.learn.microservices.restaurant.model.Dish;
import ro.galanton.learn.microservices.restaurant.repository.DishRepository;

@Component
@Transactional
public class DishService {

	private final DishRepository repository;

	public DishService(DishRepository repository) {
		this.repository = repository;
	}
	
	public Dish create(String name, String description, BigDecimal price) {
		Dish dish = new Dish();
		dish.setName(name);
		dish.setDescription(description);
		dish.setPrice(price);
		
		return repository.save(dish);
	}
	
	public Iterable<Dish> list() {
		return repository.findAll();
	}
	
	public Dish find(int id) {
		return repository.findOne(id);
	}

}

package ro.galanton.learn.microservices.restaurant.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import ro.galanton.learn.microservices.restaurant.model.Dish;
import ro.galanton.learn.microservices.restaurant.model.Order;
import ro.galanton.learn.microservices.restaurant.model.OrderEntry;
import ro.galanton.learn.microservices.restaurant.model.Table;
import ro.galanton.learn.microservices.restaurant.repository.OrderRepository;

@Component
@Transactional
public class OrderService {

	private final TableService tableService;
	private final DishService dishService;
	private final OrderRepository repository;

	public OrderService(TableService tableService, DishService dishService, OrderRepository repository) {
		this.tableService = tableService;
		this.dishService = dishService;
		this.repository = repository;
	}
	
	public Order create(int tableNumber) {
		// TODO null-checks, exception handling
		Table table = tableService.find(tableNumber);
		
		// TODO check if there is already an order for this table
		
		Order order = new Order();
		table.setCurrentOrder(order);
		order.setTable(table);
		
		return repository.save(order);
	}
	
	public Order addDish(int orderId, int dishId, int amount) {
		Order order = repository.findOne(orderId);
		Dish dish = dishService.find(dishId);

		OrderEntry entry = new OrderEntry();
		entry.setDish(dish);
		entry.setAmount(amount);
		
		// TODO move relationship maintenance logic to the entity
		entry.setOrder(order);
		order.getEntries().add(entry);
		
		return repository.save(order);
	}
	
	public Order findByTableNumber(int tableNumber) {
		Table table = tableService.find(tableNumber);
		
		// TODO null-check on table and table.currentOrder
		return table.getCurrentOrder();
	}
	
	public Iterable<Order> list() {
		return repository.findAll();
	}

	public void finishOrder(int orderId) {
		Order order = repository.findOne(orderId);
		order.getTable().setCurrentOrder(null);
		
		repository.delete(orderId);
	}

}

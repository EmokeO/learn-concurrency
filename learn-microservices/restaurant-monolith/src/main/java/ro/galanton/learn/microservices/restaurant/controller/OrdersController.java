package ro.galanton.learn.microservices.restaurant.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ro.galanton.learn.microservices.restaurant.model.Order;
import ro.galanton.learn.microservices.restaurant.service.DishService;
import ro.galanton.learn.microservices.restaurant.service.OrderService;
import ro.galanton.learn.microservices.restaurant.service.UiService;

@Controller
public class OrdersController {

	private final UiService uiService;
	private final OrderService orderService;
	private final DishService dishService;

	public OrdersController(UiService uiService, OrderService orderService, DishService dishService) {
		this.uiService = uiService;
		this.orderService = orderService;
		this.dishService = dishService;
	}
	
	@GetMapping("/orders")
	public String listAll(Model model) {
		
		Iterable<Order> orders = orderService.list();
		
		model.addAttribute("menu", uiService.getMenu());
		model.addAttribute("orders", orders);
		
		return "orders";
	}
	
	@GetMapping(value = "/orders/{tableNumber}", params = "new")
	public String newOrder(@PathVariable("tableNumber") int tableNumber, Model model) {
		Order order = orderService.create(tableNumber);
		
		model.addAttribute("menu", uiService.getMenu());
		model.addAttribute("order", order);
		model.addAttribute("dishes", dishService.list());
		
		return "order";
	}
	
	@GetMapping("/orders/{tableNumber}")
	public String orderDetails(@PathVariable("tableNumber") int tableNumber, Model model) {
		Order order = orderService.findByTableNumber(tableNumber);
		
		model.addAttribute("menu", uiService.getMenu());
		model.addAttribute("order", order);
		model.addAttribute("dishes", dishService.list());
		
		return "order";
	}
	
	@PostMapping(value="/orders/{orderId}", params="addEntry")
	public String addEntry(@PathVariable("orderId") int orderId, @RequestParam("dish") int dishId, @RequestParam("amount") int amount, Model model) {
		Order order = orderService.addDish(orderId, dishId, amount);
		
		model.addAttribute("menu", uiService.getMenu());
		model.addAttribute("order", order);
		model.addAttribute("dishes", dishService.list());
		
		return "order";
	}
	
	@PostMapping(value = "/orders/{orderId}", params="finish")
	public String finishOrder(@PathVariable("orderId") int orderId) {
		orderService.finishOrder(orderId);
		return "redirect:/tables";
	}
}

package ro.galanton.learn.microservices.restaurant.service;

import org.springframework.stereotype.Component;

import ro.galanton.learn.microservices.restaurant.model.ui.Menu;
import ro.galanton.learn.microservices.restaurant.model.ui.MenuItem;

@Component
public class UiService {

	public Menu getMenu() {
		return new Menu(
				new MenuItem("Home", "/"),
				new MenuItem("Tables", "/tables"), 
				new MenuItem("Orders", "/orders"),
				new MenuItem("Logout", "/logout"));
	}
}

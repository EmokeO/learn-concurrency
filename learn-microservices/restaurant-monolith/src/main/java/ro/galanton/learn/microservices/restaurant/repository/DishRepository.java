package ro.galanton.learn.microservices.restaurant.repository;

import org.springframework.data.repository.CrudRepository;

import ro.galanton.learn.microservices.restaurant.model.Dish;

public interface DishRepository extends CrudRepository<Dish, Integer> {

}

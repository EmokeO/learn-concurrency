package ro.galanton.learn.microservices.restaurant.model.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Menu {

	private final List<MenuItem> menuItems;
	
	public Menu(MenuItem... menuItems) {
		this.menuItems = new ArrayList<>(menuItems.length);
		for (MenuItem item : menuItems) {
			this.menuItems.add(item);
		}
	}

	public List<MenuItem> getMenuItems() {
		return Collections.unmodifiableList(menuItems);
	}
}

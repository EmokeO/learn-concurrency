package ro.galanton.learn.junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

public class CalculatorTest {

	@ClassRule
	public static TestRule myTestRule = new MyTestRule();
	
	@ClassRule 
	public static TestRule myTestRuleAgain = new MyTestRule();
	
	@Rule
	public MethodRule myMethodRule = new MyMethodRule();
	
	@BeforeClass
	public static void init() {
		System.out.println("### before class");
	}
	
	@Before
	public void setUp() {
		System.out.println("### setup");
	}
	
	@After
	public void tearDown() {
		System.out.println("### tear down");
	}
	
	@AfterClass
	public static void destroy() {
		System.out.println("### destroy");
	}
	
	
	@Test
	public void test1() {
		System.out.println("### test 1");
	}
	
	@Test
	public void test2() {
		System.out.println("### test 2");
	}
	
}

package ro.galanton.learn.junit;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class MyTestRule implements TestRule {

	@Override
	public Statement apply(Statement base, Description description) {
		return new MyStatement(base);
	}

	
	
	static class MyStatement extends Statement {
		
		private Statement stmt;
		
		public MyStatement(Statement stmt) {
			this.stmt = stmt;
		}
		
		@Override
		public void evaluate() throws Throwable {
			System.out.println("### MyStatement before");
			stmt.evaluate();
			System.out.println("### MyStatement after");
		}
	}
}

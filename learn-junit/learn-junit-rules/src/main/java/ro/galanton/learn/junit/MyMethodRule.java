package ro.galanton.learn.junit;

import org.junit.rules.MethodRule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class MyMethodRule implements MethodRule {

	static class MyStatement extends Statement {
		
		private Statement stmt;
		
		public MyStatement(Statement stmt) {
			this.stmt = stmt;
		}
		
		@Override
		public void evaluate() throws Throwable {
			System.out.println("### method MyStatement before");
			stmt.evaluate();
			System.out.println("### method MyStatement after");
		}
	}

	@Override
	public Statement apply(Statement base, FrameworkMethod method, Object target) {
		System.out.println("### target = " + target);
		return new MyStatement(base);
	}
}

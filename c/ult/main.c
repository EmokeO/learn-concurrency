#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ult.h"

// how many iterations each thread does
#define ITERATIONS 5

// whether locking the mutex is enabled
#define LOCK 0

int n;
ult_mutex_t mutex;

void long_operation() {
    int i, j, k, count;

    count = 10 + (rand() % 990); // random number between 10 and 999
    for (i = 0; i < count; i++) {
        for (j = 0; j < 1000000; j++) {
            k = i + j;
        }
    }

    // just to get rid of warning about unused variable k
    i = k;
}

void increment() {
    int i,k;

    // do a warmup so thread execution order gets mixed up
    for (i = 0; i < ITERATIONS; i++) {
        long_operation();
    }

    for (i = 0; i < ITERATIONS; i++) {
        if (LOCK) {
            ult_mutex_lock(mutex);
        }

        k = n;
        long_operation();
        n = k + 1;
        printf("thread %d: i = %d (time = %d)\n", ult_self(), i, (int) time(0));

        if (LOCK) {
            ult_mutex_unlock(mutex);
        }
    }

    printf("thread %d finished\n", ult_self());
}


void decrement() {
    int i,k;

    // do a warmup so thread execution order gets mixed up
    for (i = 0; i < ITERATIONS; i++) {
        long_operation();
    }

    for (i = 0; i < ITERATIONS; i++) {
        if (LOCK) {
            ult_mutex_lock(mutex);
        }

        k = n;
        long_operation();
        n = k - 1;
        printf("thread %d: i = %d (time = %d)\n", ult_self(), i, (int) time(0));

        if (LOCK) {
            ult_mutex_unlock(mutex);
        }
    }

    printf("thread %d finished\n", ult_self());
}

int main(int argc, char** argv) {
    int i, count = 10;
    ult_t threads[count];

    srand(time(0));
    ult_init();
    //ult_log(ULT_CREATE | ULT_LOCK | ULT_UNLOCK | ULT_SUSPEND | ULT_RESUME);
    ult_log(ULT_ALL);
    mutex = ult_mutex_create();

    for (i = 0; i < count; i += 2) {
        threads[i] = ult_create(increment);
        threads[i+1] = ult_create(decrement);
    }

    for (i = 0; i < count; i++) {
        ult_join(threads[i]);
    }

    ult_mutex_destroy(mutex);

    // without mutexes, n = -7, -6, -7
    // with mutexes, n = 0, 0, 0
    printf("main thread finished, n = %d\n", n);

    return 0;
}

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include "ult.h"

// maximum number of threads that can be created
#define MAX_THREADS 1000

// maximum number of mutexes that can be created
#define MAX_MUTEXES 1000

// stack size of new threads
#define STACK_SIZE 16384

struct ult_thread {
    int id; // thread ID
    int finished; // flag indicating if thread is finished (0 = not finished, 1 = finished)
    ucontext_t ctx; // thread context
    ult_t joining; // flag indicating if thread is joining another thread (-1 = not joining, otherwise joining the specified thread)
    ult_mutex_t locking; // flag indicating if thread is waiting for a mutex (-1 = not locking, otherwise locking the specified mutex)
};

struct ult_mutex {
    int id; // mutex ID
    ult_t owner; // ID of thread who currently has this mutex
};

// logging level
int LOG_LEVEL = ULT_NONE;

// INTERNAL FUNCTION DECLARATIONS

void ult_sched();
void ult_end();
void init_thread(int id);
void print_threads();

// context which is activated when a thread ends (same for all threads)
ucontext_t end;

// internal thread and mutex buffers
// TODO make these circular buffers, or something
struct ult_thread threads[MAX_THREADS];
struct ult_mutex mutexes[MAX_MUTEXES];

// indicates how many threads we have in the thread buffer
int thread_count = 0;

// indicates which is the currently executing thread
int current_thread = -1;

// indicates how many mutexes we've created
int mutex_count = 0;


// initializes the ULT library
void ult_init() {
    // convert the main thread to an ULT thread
    init_thread(0);
    thread_count = 1;
    current_thread = 0;
    // note: the main thread doesn't have an end context, so when the main thread ends, the process ends


    // send us a SIGALRM signal in 1 second
    signal(SIGALRM, ult_sched);
    alarm(1);

    // init end context
    getcontext(&end);
    end.uc_stack.ss_sp = (void *) malloc(STACK_SIZE);
    end.uc_stack.ss_size = STACK_SIZE;
    makecontext(&end, ult_end, 0);
}

// creates a thread that executes the specified function
// return -1 if the thread could not be created
ult_t ult_create(void (*f)(void)) {
    signal(SIGALRM, SIG_IGN);


    // check that we don't overflow
    ult_t tid = thread_count < MAX_THREADS ? thread_count : -1;

    if (tid >= 0) {
        thread_count++;
        init_thread(tid);

        getcontext(&threads[tid].ctx);
        threads[tid].ctx.uc_stack.ss_sp = (void *) malloc(STACK_SIZE);
        threads[tid].ctx.uc_stack.ss_size = STACK_SIZE;
        threads[tid].ctx.uc_link = &end;
        makecontext(&threads[tid].ctx, f, 0);

        if (LOG_LEVEL & ULT_CREATE) {
            printf("create: created thread %d\n", tid);
        }
    }

    signal(SIGALRM, ult_sched);

    return tid;
}

// joins the thread with the specified id (waits until it finishes)
void ult_join(ult_t tid) {
    signal(SIGALRM, SIG_IGN);

    if (!threads[tid].finished) {
        if (LOG_LEVEL & ULT_SUSPEND) {
            printf("join: thread %d joining thread %d\n", current_thread, tid);
        }
        threads[current_thread].joining = tid;
    }

    // TODO move this inside the if, no need to call the scheduler if we didn't suspend the thread
    ult_sched();
}

// returns the id of the current thread
ult_t ult_self() {
    // TODO ignore SIGALRM ?
    return current_thread;
}

ult_mutex_t ult_mutex_create() {
    // TODO ignore SIGALRM ?

    // check that we don't overflow
    ult_mutex_t mid = mutex_count < MAX_MUTEXES ? mutex_count : -1;
    if (mid >= 0) {
        mutex_count++;
        mutexes[mid].id = mid;
        mutexes[mid].owner = -1;
        if (LOG_LEVEL & ULT_CREATE) {
            printf("mutex create: created mutex %d\n", mid);
        }
    }
    return mid;
}

void ult_mutex_lock(ult_mutex_t mutex) {
    signal(SIGALRM, SIG_IGN);

    if (mutexes[mutex].owner == -1) {
        mutexes[mutex].owner = current_thread;
        if (LOG_LEVEL & ULT_LOCK) {
            printf("mutex lock: thread %d locked mutex %d\n", current_thread, mutex);
        }
    } else if (mutexes[mutex].owner != current_thread) {
        if (LOG_LEVEL & ULT_SUSPEND) {
            printf("mutex lock: thread %d waiting to lock mutex %d owned by thread %d\n", current_thread, mutex, mutexes[mutex].owner);
        }

        threads[current_thread].locking = mutex;
        ult_sched();
    }

    signal(SIGALRM, ult_sched);
}

void ult_mutex_unlock(ult_mutex_t mutex) {
    int i;

    signal(SIGALRM, SIG_IGN);

    mutexes[mutex].owner = -1;
    if (LOG_LEVEL & ULT_UNLOCK) {
        printf("mutex unlock: thread %d unlocked mutex %d\n", current_thread, mutex);
    }

    for (i = (current_thread+1) % thread_count; i != current_thread; i = (i+1) % thread_count) {
        if (threads[i].locking == mutex) {
            threads[i].locking = -1;
            mutexes[mutex].owner = i;
            if (LOG_LEVEL & ULT_RESUME) {
                printf("mutex unlock: thread %d resumed and locked mutex %d\n", i, mutex);
            }
            break;
        }
    }

    signal(SIGALRM, ult_sched);
}

void ult_mutex_destroy(ult_mutex_t mutex) {
    // TODO when mutexes are not statically allocated
}


// ------ PRIVATE FUNCTIONS --------------

// executes the thread scheduler
void ult_sched() {
    signal(SIGALRM, SIG_IGN);

    int candidate, current = current_thread;

    if (LOG_LEVEL & ULT_SCHED) {
        printf("scheduler: executing, current_thread = %d\n", current_thread);
    }

    // pick a candidate
    for (candidate = (current + 1) % thread_count; candidate != current; candidate = (candidate + 1) % thread_count)  {
        if (threads[candidate].finished) continue;
        if (threads[candidate].joining >= 0) continue;
        if (threads[candidate].locking >= 0) continue;

        break;
    }

    if (LOG_LEVEL & ULT_SCHED) {
        printf("schedule: candidate = %d\n", candidate);
    }

    current_thread = candidate;

    signal(SIGALRM, ult_sched);
    alarm(1);

    // TODO this code is outside SIG_IGN, what if we get interrupted ?

    // swap only if there is a point in swapping
    if (current != candidate) {
        if (LOG_LEVEL & ULT_SCHED) {
            printf("scheduler: swapping context %d -> %d\n", threads[current].id, threads[candidate].id);
        }
        swapcontext(&threads[current].ctx, &threads[candidate].ctx);
    }

    // TODO alarm and signal don't work well here
}

// executes when a thread ends
void ult_end() {
    int i;

    if (LOG_LEVEL & ULT_SCHED) {
        printf("end: current_thread = %d\n", current_thread);
    }

    // mark current thread as finished
    threads[current_thread].finished = 1;

    // search for threads joining this thread
    for (i = 0; i < thread_count; i++) {
        if (threads[i].joining == current_thread) {
            if (LOG_LEVEL & ULT_RESUME) {
                printf("end: thread %d no longer joining\n", i);
            }
            threads[i].joining = -1;
        }
    }

    ult_sched();
}


void init_thread(int id) {
    threads[id].id = id;
    threads[id].finished = 0;
    threads[id].joining = -1;
    threads[id].locking = -1;
}

void ult_log(int flags) {
    LOG_LEVEL = flags;
}

void print_threads() {
    for (int i = 0; i < thread_count; i++) {
        printf("thread %d: finished = %d, joining = %d, locking = %d\n", threads[i].finished, threads[i].joining, threads[i].locking);
    }
}

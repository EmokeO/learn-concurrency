#include <stdio.h>
#include "ult.h"


void first() {
    int i,j,k;

    for (i = 0; i < 1000; i++) {
        for (j = 0; j < 1000000; j++) {
            k = i + j;
        }

        if (i %  100 == 0) {
            printf("in first, i = %d\n", i);
        }
    }
}

void second() {
    int i,j,k;

    for (i = 0; i < 2000; i++) {
        for (j = 0; j < 1000000; j++) {
            k = i + j;
        }

        if (i %  100 == 0) {
            printf("in second, i = %d\n", i);
        }
    }
}

int main(int argc, char** argv) {
    int i, j, k;

    ult_init();
    ult_log(ULT_SCHED);

    ult_t t1 = ult_create(first);
    ult_t t2 = ult_create(second);

    ult_join(t1);
    for (i = 0; i < 2000; i++) {
        for (j = 0; j < 1000000; j++) {
            k = i + j;
        }

        if (i %  100 == 0) {
            printf("in main, i = %d\n", i);
        }
    }


    return 0;
}

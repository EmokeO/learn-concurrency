package ro.galanton.learn.javase.concurrency;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static ro.galanton.learn.javase.concurrency.phaser.builder.PhaserWorkerBuilder.worker;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.phaser.PhaserWorker;
import ro.galanton.learn.javase.concurrency.phaser.builder.AssertThat;
import ro.galanton.learn.javase.concurrency.util.CapturingExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.Holder;
import ro.galanton.learn.javase.concurrency.util.SysErrExceptionHandler;

public class PhaserSpec {
	
	private static final ExceptionHandler LOG_ERRORS = new SysErrExceptionHandler();
	
	private static void execute(PhaserWorker... workers) throws InterruptedException {
		ExecutorService executor = Executors.newFixedThreadPool(workers.length);
		for (PhaserWorker worker : workers) {
			executor.submit(worker);
		}
		executor.shutdown();
		executor.awaitTermination(2, TimeUnit.SECONDS);
	}
	
	private static void verify(PhaserWorker worker) {
		int i = 1;
		for (PhaserOperation operation : worker.getOperations()) {
			if (operation instanceof AssertThat) {
				AssertThat<?> assertion = (AssertThat<?>) operation;
				AssertionError error = assertion.getError();
				if (error != null) {
					throw new AssertionError("Assertion " + i + " failed", error);
				}
				i++;
			}
		}
	}

	public static class AsCyclicBarrier {
		
		@Test
		public void allows_threads_to_wait_for_each_other_to_reach_a_common_point() throws Exception {
			Phaser phaser = new Phaser(2);
			final PhaserWorker worker1 = worker(phaser, "worker-1", LOG_ERRORS)
					.sleep(100)
					.arriveAndAwaitAdvance()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2", LOG_ERRORS)
					.sleep(200)
					.arriveAndAwaitAdvance()
					.build();
			
			final PhaserWorker verifier = worker(null, "verifier")
					.sleep(150)
					.assertThat(() -> worker1.getOperation(1).isStarted(), is(true))
					.assertThat(() -> worker1.getOperation(1).isFinished(), is(false))
					.assertThat(() -> worker2.getOperation(1).isStarted(), is(false))
					.sleep(100)
					.assertThat(() -> worker1.getOperation(1).isFinished(), is(true))
					.assertThat(() -> worker2.getOperation(1).isFinished(), is(true))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
	}
	
	public static class AsCountDownLatch {
		
		@Test
		public void allows_threads_to_wait_until_a_set_of_operations_complete() throws Exception {
			Phaser phaser = new Phaser(2);
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.awaitAdvance(0)
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.awaitAdvance(0)
					.build();
			PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(100)
					.assertThat(() -> worker1.getOperation(0).isFinished(), is(false))
					.assertThat(() -> worker1.getOperation(0).isFinished(), is(false))
					.arrive()
					.sleep(100)
					.assertThat(() -> worker1.getOperation(0).isFinished(), is(false))
					.assertThat(() -> worker1.getOperation(0).isFinished(), is(false))
					.arrive()
					.sleep(100)
					.assertThat(() -> worker1.getOperation(0).isFinished(), is(true))
					.assertThat(() -> worker1.getOperation(0).isFinished(), is(true))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
	}
	
	public static class Registration {
		
		@Test
		public void number_of_registered_parties_may_increase_over_time() throws Exception {
			final Phaser phaser = new Phaser(0);
			assertThat(phaser.getRegisteredParties(), is(0));

			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.sleep(100)
					.register()
					.awaitAdvance(0)
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(200)
					.register()
					.awaitAdvance(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getRegisteredParties(), is(0))
					.sleep(100)
					.assertThat(() -> phaser.getRegisteredParties(), is(1))
					.sleep(100)
					.assertThat(() -> phaser.getRegisteredParties(), is(2))
					.arrive()
					.sleep(50)
					.assertThat(() -> worker1.getOperation(2).isFinished(), is(false))
					.assertThat(() -> worker2.getOperation(2).isFinished(), is(false))
					.arrive()
					.sleep(50)
					.assertThat(() -> worker1.getOperation(2).isFinished(), is(true))
					.assertThat(() -> worker2.getOperation(2).isFinished(), is(true))
					.assertThat(() -> phaser.getRegisteredParties(), is(2))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void number_of_registered_parties_may_decrease_over_time() throws Exception {
			Phaser phaser = new Phaser(2);
			
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.sleep(100)
					.arriveAndDeregister()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(200)
					.arriveAndDeregister()
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getRegisteredParties(), is(2))
					.sleep(100)
					.assertThat(() -> phaser.getRegisteredParties(), is (1))
					.sleep(100)
					.assertThat(() -> phaser.getRegisteredParties(), is(0))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void arrival_without_deregistration_changes_arrived_parties_count() throws Exception {
			final Phaser phaser = new Phaser(3);
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.sleep(100)
					.arrive()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(200)
					.arrive()
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getArrivedParties(), is(0))
					.sleep(100)
					.assertThat(() -> phaser.getArrivedParties(), is(1))
					.sleep(100)
					.assertThat(() -> phaser.getArrivedParties(), is(2))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void arrival_with_deregisitration_does_not_change_arrived_parties_count() throws Exception {
			final Phaser phaser = new Phaser(3);
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.sleep(100)
					.arriveAndDeregister()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(200)
					.arriveAndDeregister()
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getArrivedParties(), is(0))
					.sleep(100)
					.assertThat(() -> phaser.getArrivedParties(), is(0))
					.sleep(100)
					.assertThat(() -> phaser.getArrivedParties(), is(0))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void arrival_without_deregistration_changes_unarrived_parties_count() throws Exception {
			final Phaser phaser = new Phaser(3);
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.sleep(100)
					.arrive()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(200)
					.arrive()
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getUnarrivedParties(), is(3))
					.sleep(100)
					.assertThat(() -> phaser.getUnarrivedParties(), is(2))
					.sleep(100)
					.assertThat(() -> phaser.getUnarrivedParties(), is(1))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void arrival_with_deregistration_changes_unarrived_parties_count() throws Exception {
			final Phaser phaser = new Phaser(3);
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.sleep(100)
					.arriveAndDeregister()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(200)
					.arriveAndDeregister()
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getUnarrivedParties(), is(3))
					.sleep(100)
					.assertThat(() -> phaser.getUnarrivedParties(), is(2))
					.sleep(100)
					.assertThat(() -> phaser.getUnarrivedParties(), is(1))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void can_be_done_in_bulk() {
			// TODO
		}
	}
	
	public static class PhaseNumber {
		
		@Test
		public void initial_value_is_zero_by_default() throws Exception {
			final Phaser phaser = new Phaser(2);
			assertThat(phaser.getPhase(), is(0));
		}
		
		@Test
		public void advances_when_all_parties_arrive() throws Exception {
			final Phaser phaser = new Phaser(3);
			assertThat(phaser.getPhase(), is(0));
			
			phaser.arrive();
			assertThat(phaser.getPhase(), is(0));
			
			phaser.arrive();
			assertThat(phaser.getPhase(), is(0));
			
			phaser.arrive();
			assertThat(phaser.getPhase(), is(1));
		}
		
		@Test
		public void advances_then_unarrived_parties_is_reset_to_registered_parties() {
			final Phaser phaser = new Phaser(3);
			assertThat(phaser.getUnarrivedParties(), is(3));
			
			phaser.arrive();
			assertThat(phaser.getUnarrivedParties(), is(2));
			
			phaser.arrive();
			assertThat(phaser.getUnarrivedParties(), is(1));
			
			phaser.arrive();
			assertThat(phaser.getUnarrivedParties(), is(3));
		}
		
		@Test
		public void advances_then_arrived_parties_is_reset_to_zero() {
			final Phaser phaser = new Phaser(3);
			assertThat(phaser.getArrivedParties(), is(0));
			
			phaser.arrive();
			assertThat(phaser.getArrivedParties(), is(1));
			
			phaser.arrive();
			assertThat(phaser.getArrivedParties(), is(2));
			
			phaser.arrive();
			assertThat(phaser.getArrivedParties(), is(0));
		}

	}
	
	public static class Arrival {
	
		@Test
		public void returns_arrival_phase_number() {
			final Phaser phaser = new Phaser(2);
			assertThat(phaser.getPhase(), is(0));
			
			assertThat(phaser.arrive(), is(0));
			assertThat(phaser.arrive(), is(0));
			
			// 2 parties + 2 arrivals => the phase number advanced
			assertThat(phaser.getPhase(), is(1));
			
			assertThat(phaser.arrive(), is(1));
			assertThat(phaser.arrive(), is(1));
			
			// 2 parties + 2 arrivals => the phase number advanced
			assertThat(phaser.getPhase(), is(2));
		}
		
		@Test
		public void of_the_last_party_triggers_phase_advance() {
			final Phaser phaser = new Phaser(3); // 3 parties
			
			// 1st arrival, phase number remains unchanged
			phaser.arrive();
			assertThat(phaser.getPhase(), is(0));
			
			// 2nd arrival, phase number remains unchanged
			phaser.arrive();
			assertThat(phaser.getPhase(), is(0));
			
			// 3rd and *last* arrival trigger phase advance
			phaser.arrive();
			assertThat(phaser.getPhase(), is(1));
		}
		
	}
	
	public static class PhaseAdvanceAction {
		
		@Test
		public void is_optionally_specified_by_subclassing() {
			// setup
			final Holder<Boolean> advanceActionExecuted = new Holder<>(false);
			final Phaser phaser = new Phaser(2) {
				protected boolean onAdvance(int phase, int registeredParties) {
					advanceActionExecuted.setValue(true);
					return false;
				};
			};
			assertThat(advanceActionExecuted.getValue(), is(false));
			
			// test
			phaser.arrive();
			phaser.arrive();
			
			assertThat(advanceActionExecuted.getValue(), is(true));
		}
		
		@Test
		public void is_triggered_when_phase_advances() {
			// setup
			final Holder<Boolean> advanceActionExecuted = new Holder<>(false);
			final Phaser phaser = new Phaser(2) {
				protected boolean onAdvance(int phase, int registeredParties) {
					advanceActionExecuted.setValue(true);
					return false;
				};
			};
			assertThat(advanceActionExecuted.getValue(), is(false));
			
			// test
			phaser.arrive();
			assertThat(advanceActionExecuted.getValue(), is(false));
			
			phaser.arrive();
			assertThat(advanceActionExecuted.getValue(), is(true));
		}
		
		@Test
		public void is_executed_by_last_arriving_party() throws Exception {
			final Map<String, Object> captured = new HashMap<>();
			final Phaser phaser = new Phaser(2) {
				protected boolean onAdvance(int phase, int registeredParties) {
					captured.put("onAdvance", Thread.currentThread());
					return false;
				}
			};
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.capture(() -> Thread.currentThread()).as("thread-1").in(captured)
					.arrive()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.sleep(50)
					.capture(() -> Thread.currentThread()).as("thread-2").in(captured)
					.arrive()
					.build();
			
			execute(worker1, worker2);
			
			assertThat(captured, hasKey("thread-1"));
			assertThat(captured, hasKey("thread-2"));
			assertThat(captured, hasEntry("onAdvance", captured.get("thread-2")));
		}
		
		@Test
		public void is_executed_while_all_waiting_parties_are_dormant() throws Exception {
			final Holder<PhaserWorker> worker1Holder = new Holder<PhaserWorker>(null);
			final Holder<PhaserWorker> worker2Holder = new Holder<PhaserWorker>(null);
			final Holder<Boolean> worker1Dormant = new Holder<Boolean>(false);
			final Holder<Boolean> worker2Dormant = new Holder<Boolean>(false);
			
			final Phaser phaser = new Phaser(2) {
				protected boolean onAdvance(int phase, int registeredParties) {
					PhaserWorker worker1 = worker1Holder.getValue();
					PhaserOperation op1 = worker1.getOperation(0);
					worker1Dormant.setValue(op1.isStarted() && !op1.isFinished());
					
					PhaserWorker worker2 = worker2Holder.getValue();
					PhaserOperation op2 = worker2.getOperation(0);
					worker2Dormant.setValue(op2.isStarted() && !op2.isFinished());
					
					return false;
				}
			};
			final PhaserWorker worker1 = worker(phaser, "worker-1")
					.arriveAndAwaitAdvance()
					.build();
			final PhaserWorker worker2 = worker(phaser, "worker-2")
					.arriveAndAwaitAdvance()
					.build();
			
			worker1Holder.setValue(worker1);
			worker2Holder.setValue(worker2);
			
			execute(worker1, worker2);
			
			assertThat(worker1Dormant.getValue(), is(true));
			assertThat(worker2Dormant.getValue(), is(true));
		}
		
		@Test
		public void terminates_the_phaser_if_returns_true() {
			final Phaser phaser = new Phaser(2) {
				protected boolean onAdvance(int phase, int registeredParties) {
					return true;
				}
			};
			assertThat(phaser.isTerminated(), is(false));
			
			phaser.arrive();
			phaser.arrive();
			
			assertThat(phaser.isTerminated(), is(true));
		}
		
		@Test
		public void by_default_terminates_phaser_if_registered_parties_reaches_zero() {
			final Phaser phaser = new Phaser(2);
			assertThat(phaser.getPhase(), is(0));
			assertThat(phaser.isTerminated(), is(false));
			
			phaser.arrive();
			phaser.arrive();
			assertThat(phaser.getPhase(), is(1));
			assertThat(phaser.getRegisteredParties(), is(2));
			assertThat(phaser.isTerminated(), is(false));
			
			phaser.arrive();
			phaser.arriveAndDeregister();
			assertThat(phaser.getPhase(), is(2));
			assertThat(phaser.getRegisteredParties(), is(1));
			assertThat(phaser.isTerminated(), is(false));
			
			phaser.arrive();
			assertThat(phaser.getPhase(), is(3));
			assertThat(phaser.getRegisteredParties(), is(1));
			assertThat(phaser.isTerminated(), is(false));
			
			phaser.arriveAndDeregister();
			assertThat(phaser.getRegisteredParties(), is(0));
			assertThat(phaser.isTerminated(), is(true));
		}
	}
	
	public static class Waiting {
		
		@Test
		public void occurs_when_phase_number_equals_current_phase() throws Exception {
			final Phaser phaser = new Phaser(1);
			final PhaserWorker worker = worker(phaser, "worker-1")
					.awaitAdvance(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> phaser.getPhase(), is(0))
					.assertThat(() -> worker.getOperation(0).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(0).isFinished(), is(false))
					.arrive()
					.sleep(50)
					.assertThat(() -> phaser.getPhase(), is(1))
					.assertThat(() -> worker.getOperation(0).isFinished(), is(true))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void does_not_occur_when_phase_number_less_than_current_phase() throws Exception {
			final Phaser phaser = new Phaser(1);
			phaser.arrive(); // increase phase to 1
			
			final PhaserWorker worker = worker(phaser, "worker-1")
					.sleep(50)
					.awaitAdvance(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(100)
					.assertThat(() -> phaser.getPhase(), is(1))
					.assertThat(() -> worker.getOperation(1).isFinished(), is(true))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void does_not_occur_when_phase_number_greater_than_current_phase() throws Exception {
			final Phaser phaser = new Phaser(1);
			final PhaserWorker worker = worker(phaser, "worker-1")
					.sleep(50)
					.awaitAdvance(7)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(100)
					.assertThat(() -> phaser.getPhase(), is(0))
					.assertThat(() -> worker.getOperation(1).isFinished(), is(true))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test(timeout = 3000)
		public void can_continue_if_thread_is_interrupted() throws Exception {
			final Phaser phaser = new Phaser(1);
			final Holder<Thread> workerThread = new Holder<Thread>(null);
			
			final PhaserWorker worker = worker(phaser, "worker-1")
					.execute(() -> workerThread.setValue(Thread.currentThread()))
					.awaitAdvance(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> worker.getOperation(1).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(1).isFinished(), is(false))
					.execute(() -> workerThread.getValue().interrupt())
					.sleep(50)
					.assertThat(() -> worker.getOperation(1).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(1).isFinished(), is(false))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
			
			// unblock the waiting thread so the test can finish
			phaser.arrive();
		}
		
		@Test
		public void can_be_interrupted() throws Exception {
			final Phaser phaser = new Phaser(1);
			final CapturingExceptionHandler handler = new CapturingExceptionHandler();
			final Holder<Thread> workerThread = new Holder<Thread>(null);
			final PhaserWorker worker = worker(phaser, "worker", handler)
					.execute(() -> workerThread.setValue(Thread.currentThread()))
					.awaitAdvanceInterruptibly(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> worker.getOperation(1).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(1).isFinished(), is(false))
					.execute(() -> workerThread.getValue().interrupt())
					.sleep(50)
					.assertThat(() -> worker.getOperation(1).isFinished(), is(true))
					.assertThat(() -> handler.getCapturedException(), is(instanceOf(InterruptedException.class)))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void is_interrupted_does_not_change_phaser_state() throws Exception {
			final Phaser phaser = new Phaser(1);
			final CapturingExceptionHandler handler = new CapturingExceptionHandler();
			final Holder<Thread> workerThread = new Holder<Thread>(null);
			final PhaserWorker worker = worker(phaser, "worker", handler)
					.execute(() -> workerThread.setValue(Thread.currentThread()))
					.awaitAdvanceInterruptibly(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.execute(() -> workerThread.getValue().interrupt())
					.sleep(50)
					.assertThat(() -> phaser.getPhase(), is(0))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void can_be_timed() throws Exception {
			final Phaser phaser = new Phaser(1);
			final CapturingExceptionHandler handler = new CapturingExceptionHandler();
			final PhaserWorker worker = worker(phaser, "worker", handler)
					.awaitAdvanceInterruptibly(0, 100, TimeUnit.MILLISECONDS)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> worker.getOperation(0).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(0).isFinished(), is(false))
					.sleep(100)
					.assertThat(() -> worker.getOperation(0).isFinished(), is(true))
					.assertThat(() -> handler.getCapturedException(), is(instanceOf(TimeoutException.class)))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void timed_out_does_not_change_phaser_state() throws Exception {
			final Phaser phaser = new Phaser(1);
			final CapturingExceptionHandler handler = new CapturingExceptionHandler();
			final PhaserWorker worker = worker(phaser, "worker", handler)
					.awaitAdvanceInterruptibly(0, 100, TimeUnit.MILLISECONDS)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(200)
					.assertThat(() -> phaser.getPhase(), is(0))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
	}
	
	public static class Termination {
		
		@Test
		public void occurs_by_default_when_registered_parties_reaches_zero() throws Exception {
			new PhaseAdvanceAction().by_default_terminates_phaser_if_registered_parties_reaches_zero();
		}
		
		@Test
		public void occurs_when_phase_advance_action_returns_true() throws Exception {
			new PhaseAdvanceAction().terminates_the_phaser_if_returns_true();
		}
		
		@Test
		public void can_be_forced() throws Exception {
			final Phaser phaser = new Phaser(1);
			phaser.forceTermination();
			assertThat(phaser.isTerminated(), is(true));
		}
		
		@Test
		public void causes_waiting_threads_to_return_with_negative_result_from_await() throws Exception {
			final Phaser phaser = new Phaser(1);
			final CapturingExceptionHandler handler = new CapturingExceptionHandler();
			final PhaserWorker worker = worker(phaser, "worker", handler)
					.awaitAdvance(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.assertThat(() -> worker.getOperation(0).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(0).isFinished(), is(false))
					.forceTermination()
					.sleep(50)
					.assertThat(() -> worker.getOperation(0).isFinished(), is(true))
					.assertThat(() -> worker.getOperation(0).getIntResult(), is(lessThan(0)))
					.assertThat(() -> handler.getCapturedException(), is(nullValue()))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void causes_await_to_return_immediately_with_negative_result() throws Exception {
			final Phaser phaser = new Phaser(1);
			final CapturingExceptionHandler handler = new CapturingExceptionHandler();
			final PhaserWorker worker = worker(phaser, "worker", handler)
					.sleep(100)
					.awaitAdvance(0)
					.build();
			final PhaserWorker verifier = worker(phaser, "verifier")
					.sleep(50)
					.forceTermination()
					.sleep(100)
					.assertThat(() -> worker.getOperation(1).isStarted(), is(true))
					.assertThat(() -> worker.getOperation(1).isFinished(), is(true))
					.assertThat(() -> worker.getOperation(1).getIntResult(), is(lessThan(0)))
					.assertThat(() -> handler.getCapturedException(), is(nullValue()))
					.build();
			
			execute(worker, verifier);
			verify(verifier);
		}
		
		@Test
		public void makes_the_phaser_unusable() throws Exception {
			causes_await_to_return_immediately_with_negative_result();
		}
	}
	
	public static class Tiering {

		@Test
		public void when_child_registered_parties_becomes_non_zero_parent_registered_parties_increases_by_one() throws Exception {
			final Phaser parent = new Phaser(0);
			final Phaser child = new Phaser(parent, 0);
			
			assertThat(parent.getRegisteredParties(), is(0));
			assertThat(child.getRegisteredParties(), is(0));
			
			// test 1
			child.register();
			
			assertThat(parent.getRegisteredParties(), is(1));
			assertThat(child.getRegisteredParties(), is(1));
			
			child.register();
			assertThat(parent.getRegisteredParties(), is(1));
			assertThat(child.getRegisteredParties(), is(2));
		}
		
		@Test
		public void when_child_registered_parties_becomes_zero_parent_registered_parties_decreases_by_one() throws Exception {
			final Phaser parent = new Phaser(3);
			final Phaser child = new Phaser(parent, 2);
			
			assertThat(parent.getRegisteredParties(), is(4)); // 3 initial parties + 1 child
			assertThat(child.getRegisteredParties(), is(2));
			
			child.arriveAndDeregister();
			
			assertThat(parent.getRegisteredParties(), is(4));
			assertThat(child.getRegisteredParties(), is(1));
			
			child.arriveAndDeregister();
			
			assertThat(parent.getRegisteredParties(), is(3));
			assertThat(child.getRegisteredParties(), is(0));
		}
		
		@Test
		public void when_child_phaser_advances_parent_phaser_also_advances() throws Exception {
			final Phaser parent = new Phaser();
			final Phaser child = new Phaser(parent, 1);
			final PhaserWorker worker = worker(child, "worker")
					.arrive()
					.build();
			
			assertThat(child.getPhase(), is(0));
			assertThat(parent.getPhase(), is(0));
			
			execute(worker);
			
			assertThat(child.getPhase(), is(1));
			assertThat(parent.getPhase(), is(1));
		}
		
		@Test
		public void allows_all_tiered_phasers_to_advance_together() throws Exception {
			final Phaser parent = new Phaser(1);
			final Phaser left = new Phaser(parent, 1);
			final Phaser right = new Phaser(parent, 1);

			assertThat(left.getRegisteredParties(), is(1));
			assertThat(left.getArrivedParties(), is(0));
			assertThat(left.getUnarrivedParties(), is(1));
			
			assertThat(right.getRegisteredParties(), is(1));
			assertThat(right.getArrivedParties(), is(0));
			assertThat(right.getUnarrivedParties(), is(1));
			
			assertThat(parent.getRegisteredParties(), is(2));
			assertThat(parent.getArrivedParties(), is(0));
			assertThat(parent.getUnarrivedParties(), is(2));
			
			final PhaserWorker worker1 = worker(left, "worker-1")
					.sleep(100)
					.arrive()
					.build();
			final PhaserWorker worker2 = worker(right, "worker-2")
					.sleep(200)
					.arrive()
					.build();
			final PhaserWorker verifier = worker(null, "verifier")
					.sleep(50)
					.assertThat(() -> left.getPhase(), is(0))
					.assertThat(() -> right.getPhase(), is(0))
					.assertThat(() -> parent.getPhase(), is(0))
					.sleep(100) // wait for worker1 to arrive
					.assertThat(() -> left.getPhase(), is(0)) // phase did not advance
					.assertThat(() -> right.getPhase(), is(0))
					.assertThat(() -> parent.getPhase(), is(0))
					.sleep(100) // wait for worker2 to arrive
					.assertThat(() -> left.getPhase(), is(1)) // phase advanced
					.assertThat(() -> right.getPhase(), is(1))
					.assertThat(() -> parent.getPhase(), is(1))
					.build();
			
			execute(worker1, worker2, verifier);
			verify(verifier);
		}
		
		@Test
		public void reduces_contention() throws Exception {
			// variant 1: all 4 workers use the same phaser
			final Phaser phaser = new Phaser();
			final PhaserWorker worker1a = producer(phaser, "worker-1a", 100);
			final PhaserWorker worker2a = producer(phaser, "worker-2a", 200);
			final PhaserWorker worker3a = producer(phaser, "worker-3a", 300);
			final PhaserWorker worker4a = producer(phaser, "worker-4a", 400);
			final PhaserWorker verifier1 = worker(null, "verifier-1")
					.sleep(150) // wait for worker1a to arrive
					.assertThat(() -> phaser.getPhase(), is(0))
					.sleep(100) // wait for worker2a to arrive
					.assertThat(() -> phaser.getPhase(), is(0))
					.sleep(100) // wait for worker3a to arrive
					.assertThat(() -> phaser.getPhase(), is(0))
					.sleep(100) // wait for worker4a to arrive
					.assertThat(() -> phaser.getPhase(), is(1))
					.build();
			
			execute(worker1a, worker2a, worker3a, worker4a, verifier1);
			verify(verifier1);
			
			// variant 2: 2 workers per phaser
			final Phaser parent = new Phaser();
			final Phaser child = new Phaser(parent);
			final PhaserWorker worker1b = producer(parent, "worker-1b", 100);
			final PhaserWorker worker2b = producer(parent, "worker-2b", 200);
			final PhaserWorker worker3b = producer(child, "worker-3b", 300);
			final PhaserWorker worker4b = producer(child, "worker-4b", 400);
			final PhaserWorker verifier2 = worker(null, "verifier-2")
					.sleep(150) // wait for worker1a to arrive
					.assertThat(() -> parent.getPhase(), is(0))
					.assertThat(() -> child.getPhase(), is(0))
					.sleep(100) // wait for worker2a to arrive
					.assertThat(() -> parent.getPhase(), is(0))
					.assertThat(() -> child.getPhase(), is(0))
					.sleep(100) // wait for worker3a to arrive
					.assertThat(() -> parent.getPhase(), is(0))
					.assertThat(() -> child.getPhase(), is(0))
					.sleep(100) // wait for worker4a to arrive
					.assertThat(() -> parent.getPhase(), is(1))
					.assertThat(() -> child.getPhase(), is(1))
					.build();
			
			execute(worker1b, worker2b, worker3b, worker4b, verifier2);
			verify(verifier2);
		}
		
		@Test
		public void allows_phasers_to_be_organized_in_a_chain_structure() throws Exception {
			final Phaser phaser0 = new Phaser();
			final Phaser phaser1 = new Phaser(phaser0);
			final Phaser phaser2 = new Phaser(phaser1);
			
			// assign 2 workers per phaser
			final PhaserWorker worker1 = producer(phaser0, "worker-1", 100);
			final PhaserWorker worker2 = producer(phaser0, "worker-2", 200);
			final PhaserWorker worker3 = producer(phaser1, "worker-3", 300);
			final PhaserWorker worker4 = producer(phaser1, "worker-4", 400);
			final PhaserWorker worker5 = producer(phaser2, "worker-5", 500);
			final PhaserWorker worker6 = producer(phaser2, "worker-6", 600);
			final PhaserWorker verifier = worker(null, "verifier")
					.sleep(150) // wait for worker1 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker2 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker3 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker4 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker5 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker6 to arrive
					.assertThat(() -> phaser0.getPhase(), is(1))
					.assertThat(() -> phaser1.getPhase(), is(1))
					.assertThat(() -> phaser2.getPhase(), is(1))
					.build();
			
			execute(worker1, worker2, worker3, worker4, worker5, worker6, verifier);
			verify(verifier);
		}
		
		@Test
		public void allows_phasers_to_be_organized_in_a_tree_structure() throws Exception {
			// full binary tree of height 1
			final Phaser phaser0 = new Phaser();
			final Phaser phaser1 = new Phaser(phaser0);
			final Phaser phaser2 = new Phaser(phaser0);
			
			// assign 2 workers per phaser
			final PhaserWorker worker1 = producer(phaser0, "worker-1", 100);
			final PhaserWorker worker2 = producer(phaser0, "worker-2", 200);
			final PhaserWorker worker3 = producer(phaser1, "worker-3", 300);
			final PhaserWorker worker4 = producer(phaser1, "worker-4", 400);
			final PhaserWorker worker5 = producer(phaser2, "worker-5", 500);
			final PhaserWorker worker6 = producer(phaser2, "worker-6", 600);
			final PhaserWorker verifier = worker(null, "verifier")
					.sleep(150) // wait for worker1 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker2 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker3 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker4 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker5 to arrive
					.assertThat(() -> phaser0.getPhase(), is(0))
					.assertThat(() -> phaser1.getPhase(), is(0))
					.assertThat(() -> phaser2.getPhase(), is(0))
					.sleep(100) // wait for worker6 to arrive
					.assertThat(() -> phaser0.getPhase(), is(1))
					.assertThat(() -> phaser1.getPhase(), is(1))
					.assertThat(() -> phaser2.getPhase(), is(1))
					.build();
			
			execute(worker1, worker2, worker3, worker4, worker5, worker6, verifier);
			verify(verifier);
		}
		
		private static PhaserWorker producer(Phaser phaser, String name, long delay) {
			return worker(phaser, name)
					.register()
					.sleep(delay) // simulate work
					.arriveAndAwaitAdvance() // wait for others to arrive
					.build();
		}
		
	}
	
}

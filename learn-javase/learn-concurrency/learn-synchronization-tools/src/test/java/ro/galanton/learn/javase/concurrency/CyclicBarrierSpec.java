package ro.galanton.learn.javase.concurrency;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.util.Arrays;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

import ro.galanton.learn.javase.concurrency.barrier.CyclicBarrierCommand;
import ro.galanton.learn.javase.concurrency.barrier.CyclicBarrierWorker;
import ro.galanton.learn.javase.concurrency.barrier.TimedCyclicBarrierWorker;
import ro.galanton.learn.javase.concurrency.util.CapturingExceptionHandler;

public class CyclicBarrierSpec {

	@Test
	public void allows_threads_to_wait_for_each_other_to_reach_a_common_point() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(2);
		CyclicBarrierWorker worker1 = new CyclicBarrierWorker(barrier, "worker-1");
		CyclicBarrierWorker worker2 = new CyclicBarrierWorker(barrier, "worker-2");
		
		// test 1
		new Thread(worker1, "thread-1").start();
		Thread.sleep(500);
		
		// verify 1
		assertThat(worker1.isBarrierPassed(), is(false));
		
		// test 2
		new Thread(worker2, "thread-2").start();
		Thread.sleep(500);
		
		// verify 2
		assertThat(worker1.isBarrierPassed(), is(true));
		assertThat(worker2.isBarrierPassed(), is(true));
	}
	
	@Test
	public void can_be_reused_after_waiting_threads_are_released() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(1);
		CyclicBarrierWorker worker = new CyclicBarrierWorker(barrier, "worker-1");
		
		new Thread(worker, "thread-1").start();
		Thread.sleep(500);
		assertThat(worker.isBarrierPassed(), is(true));
		
		// test - reuse the same barrier
		worker = new CyclicBarrierWorker(barrier, "worker-2");
		new Thread(worker, "thread-2").start();
		Thread.sleep(500);
		
		// verify
		assertThat(worker.isBarrierPassed(), is(true));
	}
	
	@Test
	public void supports_optional_command_after_last_thread_arrives_but_before_all_threads_are_released() throws Exception {
		// setup
		CyclicBarrierCommand command = new CyclicBarrierCommand("barrier-command");
		CyclicBarrier barrier = new CyclicBarrier(2, command);
		CyclicBarrierWorker worker1 = new CyclicBarrierWorker(barrier, "worker-1");
		CyclicBarrierWorker worker2 = new CyclicBarrierWorker(barrier, "worker-2");
		
		// test
		new Thread(worker1, "thread-1").start();
		new Thread(worker2, "thread-2").start();
		Thread.sleep(500);
		
		// verify
		assertThat(command.isExecuted(), is(true));
		assertThat(Arrays.asList("thread-1", "thread-2"), hasItems(command.getThreadName()));
		
	}
	
	@Test
	public void if_waiting_thread_is_interrupted_the_barrier_is_broken() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(2);
		CyclicBarrierWorker worker = new CyclicBarrierWorker(barrier, "worker-1");
		Thread t = new Thread(worker, "thread-1");
		t.start();
		Thread.sleep(500);
		
		// test
		t.interrupt();
		Thread.sleep(500);
		
		// verify
		assertThat(barrier.isBroken(), is(true));
	}
	
	@Test
	public void interrupted_waiting_thread_receives_InterruptedException() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(2);
		CapturingExceptionHandler exceptionHandler = new CapturingExceptionHandler();
		CyclicBarrierWorker worker = new CyclicBarrierWorker(barrier, "worker-1", exceptionHandler);
		Thread t = new Thread(worker, "thread-1");
		t.start();
		Thread.sleep(500);
		
		// test
		t.interrupt();
		Thread.sleep(500);
		
		// verify
		assertThat(exceptionHandler.getCapturedException(), is(instanceOf(InterruptedException.class)));
	}
	
	@Test
	public void if_waiting_thread_times_out_the_barrier_is_broken() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(2);
		TimedCyclicBarrierWorker worker = new TimedCyclicBarrierWorker(barrier, "worker-1");
		
		// test
		new Thread(worker).start();
		Thread.sleep(1_000);
		
		// verify
		assertThat(barrier.isBroken(), is(true));
	}
	
	@Test
	public void timed_out_waiting_thread_receives_TimeoutException() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(2);
		CapturingExceptionHandler exceptionHandler = new CapturingExceptionHandler();
		TimedCyclicBarrierWorker worker = new TimedCyclicBarrierWorker(barrier, "worker-1", exceptionHandler);
		
		// test
		new Thread(worker).start();
		Thread.sleep(1_000);
		
		// verify
		assertThat(worker.isBarrierPassed(), is(false));
		assertThat(exceptionHandler.getCapturedException(), is(instanceOf(TimeoutException.class)));
	}
	
	@Test
	public void if_barrier_breaks_all_other_threads_receive_BarrierBrokenException() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(3);
		CapturingExceptionHandler exceptionHandler1 = new CapturingExceptionHandler();
		CapturingExceptionHandler exceptionHandler2 = new CapturingExceptionHandler();
		CyclicBarrierWorker worker1 = new CyclicBarrierWorker(barrier, "worker-1", exceptionHandler1);
		TimedCyclicBarrierWorker worker2 = new TimedCyclicBarrierWorker(barrier, "worker-2", exceptionHandler2);
		
		// test
		new Thread(worker1).start();
		new Thread(worker2).start();
		Thread.sleep(1_000);
		
		// verify
		assertThat(exceptionHandler1.getCapturedException(), is(instanceOf(BrokenBarrierException.class)));
		assertThat(exceptionHandler2.getCapturedException(), is(instanceOf(TimeoutException.class)));
	}
	
	@Test
	public void if_barrier_is_reset_waiting_threads_receive_BarrierBrokenException() throws Exception {
		// setup
		CyclicBarrier barrier = new CyclicBarrier(2);
		CapturingExceptionHandler exceptionHandler = new CapturingExceptionHandler();
		CyclicBarrierWorker worker = new CyclicBarrierWorker(barrier, "worker-1", exceptionHandler);
		
		// test
		new Thread(worker).start();
		Thread.sleep(500); // wait for worker to reach barrier
		barrier.reset();
		Thread.sleep(500); // wait for handler to handler exception
		
		// verify
		assertThat(exceptionHandler.getCapturedException(), is(instanceOf(BrokenBarrierException.class)));
		assertThat(barrier.isBroken(), is(false));
	}
	
	@Test
	public void broken_barrier_cannot_be_used_until_reset() throws Exception {
		CyclicBarrier barrier = new CyclicBarrier(3);
		CapturingExceptionHandler exceptionHandler = new CapturingExceptionHandler();
		TimedCyclicBarrierWorker worker1 = new TimedCyclicBarrierWorker(barrier, "worker-1");
		CyclicBarrierWorker worker2 = new CyclicBarrierWorker(barrier, "worker-2", exceptionHandler);
		
		new Thread(worker1).start();
		Thread.sleep(500); // wait for barrier to be broken
		assertThat(barrier.isBroken(), is(true));
		
		// test
		new Thread(worker2).start();
		Thread.sleep(500); // wait for worker2 to reach barrier
		
		// verify
		assertThat(exceptionHandler.getCapturedException(), is(instanceOf(BrokenBarrierException.class)));
	}
	
	@Test
	public void broken_barrier_can_be_used_after_reset() throws Exception {
		CyclicBarrier barrier = new CyclicBarrier(3);
		CapturingExceptionHandler exceptionHandler = new CapturingExceptionHandler();
		TimedCyclicBarrierWorker worker1 = new TimedCyclicBarrierWorker(barrier, "worker-1");
		CyclicBarrierWorker worker2 = new CyclicBarrierWorker(barrier, "worker-2", exceptionHandler);
		
		new Thread(worker1).start();
		Thread.sleep(500); // wait for barrier to be broken
		assertThat(barrier.isBroken(), is(true));
		
		// test
		barrier.reset();
		new Thread(worker2).start();
		Thread.sleep(500); // wait for worker2 to reach barrier
		
		// verify
		assertThat(exceptionHandler.getCapturedException(), is(nullValue()));
	}
	
}

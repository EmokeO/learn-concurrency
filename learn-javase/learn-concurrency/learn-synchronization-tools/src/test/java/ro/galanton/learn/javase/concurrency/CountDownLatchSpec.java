package ro.galanton.learn.javase.concurrency;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import java.util.concurrent.CountDownLatch;

import org.junit.Test;

import ro.galanton.learn.javase.concurrency.latch.CountDownLatchWorker;
import ro.galanton.learn.javase.concurrency.latch.TimedCountDownLatchWorker;
import ro.galanton.learn.javase.concurrency.util.CapturingExceptionHandler;

public class CountDownLatchSpec {

	@Test
	public void allows_threads_to_wait_until_a_set_of_operations_complete() throws Exception {
		// setup
		CountDownLatch latch = new CountDownLatch(3);
		CountDownLatchWorker worker1 = new CountDownLatchWorker(latch, "worker-1");
		CountDownLatchWorker worker2 = new CountDownLatchWorker(latch, "worker-2");
		new Thread(worker1).start();
		new Thread(worker2).start();
		
		// test & verify 1
		latch.countDown();
		Thread.sleep(100);
		assertThat(worker1.isFinished(), is(false));
		assertThat(worker2.isFinished(), is(false));
		
		// test & verify 2
		latch.countDown();
		Thread.sleep(100);
		assertThat(worker1.isFinished(), is(false));
		assertThat(worker2.isFinished(), is(false));
		
		// test & verify 3
		latch.countDown();
		Thread.sleep(100);
		assertThat(worker1.isFinished(), is(true));
		assertThat(worker2.isFinished(), is(true));
	}
	
	@Test
	public void it_cannot_be_reused() throws Exception {
		// setup
		CountDownLatch latch = new CountDownLatch(1);
		CountDownLatchWorker worker = new CountDownLatchWorker(latch, "worker-1");
		new Thread(worker).start();
		
		// sanity check - worker is blocked
		Thread.sleep(100);
		assertThat(worker.isFinished(), is(false));
		
		// release the worker
		latch.countDown();
		
		// sanity check - the worker is released
		Thread.sleep(100);
		assertThat(worker.isFinished(), is(true));
		
		// test - start a new worker with a latch whose count already reached zero
		worker = new CountDownLatchWorker(latch, "worker-2");
		new Thread(worker).start();
		Thread.sleep(100);
		
		// verify - the worker not blocked at the latch
		assertThat(worker.isFinished(), is(true));
		
	}
	
	@Test(timeout = 1_000)
	public void threads_calling_countDown_are_not_blocked() {
		// setup
		CountDownLatch latch = new CountDownLatch(1);
		
		// test - this call does not block the current thread
		latch.countDown();
		
		// verify
		assertThat(latch.getCount(), is(0L));
	}
	
	@Test
	public void waiting_threads_can_be_interrupted() throws Exception {
		// setup
		CountDownLatch latch = new CountDownLatch(1);
		CapturingExceptionHandler handler = new CapturingExceptionHandler();
		CountDownLatchWorker worker = new CountDownLatchWorker(latch, "worker-1", handler);
		Thread t = new Thread(worker);
		t.start();
		
		// sanity check 
		Thread.sleep(100); // wait for worker to reach the latch
		assertThat(worker.isStarted(), is(true));
		assertThat(worker.isFinished(), is(false));
		
		// test
		t.interrupt();
		
		// verify
		Thread.sleep(100); // wait for handler to handle the exception
		assertThat(handler.getCapturedException(), is(instanceOf(InterruptedException.class)));
	}
	
	@Test
	public void waiting_threads_can_timeout() throws Exception {
		// setup
		CountDownLatch latch = new CountDownLatch(1);
		TimedCountDownLatchWorker worker = new TimedCountDownLatchWorker(latch, "worker-1");
		
		// test
		new Thread(worker).start();
		Thread.sleep(500); // wait for worker to time-out
		
		// verify
		assertThat(worker.isTimedOut(), is(true));
	}
}

package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class BulkRegister extends AbstractPhaserOperation {

	private final int parties;
	
	private int result;
	
	public BulkRegister(int parties) {
		this.parties = parties;
	}

	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before bulkRegister(%d)", parties);
		result = phaser.bulkRegister(parties);
		logger.log("after bulkRegister(%d): %d", parties, result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}

}

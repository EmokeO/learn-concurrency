package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class AwaitAdvance extends AbstractPhaserOperation {

	private final int phase;
	
	private int result;
	
	public AwaitAdvance(int phase) {
		this.phase = phase;
	}

	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before awaitAdvance(%d)", phase);
		result = phaser.awaitAdvance(phase);
		logger.log("after awaitAdvance(%d): %d", phase, result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}
}

package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class AwaitAdvanceInterruptibly extends AbstractPhaserOperation {

	private final int phase;
	
	private int result;
	
	public AwaitAdvanceInterruptibly(int phase) {
		this.phase = phase;
	}
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before awaitAdvanceInterruptibly(%d)", phase);
		result = phaser.awaitAdvanceInterruptibly(phase);
		logger.log("after awaitAdvanceInterruptibly(%d): %d", phase, result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}

}

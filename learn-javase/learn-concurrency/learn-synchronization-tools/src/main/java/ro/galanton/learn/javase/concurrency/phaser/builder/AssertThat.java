package ro.galanton.learn.javase.concurrency.phaser.builder;

import static org.hamcrest.MatcherAssert.assertThat;

import java.util.concurrent.Phaser;

import org.hamcrest.Matcher;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class AssertThat<T> extends AbstractPhaserOperation {

	private final DelayedValue<T> actual;
	private final Matcher<T> matcher;
	
	private AssertionError error;
	
	public AssertThat(DelayedValue<T> actual, Matcher<T> matcher) {
		this.actual = actual;
		this.matcher = matcher;
	}
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		T actualValue = actual.evaluate();
		logger.log("assert that %s %s", actualValue, matcher);
		try {
			assertThat(actualValue, matcher);
		} catch (AssertionError e) {
			this.error = e;
			throw e;
		}
	}

	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("ExpectThat does not return an int result");
	}
	
	public AssertionError getError() {
		return error;
	}

}

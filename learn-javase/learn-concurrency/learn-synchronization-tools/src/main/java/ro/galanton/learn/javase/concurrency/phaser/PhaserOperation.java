package ro.galanton.learn.javase.concurrency.phaser;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public interface PhaserOperation {

	public void execute(Phaser phaser, Logger logger) throws Exception;
	
	public int getIntResult();
	
	public boolean isStarted();
	
	public boolean isFinished();
}

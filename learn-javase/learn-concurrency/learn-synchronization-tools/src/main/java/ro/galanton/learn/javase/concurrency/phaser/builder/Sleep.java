package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class Sleep extends AbstractPhaserOperation {

	private final long millis;
	
	public Sleep(long millis) {
		this.millis = millis;
	}
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before sleep(%d)", millis);
		Thread.sleep(millis);
		logger.log("after sleep(%d)", millis);
	}

	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("Thread.sleep() does not return an int result");
	}

}

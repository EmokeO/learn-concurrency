package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class ForceTermination extends AbstractPhaserOperation {

	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("ForceTermination does not return an int result");
	}

	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before forceTermination()");
		phaser.forceTermination();
		logger.log("after forceTermination()");
	}

}

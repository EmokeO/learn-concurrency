package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class Arrive extends AbstractPhaserOperation {

	private int result;
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) {
		logger.log("before arrive");
		result = phaser.arrive();
		logger.log("after arrive: %d", result);
	}

	@Override
	public int getIntResult() {
		return result;
	}
}

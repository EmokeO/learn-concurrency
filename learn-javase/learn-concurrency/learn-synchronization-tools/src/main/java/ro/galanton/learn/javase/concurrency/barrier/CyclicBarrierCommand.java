package ro.galanton.learn.javase.concurrency.barrier;

import ro.galanton.learn.javase.concurrency.util.ConsoleLogger;
import ro.galanton.learn.javase.concurrency.util.Logger;
import ro.galanton.learn.javase.concurrency.util.NameLogger;
import ro.galanton.learn.javase.concurrency.util.Named;
import ro.galanton.learn.javase.concurrency.util.ThreadNameLogger;

public class CyclicBarrierCommand implements Runnable, Named {

	private final String name;
	private final Logger logger;
	
	private String threadName;
	private boolean executed;
	
	public CyclicBarrierCommand(String name) {
		this.name = name;
		logger = new ThreadNameLogger(new NameLogger(this, new ConsoleLogger()));
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void run() {
		logger.log("executing");
		threadName = Thread.currentThread().getName();
		executed = true;
	}

	public String getThreadName() {
		return threadName;
	}
	
	public boolean isExecuted() {
		return executed;
	}
}

package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class ArriveAndDeregister extends AbstractPhaserOperation {

	private int result;
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before arriveAndDeregister");
		result = phaser.arriveAndDeregister();
		logger.log("after arriveAndDeregister: %d", result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}

}

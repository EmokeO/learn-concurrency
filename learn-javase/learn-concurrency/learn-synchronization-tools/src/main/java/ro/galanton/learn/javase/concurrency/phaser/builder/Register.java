package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class Register extends AbstractPhaserOperation {

	private int result;
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) {
		logger.log("before register");
		result = phaser.register();
		logger.log("after register: %d", result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}
}

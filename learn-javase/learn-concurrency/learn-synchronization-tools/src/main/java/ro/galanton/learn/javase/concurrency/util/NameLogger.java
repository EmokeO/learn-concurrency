package ro.galanton.learn.javase.concurrency.util;

public class NameLogger implements Logger {

	private final Logger delegate;
	private final Named named;
	
	public NameLogger(Named named, Logger delegate) {
		this.delegate = delegate;
		this.named = named;
	}

	@Override
	public void log(String message) {
		String name = named.getName();
		delegate.log(String.format("%s: %s", name, message));
	}
	
	@Override
	public void log(String format, Object... args) {
		log(String.format(format, args));
	}
}

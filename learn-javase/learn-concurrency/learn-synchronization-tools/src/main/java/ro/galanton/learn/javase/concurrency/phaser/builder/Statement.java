package ro.galanton.learn.javase.concurrency.phaser.builder;

@FunctionalInterface
public interface Statement {

	public void execute();
	
}

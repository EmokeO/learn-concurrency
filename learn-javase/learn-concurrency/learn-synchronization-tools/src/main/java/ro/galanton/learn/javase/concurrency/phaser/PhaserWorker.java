package ro.galanton.learn.javase.concurrency.phaser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.ConsoleLogger;
import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.Logger;
import ro.galanton.learn.javase.concurrency.util.NameLogger;
import ro.galanton.learn.javase.concurrency.util.Named;
import ro.galanton.learn.javase.concurrency.util.ThreadNameLogger;

public class PhaserWorker implements Runnable, Named {

	private final Phaser phaser;
	private final String name;
	private final Optional<ExceptionHandler> exceptionHandler;
	private final Logger logger;

	private List<PhaserOperation> operations = new ArrayList<>();
	
	public PhaserWorker(Phaser phaser, String name) {
		this(phaser, name, null);
	}
	
	public PhaserWorker(Phaser phaser, String name, ExceptionHandler exceptionHandler) {
		this.phaser = phaser;
		this.name = name;
		this.exceptionHandler = Optional.ofNullable(exceptionHandler);
		this.logger = new ThreadNameLogger(new NameLogger(this, new ConsoleLogger()));
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void run() {
		try {
			for (PhaserOperation operation : operations) {
				operation.execute(phaser, logger);
			}
		} catch (Throwable e) {
			if (exceptionHandler.isPresent()) {
				exceptionHandler.get().handleException(e);
			}
		}
	}

	public void addOperation(PhaserOperation operation) {
		operations.add(operation);
	}
	
	public PhaserOperation getOperation(int index) {
		return operations.get(index);
	}
	
	public List<PhaserOperation> getOperations() {
		return Collections.unmodifiableList(operations);
	}
}

package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.Map;
import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class Capture<K, V> extends AbstractPhaserOperation {

	private Map<K, ? super V> map;
	private K key;
	private DelayedValue<V> actual;
	
	public Capture(Map<K, ? super V> map, K key, DelayedValue<V> actual) {
		this.map = map;
		this.key = key;
		this.actual = actual;
	}
	
	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("Capture operation does not return an int result");
	}

	@Override
	public void doExecute(Phaser phaser, Logger logger) throws Exception {
		map.put(key, actual.evaluate());
	}

}

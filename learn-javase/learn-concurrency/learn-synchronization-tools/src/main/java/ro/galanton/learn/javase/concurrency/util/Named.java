package ro.galanton.learn.javase.concurrency.util;

public interface Named {

	String getName();
	
}

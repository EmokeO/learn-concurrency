package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.util.Logger;

public abstract class AbstractPhaserOperation implements PhaserOperation {

	private volatile boolean started;
	private volatile boolean finished;
	
	@Override
	public final void execute(Phaser phaser, Logger logger) throws Exception {
		started = true;
		try {
			doExecute(phaser, logger);
		} finally {
			finished = true;
		}
	}
	
	protected abstract void doExecute(Phaser phaser, Logger logger) throws Exception;

	@Override
	public final boolean isStarted() {
		return started;
	}

	@Override
	public final boolean isFinished() {
		return finished;
	}

}

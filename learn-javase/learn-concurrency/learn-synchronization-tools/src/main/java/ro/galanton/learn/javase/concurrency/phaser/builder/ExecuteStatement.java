package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class ExecuteStatement extends AbstractPhaserOperation {

	private final Statement stmt;
	
	public ExecuteStatement(Statement stmt) {
		this.stmt = stmt;
	}
	
	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("Statement does not return an int result");
	}

	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before statement");
		stmt.execute();
		logger.log("after statement");
	}

}

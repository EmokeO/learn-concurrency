package ro.galanton.learn.javase.concurrency.latch;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import ro.galanton.learn.javase.concurrency.util.ConsoleLogger;
import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.Logger;
import ro.galanton.learn.javase.concurrency.util.NameLogger;
import ro.galanton.learn.javase.concurrency.util.Named;
import ro.galanton.learn.javase.concurrency.util.ThreadNameLogger;

public class CountDownLatchWorker implements Named, Runnable {

	private final String name;
	private final CountDownLatch latch;
	private final Optional<ExceptionHandler> exceptionHandler;
	private final Logger logger;

	private boolean started;
	private boolean finished;
	
	public CountDownLatchWorker(CountDownLatch latch, String name) {
		this(latch, name, null);
	}
	
	public CountDownLatchWorker(CountDownLatch latch, String name, ExceptionHandler exceptionHandler) {
		this.latch = latch;
		this.name = name;
		this.exceptionHandler = Optional.ofNullable(exceptionHandler);
		this.logger = new ThreadNameLogger(new NameLogger(this, new ConsoleLogger()));
	}
	
	@Override
	public void run() {
		try {
			started = true;
			
			logger.log("waiting at latch");
			latch.await();
			logger.log("passed latch");
			
			finished = true;
		} catch (Exception e) {
			if (exceptionHandler.isPresent()) {
				exceptionHandler.get().handleException(e);
			}
		}
	}

	@Override
	public String getName() {
		return name;
	}
	
	public boolean isStarted() {
		return started;
	}
	
	public boolean isFinished() {
		return finished;
	}

}

package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class ArriveAndAwaitAdvance extends AbstractPhaserOperation {

	private int result;
	
	@Override
	protected void doExecute(Phaser phaser, Logger logger) {
		logger.log("before arriveAndAwaitAdvance");
		result = phaser.arriveAndAwaitAdvance();
		logger.log("after arriveAndAwaitAdvance: %d", result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}

}

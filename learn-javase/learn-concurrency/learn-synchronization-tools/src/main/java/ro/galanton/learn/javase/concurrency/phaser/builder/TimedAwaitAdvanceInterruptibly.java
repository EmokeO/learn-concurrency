package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class TimedAwaitAdvanceInterruptibly extends AbstractPhaserOperation {

	private final int phase;
	private final long timeout;
	private final TimeUnit unit;
	
	private int result;
	
	public TimedAwaitAdvanceInterruptibly(int phase, long timeout, TimeUnit unit) {
		this.phase = phase;
		this.timeout = timeout;
		this.unit = unit;
	}


	@Override
	protected void doExecute(Phaser phaser, Logger logger) throws Exception {
		logger.log("before awaitAdvanceInterruptibly(%d, %d, %s)", phase, timeout, unit);
		result = phaser.awaitAdvanceInterruptibly(phase, timeout, unit);
		logger.log("after awaitAdvanceInterruptibly(%d, %d, %s): %d", phase, timeout, unit, result);
	}
	
	@Override
	public int getIntResult() {
		return result;
	}

}

package ro.galanton.learn.javase.concurrency.phaser.builder;

@FunctionalInterface
public interface DelayedValue<T> {

	public T evaluate();
}

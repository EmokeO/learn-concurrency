package ro.galanton.learn.javase.concurrency.util;

public interface Logger {

	void log(String message);
	
	void log(String format, Object... args);
	
}
